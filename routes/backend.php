<?php
Route::middleware(['auth'])->group(function () {
	Route::get('/dashboard', 'BackController@index')->name('be.dashboard');
	Route::middleware(['role:administrador'])->group(function () {
		/*
		|------------------------------------------------------------------------------------------------
		| admin product routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('products', 'ProductController', ['names' => [
			'index' => 'admin-products.index',
			'show' => 'admin-products.show',
			'create' => 'admin-products.create',
			'store' => 'admin-products.store',
			'edit' => 'admin-products.edit',
			'update' => 'admin-products.update',
			'destroy' => 'admin-products.destroy',
			'store_image' => 'admin-products.store_image',
			'destroy_image' => 'admin-products.destroy_image',
		]]);
		Route::get('datatable/products', 'ProductController@datatable')->name('superadmin.datatable.products');
		Route::get('status/products/{id}', 'ProductController@status')->name('superadmin.datatable.status.product');

		/*
		|------------------------------------------------------------------------------------------------
		| admin orders routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('orders', 'OrderController', ['names' => [
			'index' => 'admin-orders.index',
			'show' => 'admin-orders.show',
			'create' => 'admin-orders.create',
			'store' => 'admin-orders.store',
			'edit' => 'admin-orders.edit',
			'update' => 'admin-orders.update',
			'destroy' => 'admin-orders.destroy',
		]]);
		Route::get('datatable/orders', 'OrderController@datatable')->name('superadmin.datatable.orders');
		Route::post('status/orders', 'OrderController@status')->name('superadmin.datatable.status.order');

		/*
		|------------------------------------------------------------------------------------------------
		| admin color routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('colors', 'ColorController', ['names' => [
			'index' => 'admin-colors-attr.index',
			'show' => 'admin-colors-attr.show',
			'create' => 'admin-colors-attr.create',
			'store' => 'admin-colors-attr.store',
			'edit' => 'admin-colors-attr.edit',
			'update' => 'admin-colors-attr.update',
			'destroy' => 'admin-colors-attr.destroy',
		]]);
		Route::get('datatable/colors', 'ColorController@datatable')->name('superadmin.datatable.colors');
		Route::get('status/colors/{id}', 'ColorController@status')->name('superadmin.datatable.status.color');

		/*
		|------------------------------------------------------------------------------------------------
		| admin manufacturer routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('manufacturing', 'ManufacturerController', ['names' => [
			'index' => 'admin-manufacturing-attr.index',
			'show' => 'admin-manufacturing-attr.show',
			'create' => 'admin-manufacturing-attr.create',
			'store' => 'admin-manufacturing-attr.store',
			'edit' => 'admin-manufacturing-attr.edit',
			'update' => 'admin-manufacturing-attr.update',
			'destroy' => 'admin-manufacturing-attr.destroy',
		]]);
		Route::get('datatable/manufacturing', 'ManufacturerController@datatable')->name('superadmin.datatable.manufacturing');
		Route::get('status/manufacturing/{id}', 'ManufacturerController@status')->name('superadmin.datatable.status.manufacturer');

		/*
		|------------------------------------------------------------------------------------------------
		| admin size routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('sizes', 'SizeController', ['names' => [
			'index' => 'admin-sizes-attr.index',
			'show' => 'admin-sizes-attr.show',
			'create' => 'admin-sizes-attr.create',
			'store' => 'admin-sizes-attr.store',
			'edit' => 'admin-sizes-attr.edit',
			'update' => 'admin-sizes-attr.update',
			'destroy' => 'admin-sizes-attr.destroy',
		]]);
		Route::get('datatable/sizes', 'SizeController@datatable')->name('superadmin.datatable.sizes');
		Route::get('status/sizes/{id}','SizeController@status')->name('superadmin.datatable.status.size');

		/*
		|------------------------------------------------------------------------------------------------
		| admin use routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('usos', 'UseController', ['names' => [
			'index' => 'admin-uses-attr.index',
			'show' => 'admin-uses-attr.show',
			'create' => 'admin-uses-attr.create',
			'store' => 'admin-uses-attr.store',
			'edit' => 'admin-uses-attr.edit',
			'update' => 'admin-uses-attr.update',
			'destroy' => 'admin-uses-attr.destroy',
		]]);
		Route::get('datatable/usos', 'UseController@datatable')->name('superadmin.datatable.uses');
		Route::get('status/usos/{id}', 'UseController@status')->name('superadmin.datatable.status.use');

		/*
		|------------------------------------------------------------------------------------------------
		| admin space routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('spaces', 'SpaceController', ['names' => [
			'index' => 'admin-spaces-attr.index',
			'show' => 'admin-spaces-attr.show',
			'create' => 'admin-spaces-attr.create',
			'store' => 'admin-spaces-attr.store',
			'edit' => 'admin-spaces-attr.edit',
			'update' => 'admin-spaces-attr.update',
			'destroy' => 'admin-spaces-attr.destroy',
		]]);
		Route::get('datatable/spaces', 'SpaceController@datatable')->name('superadmin.datatable.spaces');
		Route::get('status/spaces/{id}', 'SpaceController@status')->name('superadmin.datatable.status.space');

		/*
		|------------------------------------------------------------------------------------------------
		| admin anti-slip routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('anti-slip', 'AntiSlipController', ['names' => [
			'index' => 'admin-anti-slip-attr.index',
			'show' => 'admin-anti-slip-attr.show',
			'create' => 'admin-anti-slip-attr.create',
			'store' => 'admin-anti-slip-attr.store',
			'edit' => 'admin-anti-slip-attr.edit',
			'update' => 'admin-anti-slip-attr.update',
			'destroy' => 'admin-anti-slip-attr.destroy',
		]]);
		Route::get('datatable/antislip', 'AntiSlipController@datatable')->name('superadmin.datatable.anti-slips');
		Route::get('status/antislip/{id}', 'AntiSlipController@status')->name('superadmin.datatable.status.anti-slip');

		/*
		|------------------------------------------------------------------------------------------------
		| admin brand routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('brands', 'BrandController', ['names' => [
			'index' => 'admin-brands-attr.index',
			'show' => 'admin-brands-attr.show',
			'create' => 'admin-brands-attr.create',
			'store' => 'admin-brands-attr.store',
			'edit' => 'admin-brands-attr.edit',
			'update' => 'admin-brands-attr.update',
			'destroy' => 'admin-brands-attr.destroy',
		]]);
		Route::get('datatable/brands', 'BrandController@datatable')->name('superadmin.datatable.brands');
		Route::get('status/brands/{id}', 'BrandController@status')->name('superadmin.datatable.status.brand');


		/*
		|------------------------------------------------------------------------------------------------
		| admin pasta routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('pastas', 'PastaController', ['names' => [
			'index' => 'admin-pastas-attr.index',
			'show' => 'admin-pastas-attr.show',
			'create' => 'admin-pastas-attr.create',
			'store' => 'admin-pastas-attr.store',
			'edit' => 'admin-pastas-attr.edit',
			'update' => 'admin-pastas-attr.update',
			'destroy' => 'admin-pastas-attr.destroy',
		]]);
		Route::get('datatable/pastas', 'PastaController@datatable')->name('superadmin.datatable.pastas');
		Route::get('status/pastas/{id}', 'PastaController@status')->name('superadmin.datatable.status.pasta');

		/*
		|------------------------------------------------------------------------------------------------
		| admin pasta routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('materials', 'TextureController', ['names' => [
			'index' => 'admin-textures-attr.index',
			'show' => 'admin-textures-attr.show',
			'create' => 'admin-textures-attr.create',
			'store' => 'admin-textures-attr.store',
			'edit' => 'admin-textures-attr.edit',
			'update' => 'admin-textures-attr.update',
			'destroy' => 'admin-textures-attr.destroy',
		]]);
		Route::get('datatable/textures', 'TextureController@datatable')->name('superadmin.datatable.textures');
		Route::get('status/textures/{id}', 'TextureController@status')->name('superadmin.datatable.status.texture');

		/*
		|------------------------------------------------------------------------------------------------
		| admin stores routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('stores', 'StoreController', ['names' => [
			'index' => 'admin-stores.index',
			'show' => 'admin-stores.show',
			'create' => 'admin-stores.create',
			'store' => 'admin-stores.store',
			'edit' => 'admin-stores.edit',
			'update' => 'admin-stores.update',
			'destroy' => 'admin-stores.destroy',
		]]);
		Route::get('datatable/stores', 'StoreController@datatable')->name('superadmin.datatable.stores');
		Route::get('status/stores/{id}', 'StoreController@status')->name('superadmin.datatable.status.store');

		/*
		|------------------------------------------------------------------------------------------------
		| admin galery routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('galery', 'GaleryController', ['names' => [
			'index' => 'admin-galery.index',
			'show' => 'admin-galery.show',
			'create' => 'admin-galery.create',
			'store' => 'admin-galery.store',
			'edit' => 'admin-galery.edit',
			'update' => 'admin-galery.update',
			'destroy' => 'admin-galery.destroy',
		]]);
		Route::get('datatable/galery/{id}', 'GaleryController@datatable')->name('superadmin.datatable.galery.product');

		/*
		|------------------------------------------------------------------------------------------------
		| admin pasta routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('showcases', 'ShowCaseController', ['names' => [
			'index' => 'admin-showcases.index',
			'show' => 'admin-showcases.show',
			'create' => 'admin-showcases.create',
			'store' => 'admin-showcases.store',
			'edit' => 'admin-showcases.edit',
			'update' => 'admin-showcases.update',
			'destroy' => 'admin-showcases.destroy',
		]]);
		Route::get('datatable/showcases', 'ShowCaseController@datatable')->name('superadmin.datatable.showcases');
		Route::get('status/showcases/{id}', 'ShowCaseController@status')->name('superadmin.datatable.status.showcase');
		
	});
	
	
	Route::middleware(['role:super administrador'])->group(function () {
		/*
		|------------------------------------------------------------------------------------------------
		| admin user routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('users', 'UserController', ['names' => [
			'index' => 'admin-users.index',
			'show' => 'admin-users.show',
			'create' => 'admin-users.create',
			'store' => 'admin-users.store',
			'edit' => 'admin-users.edit',
			'update' => 'admin-users.update',
			'destroy' => 'admin-users.destroy',
		]]);
		Route::get('datatable/users', 'UserController@datatable')->name('superadmin.datatable.users');
		Route::get('status/users/{id}', 'UserController@status')->name('superadmin.datatable.status.user');
		
		/*
		|------------------------------------------------------------------------------------------------
		| admin clients routes
		|------------------------------------------------------------------------------------------------
		|
		*/
		Route::resource('clients', 'ClientController', ['names' => [
			'index' => 'admin-clients.index',
			'show' => 'admin-clients.show',
			'create' => 'admin-clients.create',
			'store' => 'admin-clients.store',
			'edit' => 'admin-clients.edit',
			'update' => 'admin-clients.update',
			'destroy' => 'admin-clients.destroy',
		]]);
		Route::get('datatable/clients', 'ClientController@datatable')->name('superadmin.datatable.clients');
		Route::get('status/clients/{id}', 'ClientController@status')->name('superadmin.datatable.status.client');
	});
	/*
	|------------------------------------------------------------------------------------------------
	| admin profile-user routes
	|------------------------------------------------------------------------------------------------
	|
	*/
	Route::get('profile/{username}', 'ProfileUserController@show')->name('profile.user');
	Route::get('profile/stteings/{username}', 'ProfileUserController@settings')->name('settings.user');
	Route::post('cuenta/actualizar-datos', 'ProfileUserController@update_info')->name('profile.user.updateInfo');
	Route::post('cuenta/actualizar-avatar', 'ProfileUserController@update_avatar')->name('profile.user.updateAvatar');
	Route::post('cuenta/actualizar-contraseña', 'ProfileUserController@update_pwd')->name('profile.user.updatePwd');
});