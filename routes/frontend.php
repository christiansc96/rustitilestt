<?php
// Auth::routes();

Route::get('/', 'FrontController@index')->name('fe.home');

Route::get('Autocomplete', 'FrontController@autocomplete');

Route::get('rustitiles-ceramic-online', 'FrontController@products')->name('fe.products');

Route::get('ceramic-products/{filter}', 'FrontController@filter')->name('fe.products.filter');

Route::get('rustitiles-about-us', 'FrontController@about_us')->name('fe.about_us');

Route::get('rustitiles-contact', 'FrontController@contact')->name('fe.contact');

Route::get('ceramic-product/{product}', 'FrontController@show_product')->name('fe.show.products');

// Route::get('rustitiles-diretory', 'FrontController@directory')->name('fe.directory');

// Route::get('rustitiles-diretory/{state}', 'FrontController@show_directory')->name('fe.show.directory');

Route::get('SignIn', 'ClientLogin@showLoginForm')->name('fe.showLoginForm');

Route::post('sigup', 'ClientRegister@register')->name('fe.register');

Route::post('signin', 'ClientLogin@login')->name('fe.login');

Route::post('cerrar-sesion', 'ClientLogin@logout')->name('fe.logout');

Route::middleware(['role:cliente'])->group(function () {

	Route::resource('rustitiles-cart','CartController');

	Route::post('rustitiles-cart/order', 'CartController@storeOrder')->name('storeOrder');

	Route::get('rustitiles-cart/datatable', 'CartController@datatable')->name('datatable.carrito');

	Route::resource('rustitiles-my-orders','MyOrdersController');

});

// Exception routes
Route::get('page-not-found/404.html', 'ExceptionController@index')->name('fe.404');

