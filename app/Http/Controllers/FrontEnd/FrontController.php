<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Image;
use App\Space;
use App\Uso;
use App\Size;
use App\Color;
use App\State;
use App\Store;
use App\ShowCase;
use App\Brand;
use App\Manufacturer;

class FrontController extends Controller
{
	/**
	 * [index description]
	 * @return [type] [description]
	 */
    public function index()
    {
        $pisos = Product::where([['use_id', '=',2],['active', '=', 1]])->get();
        $paredes = Product::where([['use_id', '=',1],['active', '=', 1]])->get();
        if(sizeof($pisos) > 0)
        {  
            $products_pisos = $pisos->take(2);
        }else{
            $products_pisos = null;
        }
        if(sizeof($paredes) > 0)
        {
            $products_paredes = $paredes->take(2);
        }else{
            $products_paredes = null;
        }
        $showcases = ShowCase::where('active', 1)->get();
    	return view('frontend.index', compact('products_pisos', 'products_paredes', 'showcases'));
    }

    public function products()
    {
    	
        $products = Product::where('active', 1)->orderBy('id', 'DESC')->paginate(32);

    	$spaces = Space::all();

    	$uses = Uso::all();

        $colors = Color::all();

        $sizes = Size::all();

        $brands = Brand::all();

        $manufacturers = Manufacturer::all();

    	return view('frontend.productos', compact('products', 'spaces', 'uses', 'colors', 'sizes', 'brands', 'manufacturers'));
    }

    public function filter($filter)
    {
        if(Space::where('name', $filter)->first())
        {
            $space = Space::where('name', $filter)->first();

            $products = Product::where('space_id', $space->id)->orderBy('id', 'DESC')->paginate(20);
        }
        elseif(Uso::where('name', $filter)->first())
        {
            $use = Uso::where('name', $filter)->first();

            $products = Product::where('use_id', $use->id)->orderBy('id', 'DESC')->paginate(20);
        }
        elseif(Color::where('name', $filter)->first())
        {
            $color = Color::where('name', $filter)->first();

            $products = $color->product()->orderBy('id', 'DESC')->paginate(20);
        }
        elseif(Size::where('name', $filter)->first())
        {
            $size = Size::where('name', $filter)->first();

            $products = $size->product()->orderBy('id', 'DESC')->paginate(20);
        }
        elseif(Brand::where('name', $filter)->first())
        {
            $brand = Brand::where('name', $filter)->first();

            $products = $brand->product()->orderBy('id', 'DESC')->paginate(20);
        }
        elseif(Manufacturer::where('name', $filter)->first())
        {
            $manufacturer = Manufacturer::where('name', $filter)->first();

            $products = $manufacturer->product()->orderBy('id', 'DESC')->paginate(20);
        }
        else
        {
            abort(404);
        }

        $spaces = Space::all();

        $uses = Uso::all();

        $colors = Color::all();

        $sizes = Size::all();

        $brands = Brand::all();

        $manufacturers = Manufacturer::all();

        return view('frontend.productos', compact('products', 'spaces', 'uses', 'colors', 'sizes', 'brands', 'manufacturers'));
    }


    public function show_product($slug)
    {
        $xproducts = Product::where('active', 1)->get()->take(4);

        if(Product::where('slug', $slug)->first()){

            $rproduct = Product::where('slug', $slug)->first();
        
            foreach($xproducts as $xproduct):

                //Share Related Products
                if($rproduct->id != $xproduct->id):

                    if($rproduct->space->id == $xproduct->space->id || $rproduct->uso->id == $xproduct->uso->id || $rproduct->price->id == $xproduct->price->id || $rproduct->antislip->id == $xproduct->antislip->id || $rproduct->brand->id == $xproduct->brand->id || $rproduct->pasta->id == $xproduct->pasta->id || $rproduct->texture->id == $xproduct->texture->id || $rproduct->manufacturer->id == $xproduct->manufacturer->id):

                        //Related Products
                        $results[] = $xproduct;

                    endif;

                else:

                    $results[] = null;

                endif;
                
            endforeach;

            $count_image = 1;

            return view('frontend.detalle', compact('rproduct', 'results', 'count_image'));

        }
    }

    public function directory()
    {
        $stores = Store::where('active', 1)->get();

        $states = State::where('active', 1)->get();

        return view('frontend.directorio', compact('stores', 'states'));
    }

    public function show_directory($state)
    {
        $states = State::where('active', 1)->get();

        $state = State::where('name', $state)->first();

        $stores = Store::where('state_id', $state->id)->get();

        return view('frontend.directorio', compact('stores', 'states'));
    }

    public function contact()
    {
        return view('frontend.contacto');
    }

    public function about_us()
    {
        return view('frontend.about-us');
    }

    public function autocomplete(Request $request) {

        if(!is_numeric($request->input('info'))) {

            if(strlen($request->input('info')) == 1){

                $search = Product::where('name', 'like', trim($request->input('info')).'%')
                                 ->orderBy('name')
                                 ->limit(10)
                                 ->get();
            }else if(strlen($request->input('info')) == 2){

                $search = Product::where('name', 'like', trim($request->input('info')).'%')
                                 ->orderBy('name')
                                 ->limit(8)
                                 ->get();
            }else if(strlen($request->input('info')) >= 3 ){

                $search = Product::where('name', 'like', trim($request->input('info')).'%')
                                 ->orderBy('name')
                                 ->limit(6)
                                 ->get();
            }
            
        }else{

            if(strlen($request->input('info')) == 1){

                $search = Product::where('code', 'like', trim($request->input('info')).'%')
                                 ->orderBy('code')
                                 ->limit(10)
                                 ->get();
            }else if(strlen($request->input('info')) == 2){

                $search = Product::where('code', 'like', trim($request->input('info')).'%')
                                 ->orderBy('code')
                                 ->limit(8)
                                 ->get();
            }else if(strlen($request->input('info')) >= 3 ){

                $search = Product::where('code', 'like', trim($request->input('info')).'%')
                                 ->orderBy('code')
                                 ->limit(6)
                                 ->get();
            }
        }

        $response = Array();

        if($search){

            if($request->segment(1) == 'rustitiles-ceramic-online'){
                $response['output'] = '<ul class="row">';
                foreach ($search as $key) {
                    $response['output']   .= '<li class="col-md-12"><a style="color: #424242 !important;" href ="'.route('fe.show.products', $key->slug).'">'.$key->name.'</a></li>';
                }
                $response['output'] .= '</ul>';
                
            }else{
                $response['output'] = '<ul class="row">';
                foreach ($search as $key) {
                    $response['output']   .= '<li class="col-md-12"><a style="color: #424242 !important;" href ="'.route('fe.show.products', $key->slug).'">'.$key->name.'</a></li>';
                }
                $response['output'] .= '</ul>';
            }
            
            return json_encode($response);

        }else{

            if($request->segment(1) == 'rustitiles-ceramic-online'){
                $response['output'] = '<ul class="row">';
                $response['output']   .= '<li class="col-md-12"><a style="color: #424242 !important;" href ="#">No results</a></li>';
                $response['output'] .= '</ul>';

            }else{

                $response['output'] = '<ul class="row">';
                $response['output']   .= '<li class="col-md-12"><a style="color: #424242 !important;" href ="#">No results</a></li>';
                $response['output'] .= '</ul>';

            }

        }
    }

}
