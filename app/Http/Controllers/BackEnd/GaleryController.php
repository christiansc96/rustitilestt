<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Product;
use App\Image;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class GaleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = new Image();

        if(sizeof($image->product) > 4):

            return 'This product can not have more than 4 images.';
            
        endif;

        $image->name = $request->file('image')->getClientOriginalName();

        $image->active = 1;

        $image->save();
        
        $image->product()->attach($request->product_id);

        $request->file('image')->storeAs('images/products/'.$request->product_id.'/', $request->file('image')->getClientOriginalName(), 'public');

        return 'Image added!';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $product = Product::findOrFail($request->product_id);

        foreach ($product->image as $image) {

            if ($image->outstanding == 1) {

                $_image = Image::findOrFail($image->id);

                $_image->outstanding = 0;

                $_image->save();

            }

        }

        $image = Image::findOrFail($id);

        $image->outstanding = 1;

        $image->save();

        return 'The image '. $image->name .' now it is highlighted!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Image::findOrFail($id);

        $message = 'Image '. $image->name .' removed';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        Storage::disk('public')->delete('images/products/'.$image->product[0]->id.'/'.$image->id);

        $image->product()->detach();

        $image->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables, $id){

        $products = Product::findOrFail($id);

        return $datatables->of($products->image)
            ->addColumn('image', function($image){

                return '<img title="'.$image->name.'" alt="'.$image->name.'" style="border-radius: 1px !important;" class="rounded-square" width="100" height="100" src="'. asset('storage/images/products/'.$image->pivot->product_id.'/'.$image->name) .'">';

            })
            ->addColumn('destacado', function($image){
                if ($image->outstanding == 1) {
                    return '<span class="label label-sm label-success"> Yes </span>';
                }
                return '<span class="label label-sm label-warning"> No </span>';

            })
            ->addColumn('opciones', function($image){
                return '<div class="btn-group pull-right">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-left" role="menu">
                                    <li>
                                        <a href="javascript:;" title="Outstanding" onclick="outstandingData('.$image->id.','.$image->pivot->product_id.')">
                                            <i class="fa fa-gratipay"></i> Outstanding 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" title="Remove" onclick="removeData('.$image->id.')">
                                            <i class="fa fa-trash"></i> Remove 
                                        </a>
                                    </li>
                                </ul>
                            </div>';
            })->rawColumns(['image', 'opciones', 'destacado' ])->make(true);
    }
}
