<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Color;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.colors.colors');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $color = new Color();

        $color->name = trim($request->name);

        $color->active = 1;

        $color->save();

        return 'Color added!';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $color = Color::findOrFail($id);

        return $color;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $color = Color::findOrFail($id);

        $color->name = trim($request->name);

        $color->active = 1;

        $color->save();

        return 'Color updated!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $color = Color::findOrFail($id);

        $message = 'color '. $color->name .' removed';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        $color->product()->detach();

        $color->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * [status description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function status($id)
    {

        $color = Color::findOrFail($id);

        if($color->active == 1):

            $color->active = 0;

            $color->save();

            $textstatus = 'Disable';

        else:

            $color->active = 1;

            $color->save();

            $textstatus = 'Available';

        endif;

        return 'Color '.$color->name.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $colors = Color::all();

        return $datatables->of($colors)
            ->addColumn('status', function($color){
                if($color->active == 1):

                    return '<span class="label label-sm label-success"> Available </span>';

                endif;

                return '<span class="label label-sm label-warning"> Disable </span>';
            })
            ->addColumn('opciones', function($color){
                if($color->active == 1):
                    return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$color->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$color->id.')">
                                                <i class="fa fa-times"></i> To disable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$color->id.')">
                                                <i class="fa fa-trash"></i> Trash 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    else:
                        return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$color->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Enable" onclick="statusData('.$color->id.')">
                                                <i class="fa fa-check"></i> Enable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$color->id.')">
                                                <i class="fa fa-trash"></i> Trash 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    endif;
            })->rawColumns(['status', 'opciones' ])->make(true);
    }
}
