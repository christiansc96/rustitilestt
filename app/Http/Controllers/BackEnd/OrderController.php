<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.orders.orders');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);

        return view('backend.orders.description-order', compact($order));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);
        $response['total_order'] = 0;
        $response['qty'] = $order->products->count();

        foreach ($order->products as $product) {
            $response['product_id'][] = $product->id;
            $response['name'][] = $product->name;
            $response['code'][] = $product->code;
            $response['price'][] = $product->price->price;
            $response['total_order'] = $response['total_order'] + $product->price->price;
        }

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);

        $message = 'Order '. $order->code .' removido';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        $order->products()->detach();

        $order->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

        return $message;
    }

    /**
     * [status description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function status(Request $request)
    {

        $order = order::findOrFail($request->order_id);

        if($request->status == 1):

            $order->description = trim($request->description);

            $order->active = 1;

            $order->save();

            $textstatus = 'OK';

        elseif($request->status == 2):

            $order->description = trim($request->description);

            $order->active = 2;

            $order->save();

            $textstatus = 'on hold';

        else:

            $order->description = trim($request->description);

            $order->active = 0;

            $order->save();

            $textstatus = 'Rejected';

        endif;

        return 'Order '.$order->name.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $orders = Order::all();

        return $datatables->of($orders)
            ->addColumn('user', function($order){
                return $order->user->username;
            })
            ->addColumn('product_count', function($order){
                return $order->products->count();
            })
            ->addColumn('status', function($order){
                if($order->active == 1):

                    return '<span class="label label-sm label-success"> OK </span>';

                elseif($order->active == 2):

                    return '<span class="label label-sm label-warning"> Espera </span>';

                elseif($order->active == 3):

                    return '<span class="label label-sm label-danger"> Anulado por '.$order->user->fullname.' </span>';

                endif;

                return '<span class="label label-sm label-danger"> Rejected </span>';
            })
            ->addColumn('opciones', function($order){
                if($order->active == 1):
                    return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a onclick="viewProducts('.$order->id.')" title="Description">
                                                <i class="fa fa-eye"></i> Details
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$order->id.', 0)">
                                                <i class="fa fa-times"></i>  
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$order->id.', 2)">
                                                <i class="fa fa-clock-o"></i> on hold 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$order->id.')">
                                                <i class="fa fa-trash"></i> Trash 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    elseif($order->active == 2):
                        return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a onclick="viewProducts('.$order->id.')" title="Description">
                                                <i class="fa fa-eye"></i> Details
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:;" title="Enable" onclick="statusData('.$order->id.', 1)">
                                                <i class="fa fa-check"></i> Aprobar 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$order->id.', 0)">
                                                <i class="fa fa-times"></i> reject 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$order->id.')">
                                                <i class="fa fa-trash"></i> Trash 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    else:   

                        return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a onclick="viewProducts('.$order->id.')" title="Description">
                                                <i class="fa fa-eye"></i> Details
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:;" title="Enable" onclick="statusData('.$order->id.', 1)">
                                                <i class="fa fa-check"></i> Aprobar 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$order->id.', 2)">
                                                <i class="fa fa-clock-o"></i> on hold 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$order->id.')">
                                                <i class="fa fa-trash"></i> Trash 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';

                    endif;
            })->rawColumns(['status', 'opciones' ])->make(true);
    }

}
