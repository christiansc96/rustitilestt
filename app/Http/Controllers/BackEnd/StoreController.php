<?php

namespace App\Http\Controllers\BackEnd;

use App\Store;
use App\Image;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = State::all();

        return view('backend.stores.stores', compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $store = new Store();

        $store->name = trim($request->name);

        $store->rif = trim($request->rif);

        $store->phone = trim($request->phone);

        $store->address = trim($request->address);

        $store->email = trim($request->email);

        $store->user_id = trim($request->user()->id);

        $store->state_id = trim($request->state);

        $store->active  = 1;

        if ($request->hasFile('image')):

            foreach($request->file('image') as $_image):

                $image          = new Image();

                $image->name    = $_image->getClientOriginalName();

                $image->active  = 1;

                $image->save();

                $store->image_id = $image->id;

                $store->save();

                $_image->storeAs('images/stores/'.$store->id.'/', $_image->getClientOriginalName(), 'public');

            endforeach;

        endif;

        return 'store '. $store->name .' added!';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $store = Store::findOrFail($id);

        return view('backend.stores.description.description-store', compact('store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = Store::findOrFail($id);
        
        return $store;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = Store::findOrFail($id);

        $store->name = trim($request->name);

        $store->rif = trim($request->rif);

        $store->phone = trim($request->phone);

        $store->address = trim($request->address);

        $store->email = trim($request->email);

        $store->user_id = trim($request->user()->id);

        $store->state_id = trim($request->state);

        if ($request->hasFile('image')):

            foreach($request->file('image') as $_image):

                $image          = Image::findOrFail($store->image->id);

                $image->name    = $_image->getClientOriginalName();

                $image->save();

                $store->save();

                Storage::disk('public')->delete('images/stores/'.$store->id.'/'.$store->image->name);

                Storage::disk('public')->delete('images/stores/'.$store->id.'/'.$_image->getClientOriginalName());

                $_image->storeAs('images/stores/'.$store->id.'/', $_image->getClientOriginalName(), 'public');

            endforeach;

        else:

            $store->save();

        endif;

        return 'store '. $store->name .' added!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = Store::findOrFail($id);

        $message = 'store '. $store->username .' removed';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        Storage::disk('public')->deleteDirectory('images/stores/'.$store->id);

        if($store->image->id != 2):

            Image::find($store->image->id)->delete();

        endif;

        $store->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * Status of user
     * @param int $id <id user>
     * @return \Illuminate\Http\Response <AJAX> 
     */

    public function status($id)
    {

        $store = Store::findOrFail($id);

        if($store->active == 1):

            $store->active = 0;

            if($store->image->id != 2):

                $store->image->active = 0;

            endif;

            $store->save();

            $textstatus = 'Disable';

        else:

            $store->active = 1;

            if($store->image->id != 2):

                $store->image->active = 1;

            endif;

            $store->save();

            $textstatus = 'Enable';

        endif;

        return 'store '.$store->name.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $stores = Store::all();

        return $datatables->of($stores)
            ->addColumn('user', function($store){

                return $store->user->username;

            })
            ->addColumn('state', function($store){

                return $store->state->name;

            })
            ->addColumn('status', function($store){
                if($store->active == 1):

                    return '<span class="label label-sm label-success"> Enable </span>';

                endif;

                return '<span class="label label-sm label-warning"> Disable </span>';
            })
            ->addColumn('show_photo', function($store){
                return '<img class="rounded-square" width="50" height="50" src="'. asset('storage/images/stores/'.$store->id.'/'.$store->image->name) .'" alt="'.$store->image->name.'" title="'.$store->image->name.'">';
            })
            ->addColumn('opciones', function($store){
                if($store->active == 1):
                    return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="'.route('admin-stores.show', $store->id).'" title="Description">
                                                <i class="fa fa-eye"></i> Description
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$store->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$store->id.')">
                                                <i class="fa fa-times"></i> Deshabilitar 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$store->id.')">
                                                <i class="fa fa-trash"></i> Removed 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    else:
                        return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="'.route('admin-stores.show', $store->id).'" title="Description">
                                                <i class="fa fa-eye"></i> Description
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$store->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Enable" onclick="statusData('.$store->id.')">
                                                <i class="fa fa-check"></i> Enable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$store->id.')">
                                                <i class="fa fa-trash"></i> Removed 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    endif;
            })->rawColumns(['show_photo','status', 'opciones' ])->make(true);
    }
}
