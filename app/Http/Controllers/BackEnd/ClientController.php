<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Client;
use Yajra\Datatables\Datatables;
use DB;
use Hash;
use Carbon\Carbon;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.clients.clients');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new Client();

        $client->name = trim($request->name);

        $client->password = trim(bcrypt($request->password));

        $client->email = trim($request->email);

        $client->phone = trim($request->phone);

        $client->fiscal_address = trim($request->fiscal_address);

        $client->store_address = trim($request->store_address);

        $client->deposit_address = trim($request->deposit_address);

        $client->contact_person = trim($request->contact_person);

        $client->active = 1;

        $client->role_id = 4;

        $client->save();

        event(new Registered($client));

        $this->registered($request, $client);

        return ucfirst($client->name);
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed  $client
     * @return mixed
     */
    protected function registered(Request $request, $client){
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);

        return $client;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::findOrFail($id);

        $client->name = trim($request->name);

        $client->password = trim(bcrypt($request->password));

        $client->email = trim($request->email);

        $client->phone = trim($request->phone);

        $client->fiscal_address = trim($request->fiscal_address);

        $client->store_address = trim($request->store_address);

        $client->deposit_address = trim($request->deposit_address);

        $client->contact_person = trim($request->contact_person);

        $client->save();

        return ucfirst($client->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = client::findOrFail($id);

        $message = 'Client '. $client->name .' removed';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        $client->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * Active of user
     * @param int $id <id user>
     * @return \Illuminate\Http\Response <AJAX> 
     */

    public function active($id)
    {

        $client = Client::findOrFail($id);

        if($client->active == 1):

            $client->active = 0;

            $client->save();

            $textstatus = 'Disable';

        else:

            $client->active = 1;

            $client->save();

            $textstatus = 'Available';

        endif;

        return 'Client '.$client->name.' '.$textstatus;

    }

    /**
     * Active of user
     * @param int $id <id user>
     * @return \Illuminate\Http\Response <AJAX> 
     */

    public function status($id)
    {

        $client = Client::findOrFail($id);

        if($client->status == 1):

            $client->active = 0;

            $client->save();

            $textstatus = 'Disable';

        else:

            $client->active = 1;

            $client->save();

            $textstatus = 'Available';

        endif;

        return 'Client '.$client->name.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $clients = Client::all();  

        return $datatables->of($clients)
            ->addColumn('status', function($client){
                if($client->active == 1):

                    return '<span class="label label-sm label-success"> Available </span>';

                endif;

                return '<span class="label label-sm label-warning"> Disable </span>';

            })
            ->addColumn('opciones', function($client){
                if($client->active == 1):
                    return '<div class="btn-group pull-right">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-left" role="menu">
                                    <li>
                                        <a href="javascript:;" title="Edit" onclick="editForm('.$client->id.')">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" title="To disable" onclick="statusData('.$client->id.')">
                                            <i class="fa fa-times"></i> To disable 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" title="Trash" onclick="removeData('.$client->id.')">
                                            <i class="fa fa-trash"></i> Trash 
                                        </a>
                                    </li>
                                </ul>
                            </div>';
                else:
                    return '<div class="btn-group pull-right">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-left" role="menu">
                                    <li>
                                        <a href="javascript:;" title="Edit" onclick="editForm('.$client->id.')">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" title="Enable" onclick="statusData('.$client->id.')">
                                            <i class="fa fa-check"></i> Enable 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" title="Trash" onclick="removeData('.$client->id.')">
                                            <i class="fa fa-trash"></i> Trash 
                                        </a>
                                    </li>
                                </ul>
                            </div>';
                endif;
            })->rawColumns(['status', 'opciones'])->make(true);
    }
}
