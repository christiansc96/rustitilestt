<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Role;
use App\Image;
use Yajra\Datatables\Datatables;
use DB;
use Hash;
use Carbon\Carbon;

class ProfileUserController extends Controller
{
    /**
     * show profile of user
     * @param  User    $user    [description]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function show(User $user, Request $request)
    {
        return view('backend.user-profile.profile', compact('user'));
    }

    /**
     * settings of user
     * @param  User    $user    [description]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function settings(User $user, Request $request)
    {
        return view('backend.user-profile.settings', compact('user'));
    }

    /**
     * update info of user
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
    */
    public function update_info(Request $request)
    {
        $this->validate($request, [

            'id'            => 'required',

            'fullname'      => 'required',

            'email'         => 'required',

            'username'      => 'required',

            'phone'         => 'required',

            // 'description'   => 'required'

        ]);

        $user               = User::findOrFail($request->id);

        $user->fullname     = trim(strtolower($request->fullname));

        $user->phone        = trim($request->phone);

        $user->username     = trim($request->username);

        $user->email        = trim(strtolower($request->email));

        // $user->description  = trim($request->description);

        $user->save();

        return 'Info updated!';

    }

    /**
     * update avatar of user
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update_avatar(Request $request)
    {
        $this->validate($request, [

            'id'            => 'required',

            'avatar'        => 'required|image',

        ]);

        $user = User::findOrFail($request->id);

        //save the file image on storage/public/image/users/{user_id}/{image.ext}
        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()):

            if($user->image->id == 1):
                //save the data image on db
                $image = new Image();

                $new_name_image = str_slug($user->fullname, '-').'.'.$request->file('avatar')->getClientOriginalExtension();

                $image->setAllAttribute($new_name_image, $user->username);

                $user->image_id = $image->id;

                $user->save();

                $request->file('avatar')->storeAs('images/users/'.$user->id.'/', $new_name_image, 'public');

            else:

                $image = Image::find($user->image->id);

                Storage::disk('public')->delete('images/users/'.$user->id.'/'.$image->name);

                $new_name_image = str_slug($user->fullname, '-').'.'.$request->file('avatar')->getClientOriginalExtension();

                $image->setAllAttribute($new_name_image, $user->username, $image->id);

                $user->image_id = $image->id;

                $user->save();

                $request->file('avatar')->storeAs('images/users/'.$user->id.'/', $new_name_image, 'public');

            endif;

            return 'Avatar updated!';

        endif;

        return "Avatar don't updated ";
    }

    /**
     * update password of user
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
    */
    public function update_pwd(Request $request)
    {
        $this->validate($request, [

            'id'            => 'required',

            'old_password'  => 'required',

            'password'      => 'required',

        ]);

        $user = User::findOrFail($request->id);

        if(!Hash::check($request->old_password, $user->password)):

            return 'password does not match the previous';

        endif;

        $user->password = trim(bcrypt($request->password));

        $user->save();

        return 'Password updated!';
    }
}

