<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Auth\Events\Registered;
use App\User;
use App\Role;
use App\Image;
use Yajra\Datatables\Datatables;
use DB;
use Hash;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        return view('backend.users.users', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [

            'role'          => 'required',

            'fullname'      => 'required',

            'password'      => 'required',

            'email'         => 'required|distinct|unique:users,email',

            'username'      => 'required|distinct|unique:users,username',

            'phone'         => 'required|distinct|unique:users,phone',

        ]);

        $user = new User();

        $user->fullname = trim($request->fullname);

        $user->username = trim($request->username);

        $user->password = trim(bcrypt($request->password));

        $user->email    = trim($request->email);

        $user->phone    = trim($request->phone);

        $user->image_id = 1;

        $user->active   = 1;

        $user->save();

        $role = Role::find($request->role);

        if($role->name == 'super administrador'){

            $roles = Role::all();

            foreach ($roles as $role_) {
                $user->roles()->attach($role_->id);
            }

        }else{

            $user->roles()->attach($role);

        }

        event(new Registered($user));

        $this->registered($request, $user);

        return ucfirst($user->username);
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user){
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $response['user'] = $user;

        $response['role'] = $user->roles[0]->id;

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'id'            => 'required',

            'role'          => 'required',

            'fullname'      => 'required',

            'email'         => 'required',

            'username'      => 'required',

            'phone'         => 'required',

        ]);

        $user = User::findOrFail($request->id);
        if($request->role == 1){
            
            $user->roles()->detach();

            $roles = Role::all();

            foreach ($roles as $role_) {
                $user->roles()->attach($role_->id);
            }

        }else{
            $user->roles()->updateExistingPivot($user->roles[0]->id, ['role_id' => $request->role]);
        }

        $user->fullname = trim($request->fullname);

        $user->username = trim($request->username);

        if($request->password !== null):

            $user->password = trim(bcrypt($request->password));

        endif;

        $user->email    = trim($request->email);

        $user->phone    = trim($request->phone);

        $user->save();

        return ucfirst($user->username);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $message = 'user '. $user->username .' removido';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        Storage::disk('public')->deleteDirectory('images/users/'.$user->id);

        if($user->image->id != 1):

            Image::find($user->image->id)->delete();

        endif;

        $user->roles()->detach();

        $user->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * Status of user
     * @param int $id <id user>
     * @return \Illuminate\Http\Response <AJAX> 
     */

    public function status($id)
    {

        $user = User::findOrFail($id);

        if($user->active == 1):

            $user->active = 0;

            if($user->image->id != 2):

                $user->image->active = 0;

            endif;

            $user->save();

            $textstatus = 'Disable';

        else:

            $user->active = 1;

            if($user->image->id != 2):

                $user->image->active = 1;

            endif;

            $user->save();

            $textstatus = 'Available';

        endif;

        return 'user '.$user->username.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $users = User::all();  

        return $datatables->of($users)
            ->addColumn('full_name', function($user){
                return $user->fullname;
            })
            ->addColumn('rol', function($user){
                return $user->roles[0]->name;
            })
            ->addColumn('created_at', function($user){
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function($user){
                if($user->active == 1):

                    return '<span class="label label-sm label-success"> Available </span>';

                endif;

                return '<span class="label label-sm label-warning"> Disable </span>';
            })
            ->addColumn('avatar', function($user){
                if ($user->image->id == 1){
                    return '<img class="text-center" style="border-radius: 5px !important;" width="50" height="50" src="'.asset($user->image->name).'" alt="'.$user->image->name.'" title="'.$user->image->name.'">';
                }
                return '<img class="rounded-square" width="50" height="50" src="'. asset('storage/images/users/'.$user->id.'/'.$user->image->name) .'" alt="'.$user->image->name.'" title="'.$user->image->name.'">';
            })
            ->addColumn('opciones', function($user){
                if($user->active == 1):
                    return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="'.route('profile.user',$user->username).'" title="Profile">
                                                <i class="fa fa-user"></i> Profile
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$user->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Deshabilitar" onclick="statusData('.$user->id.')">
                                                <i class="fa fa-times"></i> Disable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$user->id.')">
                                                <i class="fa fa-trash"></i> Remove 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    else:
                        return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="'.route('profile.user',$user->username).'" title="Profile">
                                                <i class="fa fa-user"></i> Profile
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$user->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Enable" onclick="statusData('.$user->id.')">
                                                <i class="fa fa-check"></i> Enable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$user->id.')">
                                                <i class="fa fa-trash"></i> Remove 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    endif;
            })->rawColumns(['status', 'opciones', 'avatar'])->make(true);
    }
}
