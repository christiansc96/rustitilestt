<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;
use App\Product;
use App\Color;
use App\Size;
use App\Space;
use App\Uso;
use App\Image;
use App\Price;
use App\AntiSlip;
use App\Brand;
use App\Pasta;
use App\Texture;
use App\Manufacturer;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors     = Color::all();
        $sizes      = Size::all();
        $spaces     = Space::all();
        $uses       = Uso::all();
        $antislips  = AntiSlip::all();
        $pastas     = Pasta::all();
        $brands     = Brand::all();
        $textures   = Texture::all();
        $manufacturers = Manufacturer::all();

        return view('backend.products.products', compact('colors', 'sizes', 'spaces', 'uses', 'antislips', 'pastas', 'brands', 'textures', 'manufacturers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $product                        =   new Product();

        $product->name                  =   trim($request->name);

        $product->slug                  =   Str::slug(trim($request->name));

        $product->description           =   trim($request->description);

        $product->short_description     =   trim($request->short_description);

        $product->packing               =   trim($request->packing);

        $product->code                  =   trim($request->code);

        $product->maintenance           =   trim($request->maintenance);

        $product->pei                   =   trim($request->pei);

        $product->manufacturer_id       =   trim($request->manufacturer);

        $product->texture_id            =   trim($request->texture);

        $product->antislip_id           =   trim($request->antislip);

        $product->pasta_id              =   trim($request->pasta);

        $product->brand_id              =   trim($request->brand);

        $product->info                  =   trim($request->info);

        $product->use_id                =   trim($request->use);

        $product->space_id              =   trim($request->space);

        $price                          =   Price::where([
                                                ['price', '=',trim($request->price)],
                                                ['coin', '=', trim($request->coin)]
                                            ])->first();

        if($price == null):

            $price                  = new Price();

            $price->price           = trim($request->price);

            $price->coin           = trim($request->coin);

            $price->active          = 1;

            $price->save();

        endif;

        $product->price_id      = $price->id;

        $product->active        = 1;

        $product->save();

        foreach($request->colors as $color):

            $product->color()->attach($color);

        endforeach;

        foreach($request->sizes as $size):

            $product->size()->attach($size);

        endforeach;

        if ($request->hasFile('images')):

            foreach($request->file('images') as $_image):

                $image          = new Image();

                $image->name    = $_image->getClientOriginalName();

                $image->active  = 1;

                $image->save();

                $image->product()->attach($product->id);

                $_image->storeAs('images/products/'.$product->id.'/', $_image->getClientOriginalName(), 'public');

            endforeach;

        endif;

        return 'product creado!';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        return view('backend.products.description.description-product', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response['product'] = Product::findOrFail($id);

        $response['manufacturer'] = $response['product']->manufacturer;

        $response['sizes'] = $response['product']->size;

        $response['colors'] = $response['product']->color;

        $response['price'] = $response['product']->price;

        $response['texture'] = $response['product']->texture;

        $response['antislip'] = $response['product']->antislip;

        $response['brand'] = $response['product']->brand;

        $response['pasta'] = $response['product']->pasta;

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product                        = Product::findOrFail($id);

        $product->name                  = trim($request->name);

        $product->slug                  = Str::slug(trim($request->name));

        $product->short_description     = trim($request->short_description);

        $product->description           = trim($request->description);

        $product->info                  = trim($request->info);

        $product->packing               = trim($request->packing);

        $product->manufacturer_id       = trim($request->manufacturer);

        $product->code                  = trim($request->code);

        $product->maintenance           = trim($request->maintenance);

        $product->pei                   = trim($request->pei);

        $product->texture_id            = trim($request->texture);

        $product->antislip_id           = trim($request->antislip);

        $product->pasta_id              = trim($request->pasta);

        $product->brand_id              = trim($request->brand);

        $product->use_id                = trim($request->use);

        $product->space_id              = trim($request->space);

        $product->active                = 1;

        $price                          =   Price::where([
                                                ['price', '=',trim($request->price)],
                                                ['coin', '=', trim($request->coin)]
                                            ])->first();

        if($price == null):

            $price                  = new Price();

            $price->price           = trim($request->price);

            $price->coin           = trim($request->coin);

            $price->active          = 1;

            $price->save();

        endif;

        $product->price_id      = $price->id;

        $product->active        = 1;

        $product->save();

        $product->color()->detach();

        foreach ($request->colors as $color):

            $product->color()->attach($color);

        endforeach;

        $product->size()->detach();

        foreach ($request->sizes as $size):

            $product->size()->attach($size);

        endforeach;

        return 'product actualizado!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        $message = 'product '. $product->name .' removed';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        Storage::disk('public')->deleteDirectory('images/products/'.$product->id);

        $product->image()->detach();

        $product->size()->detach();

        $product->color()->detach();

        $product->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * [status description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function status($id)
    {

        $product = Product::findOrFail($id);

        if($product->active == 1):

            $product->active = 0;

            $product->save();

            $textstatus = 'Disable';

        else:

            $product->active = 1;

            $product->save();

            $textstatus = 'Available';

        endif;

        return 'product '.$product->name.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $products = Product::all();

        return $datatables->of($products)
            ->addColumn('price', function($product){

                return $product->price->price.' '.$product->price->coin;
            })
            ->addColumn('color', function($product){

                foreach($product->color as $color):

                    $_color[] = $color->name;

                endforeach;

                return $_color;
            })
            ->addColumn('tamaño', function($product){
                foreach ($product->size as $size): 
                    $_size[] = $size->name;
                endforeach;
                return $_size;
            })
            ->addColumn('espacio', function($product){
                return $product->space->name;
            })
            ->addColumn('uso', function($product){
                return $product->uso->name;
            })
            ->addColumn('status', function($product){
                if($product->active == 1):

                    return '<span class="label label-sm label-success"> Available </span>';

                endif;

                return '<span class="label label-sm label-warning"> Disable </span>';
            })
            ->addColumn('opciones', function($product){
                if($product->active == 1):
                    return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="'.route('admin-products.show', $product->id).'" title="Description">
                                                <i class="fa fa-eye"></i> Description
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$product->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$product->id.')">
                                                <i class="fa fa-times"></i> Disable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$product->id.')">
                                                <i class="fa fa-trash"></i> Remove 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    else:
                        return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="'.route('admin-products.show', $product->id).'" title="Description">
                                                <i class="fa fa-eye"></i> Description
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$product->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Enable" onclick="statusData('.$product->id.')">
                                                <i class="fa fa-check"></i> Habilitar 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$product->id.')">
                                                <i class="fa fa-trash"></i> Remove 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    endif;
            })->rawColumns(['status', 'opciones' ])->make(true);
    }

    /**
     * [galery description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function galery(Datatables $datatables, $id){

        $products = Product::findOrFail($id);

        return $datatables->of($products->image)
            ->addColumn('image', function($image){

                return '<img style="border-radius: 5px !important;" class="rounded-square" width="50" height="50" src="'. asset('storage/images/products/'.$image->pivot->product_id.'/'.$image->name) .'">';

            })
            ->addColumn('opciones', function($image){
                return '<div class="btn-group pull-right">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-left" role="menu">
                                    <li>
                                        <a href="javascript:;" title="Remove" onclick="removeData('.$image->id.')">
                                            <i class="fa fa-trash"></i> Remove 
                                        </a>
                                    </li>
                                </ul>
                            </div>';
            })->rawColumns(['image', 'opciones' ])->make(true);
    }

    /**
     * [store_image description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store_image(Request $request)
    {
        $image = new Image();

        $image->name = $request->file('image')->getClientOriginalName();

        $image->active = 1;

        $image->save();
        
        $image->product()->attach($request->product_id);

        return 'Image added!';
    }

    /**
     * [destroy_image description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy_image($id)
    {
        $image = Image::findOrFail($id);

        $message = 'Image '. $image->name .' removed';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        Storage::disk('public')->delete('images/products/'.$image->pivot->product_id.'/'.$image->id);

        $image->product()->detach();

        $image->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;

    }
}
