<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AntiSlip;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class AntiSlipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.anti-slips.anti-slips');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $antislip = new AntiSlip();

        $antislip->name = trim($request->name);

        $antislip->active = 1;

        $antislip->save();

        return 'Antislip created!';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $antislip = AntiSlip::findOrFail($id);

        return $antislip;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $antislip = AntiSlip::findOrFail($id);

        $antislip->name = trim($request->name);

        $antislip->active = 1;

        $antislip->save();

        return 'Antislip updated!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $antislip = AntiSlip::findOrFail($id);

        $message = 'Antislip '. $antislip->name .' removed';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        $antislip->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * [status description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function status($id)
    {

        $antislip = AntiSlip::findOrFail($id);

        if($antislip->active == 1):

            $antislip->active = 0;

            $antislip->save();

            $textstatus = 'Disable';

        else:

            $antislip->active = 1;

            $antislip->save();

            $textstatus = 'Available';

        endif;

        return 'Antislip '.$antislip->name.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $antislips = AntiSlip::all();

        return $datatables->of($antislips)
            ->addColumn('status', function($antislip){
                if($antislip->active == 1):

                    return '<span class="label label-sm label-success"> Available </span>';

                endif;

                return '<span class="label label-sm label-warning"> Disable </span>';
            })
            ->addColumn('opciones', function($antislip){
                if($antislip->active == 1):
                    return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$antislip->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$antislip->id.')">
                                                <i class="fa fa-times"></i> To disable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$antislip->id.')">
                                                <i class="fa fa-trash"></i> Trash 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    else:
                        return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$antislip->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$antislip->id.')">
                                                <i class="fa fa-times"></i> Enable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$antislip->id.')">
                                                <i class="fa fa-trash"></i> Trash 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    endif;
            })->rawColumns(['status', 'opciones' ])->make(true);
    }
}
