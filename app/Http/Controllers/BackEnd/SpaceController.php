<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Space;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class SpaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.spaces.spaces');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $space = new Space();

        $space->name = trim($request->name);

        $space->active = 1;

        $space->save();

        return 'Space created!';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $space = Space::findOrFail($id);

        return $space;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $space = Space::findOrFail($id);

        $space->name = trim($request->name);

        $space->active = 1;

        $space->save();

        return 'Space updated!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $space = Space::findOrFail($id);

        $message = 'Space '. $space->name .' removed';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        $space->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * [status description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function status($id)
    {

        $space = Space::findOrFail($id);

        if($space->active == 1):

            $space->active = 0;

            $space->save();

            $textstatus = 'Disable';

        else:

            $space->active = 1;

            $space->save();

            $textstatus = 'Enable';

        endif;

        return 'Space '.$space->name.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $spaces = Space::all();

        return $datatables->of($spaces)
            ->addColumn('status', function($space){
                if($space->active == 1):

                    return '<span class="label label-sm label-success"> Enable </span>';

                endif;

                return '<span class="label label-sm label-warning"> Disable </span>';
            })
            ->addColumn('opciones', function($space){
                if($space->active == 1):
                    return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$space->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$space->id.')">
                                                <i class="fa fa-times"></i> Disable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$space->id.')">
                                                <i class="fa fa-trash"></i> Remove 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    else:
                        return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$space->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Enable" onclick="statusData('.$space->id.')">
                                                <i class="fa fa-check"></i> Enable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$space->id.')">
                                                <i class="fa fa-trash"></i> Remove 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    endif;
            })->rawColumns(['status', 'opciones' ])->make(true);
    }
}
