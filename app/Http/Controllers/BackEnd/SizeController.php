<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Size;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class SizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.sizes.sizes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $size = new Size();

        $size->name = trim($request->name);

        $size->active = 1;

        $size->save();

        return 'Size agregado!';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $size = Size::findOrFail($id);

        return $size;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $size = Size::findOrFail($id);

        $size->name = trim($request->name);

        $size->active = 1;

        $size->save();

        return 'Size updated!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $size = Size::findOrFail($id);

        $message = 'Size '. $size->name .' removido';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        $size->product()->detach();

        $size->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * [status description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function status($id)
    {

        $size = Size::findOrFail($id);

        if($size->active == 1):

            $size->active = 0;

            $size->save();

            $textstatus = 'Disable';

        else:

            $size->active = 1;

            $size->save();

            $textstatus = 'Available';

        endif;

        return 'size '.$size->name.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $sizes = Size::all();

        return $datatables->of($sizes)
            ->addColumn('status', function($size){
                if($size->active == 1):

                    return '<span class="label label-sm label-success"> Available </span>';

                endif;

                return '<span class="label label-sm label-warning"> Disable </span>';
            })
            ->addColumn('opciones', function($size){
                if($size->active == 1):
                    return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$size->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$size->id.')">
                                                <i class="fa fa-times"></i> Disable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$size->id.')">
                                                <i class="fa fa-trash"></i> Remove 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    else:
                        return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$size->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Enable" onclick="statusData('.$size->id.')">
                                                <i class="fa fa-check"></i> Enable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$size->id.')">
                                                <i class="fa fa-trash"></i> Remove 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    endif;
            })->rawColumns(['status', 'opciones' ])->make(true);
    }
}
