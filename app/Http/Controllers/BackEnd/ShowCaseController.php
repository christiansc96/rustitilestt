<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\ShowCase;
use App\Image;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class ShowCaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.showcases.showcase');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = new Image();

        $image->name = $request->file('image')->getClientOriginalName();

        $image->active = 1;

        $image->save();
        
        $showcase = new ShowCase();

        $showcase->heading = trim($request->heading);

        $showcase->description = trim($request->description);

        $showcase->image_id = $image->id;

        $showcase->active = 1;

        $showcase->save();

        $request->file('image')->storeAs('images/show-cases/'.$showcase->id.'/', $request->file('image')->getClientOriginalName(), 'public');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $showcase = ShowCase::findOrFail($id);

        return $showcase;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $showcase = ShowCase::findOrFail($id);

        $showcase->heading = trim($request->heading);

        $showcase->description = trim($request->description);

        if($request->file('image') !== null){

            Storage::disk('public')->delete('images/show-cases/'.$showcase->id.'/'.$showcase->image->name);

            $request->file('image')->storeAs('images/show-cases/'.$showcase->id.'/', $request->file('image')->getClientOriginalName(), 'public');

            $image = Image::findOrFail($showcase->image->id);

            $image->name = $request->file('image')->getClientOriginalName();

            $image->save();
        }

        $message = 'showcase '.$showcase->name.' updated!';

        $showcase->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $showcase = ShowCase::findOrFail($id);

        $message = 'showcase '. $showcase->name .' removed';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        Storage::disk('public')->delete('images/show-cases/'.$showcase->id.'/'.$showcase->name);

        Image::where('id', $showcase->image_id)->delete();

        $showcase->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * [status description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function status($id)
    {

        $showcase = ShowCase::findOrFail($id);

        if($showcase->active == 1):

            $showcase->active = 0;

            $showcase->save();

            $textstatus = 'Disable';

        else:

            $showcase->active = 1;

            $showcase->save();

            $textstatus = 'Available';

        endif;

        return 'showcase '.$showcase->name.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $showcases = ShowCase::all();

        return $datatables->of($showcases)
            ->addColumn('image', function($showcase){

                return '<img title="'.$showcase->image->name.'" alt="'.$showcase->image->name.'" style="border-radius: 1px !important;" class="rounded-square" width="200" height="200" src="'. asset('storage/images/show-cases/'.$showcase->id.'/'.$showcase->image->name.'') .'">';

            })
            ->addColumn('status', function($showcase){
                if($showcase->active == 1):

                    return '<span class="label label-sm label-success"> Available </span>';

                endif;

                return '<span class="label label-sm label-warning"> Disable </span>';
            })
            ->addColumn('opciones', function($showcase){
            	if($showcase->active == 1):
	                return '<div class="btn-group pull-right">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-left" role="menu">
                                    <li>
                                        <a href="javascript:;" title="Edit" onclick="editForm('.$showcase->id.')">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    </li>
                                	<li>
                                        <a href="javascript:;" title="Disable" onclick="statusData('.$showcase->id.')">
                                            <i class="fa fa-times"></i> Disable 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" title="Remove" onclick="removeData('.$showcase->id.')">
                                            <i class="fa fa-trash"></i> Removed 
                                        </a>
                                    </li>
                                </ul>
	                        </div>';
	            else:

	            	return '<div class="btn-group pull-right">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-left" role="menu">
                                    <li>
                                        <a href="javascript:;" title="Edit" onclick="editForm('.$showcase->id.')">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    </li>
                                	<li>
                                        <a href="javascript:;" title="Enable" onclick="statusData('.$showcase->id.')">
                                            <i class="fa fa-check"></i> Enable 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" title="Remove" onclick="removeData('.$showcase->id.')">
                                            <i class="fa fa-trash"></i> Removed 
                                        </a>
                                    </li>
                                </ul>
	                        </div>';

	            endif;
            })->rawColumns(['image', 'opciones', 'status' ])->make(true);
    }
}
