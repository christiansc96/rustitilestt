<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.brands.brands');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand = new Brand();

        $brand->name = trim($request->name);

        $brand->active = 1;

        $brand->save();

        return 'Brand Created!';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::findOrFail($id);

        return $brand;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = Brand::findOrFail($id);

        $brand->name = trim($request->name);

        $brand->active = 1;

        $brand->save();

        return 'Brand updated!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);

        $message = 'Brand '. $brand->name .' removed';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        $brand->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * [status description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function status($id)
    {

        $brand = Brand::findOrFail($id);

        if($brand->active == 1):

            $brand->active = 0;

            $brand->save();

            $textstatus = 'Disable';

        else:

            $brand->active = 1;

            $brand->save();

            $textstatus = 'Available';

        endif;

        return 'Brand '.$brand->name.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $brands = Brand::all();

        return $datatables->of($brands)
            ->addColumn('status', function($brand){
                if($brand->active == 1):

                    return '<span class="label label-sm label-success"> Available </span>';

                endif;

                return '<span class="label label-sm label-warning"> Disable </span>';
            })
            ->addColumn('opciones', function($brand){
                if($brand->active == 1):
                    return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$brand->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$brand->id.')">
                                                <i class="fa fa-times"></i> To disable 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$brand->id.')">
                                                <i class="fa fa-trash"></i> Trash 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    else:
                        return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$brand->id.')">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Enable" onclick="statusData('.$brand->id.')">
                                                <i class="fa fa-check"></i> Enable
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$brand->id.')">
                                                <i class="fa fa-trash"></i> Trash 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    endif;
            })->rawColumns(['status', 'opciones' ])->make(true);
    }
}
