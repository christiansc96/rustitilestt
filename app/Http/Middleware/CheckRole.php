<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (Auth::guard('clients')->check())
        {
            return $next($request);
        }
        if(!$request->user()->hasRole($role)):

            return redirect('/admin/dashboard');
            
        endif;
        return $next($request);
    }
}
