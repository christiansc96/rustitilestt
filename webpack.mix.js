let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js/fronten')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
      'public/css/bootstrap.css',
      'public/css/font-awesome.css',
      'public/css/style.css',
      'public/css/media.css',
      'public/css/animate.css',
      'public/css/normalize.css',
      'public/css/owl.carousel.css',
      'public/css/owl.theme.css',
      'public/css/owl.transitions.css',
      'public/css/jquery.bxslider.css',
      'public/css/elegant-icons.css',
      'public/css/magnific-popup.css',
      'public/css/flexslider.css',
      'public/css/flexslider-set.css',
      'public/css/jquery-ui.css',
      'public/assets/sweetalert2/sweetalert2.min.css'
   ], 'public/css/all.css');

mix.scripts([
   'public/js/lib/jquery.min.js',
   'public/js/vendors/bootstrap.min.js',
   'public/js/lib/jquery.waypoints.min.js',
   'public/js/lib/moderniz.min.js',
   'public/js/scripts.js',
   'public/js/vendors/owl.carousel.min.js',
   'public/js/vendors/jquery.bxslider.min.js',
   'public/js/vendors/jquery.magnific-popup.min.js',
   'public/js/vendors/jquery.flexslider-min.js',
   'public/js/vendors/jquery-ui.js',
   'public/js/vendors/flexslider-init.j',
   'public/js/vendors/smoothscroll.js',
   'public/js/frontend/products-detail/js.cookie.min.js',
   'public/js/frontend/products-detail/woocomerce.min.js',
   'public/js/frontend/products-detail/jquery.magnific-popup.min.js',
   'public/js/frontend/products-detail/slick.min.js',
   'public/js/frontend/products-detail/wp-igsp-pro-public.js',
   'public/js/frontend/products-detail/woocomerce.min.js',
   'public/js/frontend/products-detail/woocomerce.min.js'
], 'public/js/all.js');

if(mix.config.inProduction){
   mix.version();
}



   

   