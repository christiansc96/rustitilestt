@extends('layouts.front')
@section('title')
{{ $exception->getMessage() }}
@endsection
@section('styles')
@endsection
@section('content')
<!-- Page Header Begins -->

<div id="page-header">
    <div class="header-bg-parallax" style="margin-top: -90px;">
        <div class="overlay">
            <div class="container text-center">
                <div class="header-description">
                    <span style="font-size: 7rem;"><i class="fa fa-ban" style="color: #ccc;"></i></span><br><br>
                    <h1>
                        {{ $exception->getMessage() }}
                        {{-- 404 Page not found. --}}
                    </h1>
                    {{-- <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="{{ route('fe.home') }}">
                                    Home
                                </a>
                            </li>
                        </ul>
                    </div> --}}
                    <!-- /header-small-nav -->
                </div>
                <!-- /header-description -->
            </div>
            <!-- /container -->
        </div>
        <!-- /overlay -->
    </div>
    <!-- /product-filter -->
</div>

<!-- Page Header End -->

<!-- Product Item List Begin -->

{{-- <div class="product-item-list">
    <div class="container-fluid padding-vertical-60">
    <div class="row" style="padding:0 25px">
        <!-- Siderbar -->
        <!-- /sidebar -->

        <!-- Product Item List Begin -->

        <div class="product-item-list col-md-12 col-sm-8 padding-top-20 text-center">


			

			<span style="font-size: 1.2rem;"></span>


        </div>
        <!-- /product-item-list -->
    </div>


    </div>
    <!-- /container -->
</div> --}}

<!-- Product Item List End -->

<!-- Footer Begin -->

@endsection
@section('scripts')
@endsection