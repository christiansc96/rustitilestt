<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    {{-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130786499-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-130786499-1');
    </script> --}}

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <meta name="description" content="Tu Cerámica">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,200i,300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

    <!-- Page Title -->
    <title>{{ config('app.name') }} | @yield('title')</title>

    <!-- Favicon -->
    <link rel="icon" href="img/favicon.png" type="image/png" />

    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <link rel="stylesheet" href="{{asset('css/media.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.transitions.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery.bxslider.css')}}">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}">
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('css/flexslider.css')}}">
    <link rel="stylesheet" href="{{asset('css/flexslider-set.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
    <script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>
    <link href="{{ asset('assets/sweetalert2/sweetalert2.min.css') }}">
{{-- 
    <script src="{{asset('js/all.js')}}"></script>
    <link src="{{asset('css/all.css')}}" rel="stylesheet"> --}}


    @yield('styles')

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- Loader Begin -->

    <div class="loader">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>

    <!-- Loader End -->

    <!-- Back To Top Begin -->

    <div id="back-top">
        <a href="#" class="scroll">
            <i class="arrow_carrot-up"></i>
        </a>
    </div>

    <!-- Back To Top End -->

    <!-- Site Wrapper Begin -->

    <div class="wrapper">

        <!-- Header Begin -->

        <header>
            <nav class="main-nav menu-dark menu-transparent nav-transparent">
                <div class="col-md-12">
                    <div class="navbar">
                        <div class="brand-logo">
                            <a href="{{ route('fe.home') }}" class="navbar-brand">
                                <img src="{{asset('img/logo_rusti.png')}}" alt="Rustitiles" />
                            </a>
                        </div>
                        <!-- brand-logo -->

                        <div class="navbar-header">
                            <div class="inner-nav right-nav">
                                <ul class="rightnav-links">
                                    @if(Request::segment(1) != 'rustitiles-ceramic-online')
                                        <li>
                                            <a href="#" id="search-trigger"><i class="fa fa-search"></i></a>
                                            <form action="#" id="search" method="get" class="">
                                                <div class="input">
                                                    <div class="container">
                                                        <input class="search" placeholder="Consiga el producto que busca..." type="text" id="buscar_producto">
                                                        <input type="hidden" name="producto_idproducto" id="producto_idproducto">
                                                        <div id="autocomplete_producto_container"></div>
                                                        <button class="submit" type="submit" value="close"><i class="fa fa-search"></i>
                                                        </button>
                                                    </div>
                                                    <!-- /container -->
                                                </div>
                                                <!-- /input -->
                                                <button class="icon_close" id="close" type="reset"></button>
                                            </form>
                                            <!-- /form -->
                                        </li>
                                    @endif
                                    
                                    @if (!Auth::guest() || Auth::guard('clients')->check())
                                        <li>
                                            <a href="javascript:;" class="side-cart-toggle">
                                                <i class="fa fa-shopping-cart"></i>
                                                <span class="notice-num" ><p id="count">{{Cart::instance('tuceramica')->count()}}</p></span>
                                            </a>
                                        </li>
                                    @endif
                                        
                                    <li>
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse">
                                            <span class="sr-only"></span>
                                            <i class="fa fa-bars"></i>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <!-- /right-nav -->
                        </div>
                        <!-- navbar-header -->

                        <div class="custom-collapse navbar-collapse collapse inner-nav margin-left-100 pull-right">
                            <ul class="nav navbar-nav nav-links">
                                <li class="dropdown classic-dropdown"><a href="http://tuceramica.com/" class="dropdown-toggle">TUCERÁMICA</a></li>
                                {{-- <li class="dropdown classic-dropdown"><a href="{{route('fe.home')}}" class="dropdown-toggle">Home</a></li> --}}
                                <!-- /dropdown -->
                                <!-- /dropdown -->

                                <li class="dropdown classic-dropdown"><a href="{{ route('fe.products') }}" class="dropdown-toggle" {{-- data-toggle="dropdown" --}}>Products {{-- <i class="fa fa-angle-down"></i> --}}</a>
                                    {{-- <ul class="dropdown-menu">
                                        <li class="submenu dropdown">
                                            <a href="productos.html" class="dropdown-toggle" data-toggle="dropdown">Categoria uno</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="productos.html">Sub-categoria uno</a>
                                                </li>
                                                <li><a href="productos.html">Sub-categoria dos</a>
                                                </li>
                                            </ul>
                                            <!-- /dropdown-menu -->
                                        </li>
                                        <!-- /submenu -->
                                        <li><a href="{{ route('fe.products') }}">Categoria dos</a>
                                        </li>
                                        <li><a href="productos.html">Categoria tres</a>
                                        </li>
                                    </ul> --}}
                                    <!-- /dropdown-menu -->
                                </li>
                                <!-- /dropdown -->

                                {{-- <li class="dropdown classic-dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog</a></li> --}}
                                <!-- /dropdown -->

                                {{-- <li class="dropdown classic-dropdown"><a href="{{ route('fe.directory') }}" class="dropdown-toggle">Directory</a></li> --}}
                                <li class="dropdown classic-dropdown"><a href="{{ route('fe.about_us') }}" class="dropdown-toggle">About Us</a></li>
                                <li class="dropdown classic-dropdown"><a href="{{ route('fe.contact') }}" class="dropdown-toggle">Contact</a></li>
                                @if(Auth::guest())
                                    @if (Auth::guard('clients')->check())

                                        <li class="dropdown classic-dropdown"><a href="#" class="dropdown-toggle" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}</a></li>
                                        <form id="logout-form" action="{{ route('fe.logout') }}" method="POST" style="display: none;">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </form>
                                    @else
                                        <li class="dropdown classic-dropdown"><a href="{{ route('fe.showLoginForm') }}" class="dropdown-toggle">Login</a></li>
                                    @endif
                                @else
                                    <li class="dropdown classic-dropdown"><a href="#" class="dropdown-toggle" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}</a></li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                @endif
                                <!-- /dropdown -->
                                <!-- /dropdown -->
                            </ul>
                            <!-- /nav -->
                        </div>
                        <!-- /collapse -->
                    </div>
                    <!-- /navbar -->
                    
                    @include('frontend.cart')

                </div>
                <!-- /container -->
            </nav>
            <!-- /nav -->
        </header>

        <!-- Header End -->

        @yield('content')

        <!-- Footer Begin -->

        <footer class="footer-2">
            <div class="container">
                <div class="row">
                    <div class="footer-social text-center">
                        <ul>
                            <li><a href="#">Facebook</a>
                            </li>
                            <li><a href="#">Twitter</a>
                            </li>
                            <li><a href="#">Instagram</a>
                            </li>
                            <li><a href="#" class="bd-right">YouTube</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /footer-social -->

                    <div class="col-md-4 col-sm-12 about-widget padding-top-50" style="margin-top: 2px;">
                        <img src="img/logo_rusti.png" alt="" />
                        <p>Finally the Best Quality. <br>

                        Come visit us at our showrooms in Trinidad and Tobago!</p>
                        <ul>
                            <li><img src="img/icon-phone.png" alt="" />+1 (868) 220-5681</li>
                            <li><img src="img/icon-mail.png" alt="" />info@rustitilestt.com</li>
                        </ul>
                    </div>
                    <!-- /column -->

                    <div class="col-md-4 col-sm-12 twitter-feed padding-top-40">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3922.1656122895834!2d-61.4076200847004!3d10.566210292464417!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c35feba58220d4f%3A0x21d1656c2a3d5970!2s121+Munroe+Road%2C+Trinidad+y+Tobago!5e0!3m2!1ses!2sve!4v1543334504847" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                            <h4 style="color: #fff;">Cunupia, Trinidad</h4>
                            <p style="color: #fff;" class="margin-top-10">Rustitiles – L.P. No. 121 Lot C, Munroe Road</p>
                    </div>
                    <!-- /column -->

                    <div class="col-md-4 col-sm-12 news padding-top-40">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3922.425685234232!2d-61.415436085201236!3d10.545823592478255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c35f902d0b7477f%3A0x19eff1a7ec1d2b06!2sRustitiles+T%26T+LTD!5e0!3m2!1ses!2sve!4v1543334351396" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                            <h4 style="color: #fff;">Chaguanas, Trinidad</h4>
                            <p style="color: #fff;" class="margin-top-10">John Peter Road / Uriah Butler Highway</p>
                    </div>
                    <!-- /column -->

                </div>
                <!-- /row -->
            </div>
            <!-- /container -->

            <div class="footer-rights">
                <div class="container">
                    <div class="copyright-info text-center">
                        <p class="ft-desc">Copyright 2018 Tuceramica all rights reserved. Desarrollado por<a href="http://arquetitek.com" target="blank"> Arquetitek</a>
                        </p>
                    </div>
                    <!-- /copyright-info -->
                    <div class="footer-payments pull-right">
                        <ul>
                            <li><img src="img/visa.png" alt="" />
                            </li>
                            <li><img src="img/mastercard.png" alt="" />
                            </li>
                            <li><img src="img/discover.png" alt="" />
                            </li>
                            <li><img src="img/americanexpress.png" alt="" />
                            </li>
                            <li><img src="img/paypal.png" alt="" />
                            </li>
                        </ul>
                    </div>
                    <!-- /footer-payments -->
                </div>
                <!-- /container -->
            </div>
            <!-- /footer-rights -->
        </footer>

        <!-- Footer End -->


    </div>

    <!-- Site Wrapper End -->

    <!--- Scripts -->
    
    <script src="{{asset('js/lib/jquery.min.js')}}"></script>
    <script src="{{asset('js/vendors/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('js/lib/moderniz.min.js')}}"></script>
    <script src="{{asset('js/scripts.js')}}"></script>
    <script src="{{asset('js/vendors/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/vendors/jquery.bxslider.min.js')}}"></script>
    <script src="{{asset('js/vendors/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/vendors/jquery.flexslider-min.js')}}"></script>
    <script src="{{asset('js/vendors/jquery-ui.js')}}"></script>
    <script src="{{asset('js/vendors/flexslider-init.js')}}"></script>
    <script src="{{asset('js/vendors/smoothscroll.js')}}"></script>

    @yield('scripts')

    <script type="text/javascript">
      $(document).ready(function(){

        $("#buscar_producto").keyup(function(){
          $.ajax({
              url: '/Autocomplete',
              type:'GET',
              dataType:'json',
              data:{info:$('#buscar_producto').val()}
          }).done(function(data){
            console.log(data);
            if( data != 0){
              $("#producto_idproducto").val(data.id);
              $("#autocomplete_producto_container").fadeIn();
              $("#autocomplete_producto_container").html(data.output);
              $(document).on('click','#opcion_productos', function(){
                // $("#buscar").prop("href", "http://peltca.pro/Productos/"+data.id);
                $("#buscar_producto").val($(this).text());
                $("#autocomplete_producto_container").fadeOut();
              });
            }else{
              $("#buscar").prop("href", "#");
              $("#autocomplete_producto_container").fadeOut();
            }
          });
        });

      });
    </script>

    <script type="text/javascript">
        $(window).on('load', function() {
            $('#newsletterModal').modal('show');
        });

    </script>
</body>
</html>