@extends('layouts.back')

@section('title', 'admin users')

@section('styles')
{{-- dataTables --}}
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css">
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        users
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('be.dashboard') }}">
                            Dashboard
                        </a>
                        <i class="fa fa-circle">
                        </i>
                    </li>
                    <li>
                        <span>
                            users
                        </span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner" id="users">
                    <div class="portlet box dark">
                        <div class="portlet-title">
                            <div class="caption">
                                Ultimos users agregados
                            </div>
                            <a class="btn green-haze btn-outline btn-circle btn-md pull-right" @click="addForm()" style="margin:3px -7px 0 0;">
                                Agregar user
                            </a>
                        </div>
                        <div class="portlet-body flip-scroll">
                            <table class="table table-striped table-bordered table-hover table-condensed flip-content" id="user-table">
                                <thead class="flip-content" style="background:#2D3742; color:#FFFFFF;">
                                    <tr >
                                        <th width="10%">id</th>
                                        <th>Full Name</th>
                                        <th>username</th>
                                        <th>email</th>
                                        <th>phone</th>
                                        <th>role</th>
                                        <th>avatar</th>
                                        <th>status</th>
                                        <th>actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    @include('backend.users.form-user')
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
{!!Form::open(['route'=>[ 'superadmin.datatable.status.user', ':USER_ID'],'method'=>'GET', 'id' => 'form-status'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-users.destroy', ':USER_ID' ],'method'=>'DELETE', 'id' => 'form-delete'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-users.edit', ':USER_ID' ],'method'=>'GET', 'id' => 'form-edit'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-users.update', ':USER_ID' ],'method'=>'PATCH', 'id' => 'form-update'])!!}
{!!Form::close()!!}
@endsection

@section('scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script>
var users = new Vue({
    el: '#users',
    data: {
        id: null,
        role: null,
        fullname: null,
        phone: null,
        username: null,
        email: null,
        password: null,
        confirm_password: null,
        save_method: null,
        method: null,
    },
    methods: {
        addForm: function(){
            users.save_method = 'add';
            users.method = 'POST';
            // $('#role').show();
            // $('#fullname').show();
            // $('#phone').show();
            // $('#email').show();
            // $('#username').show();
            // $('#password').show();
            // $('#confirm_password').show();
            // $('#divider').show();
            $('#form-post').show();
            $('#modal-form').modal('show');
            users.id = null;
            users.fullname = null;
            users.phone = null;
            users.username = null;
            users.email = null;
            users.password = null;
            users.confirm_password = null;
            $('.modal-title').text('Add user');
        },
        modal_form: function(){
            this.$validator.validateAll('form-1').then((result) => {
                if(result){

                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    
                    let formData = new FormData();

                    if (users.save_method == 'add'){

                        var form = $('#form-post');

                        var url = form.attr('action');

                    }else{

                        var form = $('#form-update');

                        var url = form.attr('action').replace(':USER_ID', users.id);

                        formData.append('id', users.id);

                    } 

                    formData.append('_method', users.method);
                    formData.append('role', users.role);
                    formData.append('fullname', users.fullname);
                    formData.append('phone', users.phone);
                    formData.append('username', users.username);
                    formData.append('email', users.email);
                    if(users.password != null){
                        formData.append('password', users.password);
                    }
                    
                    axios.post(url, formData,{
                        headers: {
                            'Content-Type': 'multipart/form-data, application/json;charset=UTF-8'
                        }
                    }).then(function (response) {
                        console.log(response);
                        dialog.modal('hide');
                        table.ajax.reload();
                        swal({
                            title: 'successfully!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                        $('#modal-form').modal('hide');
                    }).catch(function (error) {
                        console.log(error);
                        dialog.modal('hide');
                        var refresh = bootbox.dialog({
                            title: "<p class='text-center'>An error has occurred :(</p>",
                            message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                            closeButton: false,
                            buttons: {
                                refresh: {
                                    label: '<i class="fa fa-refresh"></i> refresh',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                    return;
                }
                bootbox.alert({
                    message: "<p class='text-center'>Must correct errors</p>",
                    backdrop: true,
                    closeButton: false,
                });
            });
        }
    }
});

var table = $('#user-table').DataTable({
              processing: true,
              serverSide: true,
              ajax: "{{ route('superadmin.datatable.users') }}",
              columns: [
                {data: 'id', name: 'id'},
                {data: 'full_name', name: 'full_name'},
                {data: 'username', name: 'username'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},                
                {data: 'rol', name: 'rol'},
                {data: 'avatar', name: 'avatar', orderable: false, searchable: false},
                {data: 'status', name: 'status'},
                {data: 'opciones', name: 'opciones', orderable: false, searchable: false}
              ]
            });

function editForm(id)
{
    var form = $('#form-edit');
    var url = form.attr('action').replace(':USER_ID', id);
    var data = form.serialize();
    users.save_method = 'edit';
    users.method = 'PATCH';
    axios.get(url, data)
    .then(function (response) {
        console.log(response.data);
        $('#modal-form').modal('show');
        $('.modal-title').text('Edit user');
        $('#form-post').show();
        users.id = response.data.user.id;
        users.role = response.data.role;
        users.fullname = response.data.user.fullname;
        users.phone = response.data.user.phone;
        users.username = response.data.user.username;
        users.email = response.data.user.email;
        users.password = null;
        users.confirm_password = null;
    }).catch(function (error) {
        console.log(error);
        var refresh = bootbox.dialog({
            title: "<p class='text-center'>An error has occurred :(</p>",
            message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
            closeButton: false,
            buttons: {
                refresh: {
                    label: '<i class="fa fa-refresh"></i> refresh',
                    callback: function (result) {
                        location.reload(true);
                    }
                }
            }
        });
    });
}

function statusData(id)
{
    var reactive = bootbox.dialog({
        message: "<p class='text-center'>Sure?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    var form = $('#form-status');
                    var url = form.attr('action').replace(':USER_ID', id);
                    var data = form.serialize();
                    axios.get(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'status updated!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refresh = bootbox.dialog({
                            title: "<p class='text-center'>An error has occurred :(</p>",
                            message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                            closeButton: false,
                            buttons: {
                                refresh: {
                                    label: '<i class="fa fa-refresh"></i> refresh',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}
function removeData(id)
{
    var form = $('#form-delete');
    var url = form.attr('action').replace(':USER_ID', id);
    var data = form.serialize();

    var eliminar = bootbox.dialog({
        message: "<p class='text-center'>Sure?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'SI',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    axios.delete(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'Removed!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refresh = bootbox.dialog({
                            title: "<p class='text-center'>An error has occurred :(</p>",
                            message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                            closeButton: false,
                            buttons: {
                                refresh: {
                                    label: '<i class="fa fa-refresh"></i> refresh',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}
</script>
@endsection
