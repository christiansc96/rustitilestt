@extends('layouts.back')

@section('title', 'admin showcase')

@section('styles')
{{-- dataTables --}}
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css">
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Showcases
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('be.dashboard') }}">
                            Dashboard
                        </a>
                        <i class="fa fa-circle">
                        </i>
                    </li>
                    <li>
                        <span>
                            Showcases
                        </span>
                    </li>
                </ul>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="alert alert-info text-center" role="alert">
                            Be sure to upload images with these specific measures: 1920 x 1056. 
                        </div>
                    </div>
                </div>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner" id="showcases">
                    <div class="portlet box dark">
                        <div class="portlet-title">
                            <div class="caption">
                                Latest Showcases
                            </div>
                            <a class="btn green-haze btn-outline btn-circle btn-md pull-right" @click="addForm()" style="margin:3px -7px 0 0;">
                                Add showcase +
                            </a>
                        </div>
                        <div class="portlet-body flip-scroll">
                            <table class="table table-striped table-bordered table-hover table-condensed flip-content" id="showcases-table">
                                <thead class="flip-content" style="background:#2D3742; color:#FFFFFF;">
                                    <tr>
                                        <th>id</th>
                                        <th>header</th>
                                        <th>description</th>
                                        <th>image</th>
                                        <th>status</th>
                                        <th>actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    @include('backend.showcases.form-showcase')
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
{!!Form::open(['route'=>[ 'superadmin.datatable.status.showcase', ':SHOWCASE_ID'],'method'=>'GET', 'id' => 'form-status'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-showcases.destroy', ':SHOWCASE_ID' ],'method'=>'DELETE', 'id' => 'form-delete'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-showcases.edit', ':SHOWCASE_ID' ],'method'=>'GET', 'id' => 'form-edit'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-showcases.update', ':SHOWCASE_ID' ],'method'=>'PATCH', 'id' => 'form-update'])!!}
{!!Form::close()!!}
@endsection

@section('scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script>
var showcases = new Vue({
    el: '#showcases',
    data: {
        id: null,
        heading: null,
        description: null,
        save_method: null,
        method: null,
    },
    methods: {
        addForm: function(){
            showcases.id = null;
            showcases.save_method = 'add';
            showcases.method = 'POST';
            showcases.heading = null;
            showcases.description = null;
            $('#form-post').show();
            $('#modal-form').modal('show');
            document.getElementById("image").value = "";
            $('.modal-title').text('Add showcase');
        },
        modal_form: function(){
            this.$validator.validateAll('form-1').then((result) => {
                if(result){

                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    
                    let formData = new FormData();

                    if (showcases.save_method == 'add'){

                        var form = $('#form-post');

                        var url = form.attr('action');

                        formData.append('image', document.getElementById('image').files[0]);

                    }else{
                        var form = $('#form-update');

                        var url = form.attr('action').replace(':SHOWCASE_ID', showcases.id);

                        formData.append('image', document.getElementById('image').files[0]);

                        formData.append('id', showcases.id);
                    }

                    formData.append('_method', showcases.method);
                    formData.append('heading', showcases.heading);
                    formData.append('description', showcases.description);
                    
                    axios.post(url, formData,{
                        headers: {
                            'Content-Type': 'multipart/form-data, application/json;charset=UTF-8'
                        }
                    }).then(function (response) {
                        console.log(response);
                        dialog.modal('hide');
                        table.ajax.reload();
                        swal({
                            title: 'successfully!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                        $('#modal-form').modal('hide');
                    }).catch(function (error) {
                        console.log(error);
                        dialog.modal('hide');
                        var refresh = bootbox.dialog({
                            title: "<p class='text-center'>An error has occurred :(</p>",
                            message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                            closeButton: false,
                            buttons: {
                                refresh: {
                                    label: '<i class="fa fa-refresh"></i> refresh',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                    return;
                }
                bootbox.alert({
                    message: "<p class='text-center'>Must correct errors</p>",
                    backdrop: true,
                    closeButton: false,
                });
            });
        }
    }
});

var table = $('#showcases-table').DataTable({
              processing: true,
              serverSide: true,
              ajax: "{{ route('superadmin.datatable.showcases') }}",
              columns: [
                {data: 'id', name: 'id'},
                {data: 'heading', name: 'heading'},
                {data: 'description', name: 'description'},
                {data: 'image', name: 'image'},
                {data: 'status', name: 'status'},
                {data: 'opciones', name: 'opciones', orderable: false, searchable: false}
              ]
            });

function editForm(id)
{
    var form = $('#form-edit');
    var url = form.attr('action').replace(':SHOWCASE_ID', id);
    var data = form.serialize();
    showcases.save_method = 'edit';
    showcases.method = 'PATCH';
    axios.get(url, data)
    .then(function (response) {
        $('#modal-form').modal('show');
        $('.modal-title').text('Edit showcase');
        showcases.id = response.data.id;
        showcases.heading = response.data.heading;
        showcases.description = response.data.description;
        document.getElementById("image").value = "";
    }).catch(function (error) {
        console.log(error);
            dialog.modal('hide');
            var refresh = bootbox.dialog({
                title: "<p class='text-center'>An error has occurred :(</p>",
                message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                closeButton: false,
                buttons: {
                    refresh: {
                        label: '<i class="fa fa-refresh"></i> refresh',
                        callback: function (result) {
                            location.reload(true);
                        }
                    }
                }
            });
    });
}

function statusData(id)
{
    var reactive = bootbox.dialog({
        message: "<p class='text-center'>Sure?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    var form = $('#form-status');
                    var url = form.attr('action').replace(':SHOWCASE_ID', id);
                    var data = form.serialize();
                    axios.get(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'status updated!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refresh = bootbox.dialog({
                            title: "<p class='text-center'>An error has occurred :(</p>",
                            message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                            closeButton: false,
                            buttons: {
                                refresh: {
                                    label: '<i class="fa fa-refresh"></i> refresh',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}
function removeData(id)
{
    var form = $('#form-delete');
    var url = form.attr('action').replace(':SHOWCASE_ID', id);
    var data = form.serialize();

    var eliminar = bootbox.dialog({
        message: "<p class='text-center'>Sure?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'SI',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    axios.delete(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'Removido!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refresh = bootbox.dialog({
                            title: "<p class='text-center'>An error has occurred :(</p>",
                            message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                            closeButton: false,
                            buttons: {
                                refresh: {
                                    label: '<i class="fa fa-refresh"></i> refresh',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}
</script>
@endsection
