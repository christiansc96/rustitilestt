<div class="modal" id="modal-form" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            {!!Form::open(['route'=>[ 'admin-showcases.store'],'method'=>'POST', 'id' => 'form-post', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'files'=>true, 'data-vv-scope' => 'form-1'])!!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> &times; </span>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    
                    <div class="form-group" :class="{'has-error': errors.has('form-1.fullname') }" id="heading">
                        <label for="heading" class="col-md-3 control-label">Header</label>
                        <div class="col-md-6">
                            <input class="form-control" name="heading" type="text" v-model="heading" v-validate="'required|max:255'"/>
                            <span v-show="errors.has('form-1.heading')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.heading') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('description') }">
                        <label for="description" class="col-md-3 control-label">Description</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('description')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('description')" data-container="body"></i>
                                <textarea name="description" class="form-control" style="resize: none;" rows="3" v-model="description" v-validate="'required|max:191'"></textarea>
                            </div>
                            <span v-show="errors.has('description')" style="color: red;" class="help is-danger">
                                @{{ errors.first('description') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('image') }" id="s-images">
                        <label for="image" class="col-md-3 control-label">Image</label>
                        <div class="col-md-6">
                            <input type="file" id="image" name="image" class="form-control" v-validate="'required'|'mimes:image/jpg,image/jpeg,image/png'">
                            <span v-show="errors.has('image')" style="color: red;" class="help is-danger">
                                @{{ errors.first('image') }}
                            </span>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" @click="modal_form()" class="btn btn-primary btn-save">Send</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            {!!Form::close()!!}
        </div>
    </div>
</div>
