@extends('layouts.back')

@section('title', 'admin Uses')

@section('styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
{{-- dataTables --}}
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css">
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Uses
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('be.dashboard') }}">
                            Dashboard
                        </a>
                        <i class="fa fa-circle">
                        </i>
                    </li>
                    <li>
                        <a href="{{ route('admin-products.index') }}">
                            Products
                        </a>
                        <i class="fa fa-circle">
                        </i>
                    </li>
                    <li>
                        <span>
                            uses
                        </span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner" id="uses" >
                    <div class="portlet box dark">
				        <div class="portlet-title">
				            <div class="caption">
				                Latest uses
				            </div>
				            <a class="btn green-haze btn-outline btn-circle btn-md pull-right" @click="addForm()" style="margin:3px -7px 0 0;">
				                Add use +
				            </a>
				        </div>
				        <div class="portlet-body flip-scroll">
				            <table class="table table-striped table-bordered table-hover table-condensed flip-content" id="use-table">
				                <thead class="flip-content" style="background:#2D3742; color:#FFFFFF;">
				                    <tr>
                                        <th>id</th>
				                        <th>name</th>
				                        <th width="10%">status</th>
				                        <th width="10%">actions</th>
				                    </tr>
				                </thead>
				            </table>
				        </div>
				    </div>
				    @include('backend.uses.form-use')
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
{!!Form::open(['route'=>[ 'superadmin.datatable.status.use', ':USE_ID'],'method'=>'GET', 'id' => 'form-status'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-uses-attr.destroy', ':USE_ID' ],'method'=>'DELETE', 'id' => 'form-delete'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-uses-attr.edit', ':USE_ID' ],'method'=>'GET', 'id' => 'form-edit'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-uses-attr.update', ':USE_ID' ],'method'=>'PATCH', 'id' => 'form-update'])!!}
{!!Form::close()!!}
<!-- END CONTAINER -->
@endsection

@section('scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>

<script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script>
var use = new Vue({
    el: '#uses',
    data: {
        id: null,
        name: null,
        save_method: null,
        method: null,
    },
    methods: {
        addForm: function(){
            use.save_method = 'add';
            use.method = 'POST';
            $('#modal-form').modal('show');
            use.id = null;
            use.name = null;
            $('.modal-title').text('Add use');
        },
        modal_form: function(){

            this.$validator.validateAll().then((result) => {
                if(result){

                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });

                    let formData = new FormData();

                    if (use.save_method == 'add'){

                        var form = $('#form-post');

                        var url = form.attr('action');

                    }else{

                        var form = $('#form-update');

                        var url = form.attr('action').replace(':USE_ID', use.id);

                        formData.append('id', use.id);

                    } 

                    
                    formData.append('_method', use.method);
                    formData.append('name', use.name);

                    axios.post(url, formData,{
                        headers: {
                            'Content-Type': 'multipart/form-data, application/json;charset=UTF-8'
                        }
                    }).then(function (response) {
                        console.log(response);
                        dialog.modal('hide');
                        table.ajax.reload();
                        swal({
                            title: 'Successfully!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                        $('#modal-form').modal('hide');
                    }).catch(function (error) {
                        console.log(error);
                        dialog.modal('hide');
                        var refresh = bootbox.dialog({
                            title: "<p class='text-center'>An error has occurred :(</p>",
                            message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                            closeButton: false,
                            buttons: {
                                refresh: {
                                    label: '<i class="fa fa-refresh"></i> refresh',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                    return;
                }
                bootbox.alert({
                    message: "<p class='text-center'>Must correct errors</p>",
                    backdrop: true,
                    closeButton: false,
                });
            });
        }
    }
});

var table = $('#use-table').DataTable({
				processing: true,
				serverSide: true,
				ajax: "{{ route('superadmin.datatable.uses') }}",
				columns: [
                    {data: 'id', name: 'id'},
					{data: 'name', name: 'name'},
					{data: 'status', name: 'status'},
					{data: 'opciones', name: 'opciones', orderable: false, searchable: false}
				]
            });

function editForm(id)
{
    var form = $('#form-edit');
    var url = form.attr('action').replace(':USE_ID', id);
    var data = form.serialize();
    use.save_method = 'edit';
    use.method = 'PATCH';
    axios.get(url, data)
    .then(function (response) {
        $('#modal-form').modal('show');
        $('.modal-title').text('Edit use');
        use.id = response.data.id;
        use.name = response.data.name;
    }).catch(function (error) {
        console.log(error);
            dialog.modal('hide');
            var refresh = bootbox.dialog({
                title: "<p class='text-center'>An error has occurred :(</p>",
                message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                closeButton: false,
                buttons: {
                    refresh: {
                        label: '<i class="fa fa-refresh"></i> refresh',
                        callback: function (result) {
                            location.reload(true);
                        }
                    }
                }
            });
    });
}

function statusData(id)
{
    var reactive = bootbox.dialog({
        message: "<p class='text-center'>Sure?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    var form = $('#form-status');
                    var url = form.attr('action').replace(':USE_ID', id);
                    var data = form.serialize();
                    axios.get(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'status updated!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refresh = bootbox.dialog({
			                title: "<p class='text-center'>An error has occurred :(</p>",
			                message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
			                closeButton: false,
			                buttons: {
			                    refresh: {
			                        label: '<i class="fa fa-refresh"></i> refresh',
			                        callback: function (result) {
			                            location.reload(true);
			                        }
			                    }
			                }
			            });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}

function removeData(id)
{
    var form = $('#form-delete');
    var url = form.attr('action').replace(':USE_ID', id);
    var data = form.serialize();

    var eliminar = bootbox.dialog({
        message: "<p class='text-center'>Sure?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    axios.delete(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'Removed!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refresh = bootbox.dialog({
			                title: "<p class='text-center'>An error has occurred :(</p>",
			                message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
			                closeButton: false,
			                buttons: {
			                    refresh: {
			                        label: '<i class="fa fa-refresh"></i> refresh',
			                        callback: function (result) {
			                            location.reload(true);
			                        }
			                    }
			                }
			            });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}
</script>
@endsection

