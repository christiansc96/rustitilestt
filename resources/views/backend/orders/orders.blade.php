@extends('layouts.back')

@section('title', 'admin orders')

@section('styles')
{{-- dataTables --}}
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css">
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Orders
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('be.dashboard') }}">
                            Dashboard
                        </a>
                        <i class="fa fa-circle">
                        </i>
                    </li>
                    <li>
                        <span>
                            Orders
                        </span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner" id="orders" >
                    <div class="portlet box dark">
				        <div class="portlet-title">
				            <div class="caption">
				                Last Orders added
				            </div>
				           {{--  <a class="btn green-haze btn-outline btn-circle btn-md pull-right" @click="addForm()" style="margin:3px -7px 0 0;">
				                Agregar pedido
				            </a> --}}
				        </div>
				        <div class="portlet-body flip-scroll">
				            <table class="table table-striped table-bordered table-hover table-condensed flip-content" id="order-table">
				                <thead class="flip-content" style="background:#2D3742; color:#FFFFFF;">
				                    <tr>
                                        <th>id</th>
				                        <th>code</th>
                                        <th>products</th>
				                        <th>description</th>
				                        <th>user</th>
                                        <th>status</th>
				                        <th>actions</th>
				                    </tr>
				                </thead>
				            </table>
				        </div>
				    </div>
				    @include('backend.orders.description-order')
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
{!!Form::open(['route'=>[ 'superadmin.datatable.status.order'],'method'=>'POST', 'id' => 'form-status'])!!}
<input type="hidden" name="order_id" value="" id="order_id"/>
<input type="hidden" name="status" value="" id="status"/>
<input type="hidden" name="description" value="" id="description"/>
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-orders.destroy', ':ORDER_ID' ],'method'=>'DELETE', 'id' => 'form-delete'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-orders.edit', ':ORDER_ID' ],'method'=>'GET', 'id' => 'form-edit'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-orders.update', ':ORDER_ID' ],'method'=>'PATCH', 'id' => 'form-update'])!!}
{!!Form::close()!!}
<!-- END CONTAINER -->
@endsection

@section('scripts')
<script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script>
var order = new Vue({
    el: '#orders',
    data: {
        id: null,
        description: null,
        product_id: [],
        name: [],
        code: [],
        price: [],
        total_order: null,
        status: null,
    }
});

var table = $('#order-table').DataTable({
				processing: true,
				serverSide: true,
				ajax: "{{ route('superadmin.datatable.orders') }}",
				columns: [
                    {data: 'id', name: 'id'},
                    {data: 'code', name: 'code'},
                    {data: 'product_count', name: 'product_count'},
                    {data: 'description', name: 'description'},
                    {data: 'user', name: 'user'},
					{data: 'status', name: 'status'},
					{data: 'opciones', name: 'opciones', orderable: false, searchable: false}
				]
            });

function viewProducts(id)
{
    var form = $('#form-edit');
    var url = form.attr('action').replace(':ORDER_ID', id);
    var data = form.serialize();
    axios.get(url, data)
    .then(function (response) {
        console.log(response.data);
        $('#modal-form').modal('show');
        $('.modal-title').text('Products');
        order.product_id = [];
        order.name = [];
        order.code = [];
        order.price = [];
        order.total_order = null;
        order.product_id.push(response.data.product_id);
        order.name.push(response.data.name);
        order.code.push(response.data.code);
        order.price.push(response.data.price);
        order.total_order = response.data.total_order;
    }).catch(function (error) {
        console.log(error);
            dialog.modal('hide');
            var refresh = bootbox.dialog({
                title: "<p class='text-center'>An error has occurred :(</p>",
                message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                closeButton: false,
                buttons: {
                    refresh: {
                        label: '<i class="fa fa-refresh"></i> refresh',
                        callback: function (result) {
                            location.reload(true);
                        }
                    }
                }
            });
    });
}

function statusData(id, status)
{
    var reactive = bootbox.dialog({
        message: "<p class='text-center'>Seguro?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success',
                callback: function () {
                    $("#status").val(status);
                    $("#order_id").val(id);
                    bootbox.prompt({
                        title: "Add description",
                        inputType: 'textarea',
                        callback: function (result) {
                            var dialog = bootbox.dialog({
                                message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                                closeButton: false
                            });
                            $("#description").val(result);
                            var form = $('#form-status');
                            var url = form.attr('action');
                            var data = form.serialize();
                            axios.post(url, data)
                            .then(function (response) {
                                dialog.modal('hide');
                                console.log(response.data);
                                table.ajax.reload();
                                swal({
                                    title: 'status updated!',
                                    text: response.data,
                                    type: 'success',
                                    timer: '1500'
                                });
                            }).catch(function (error) {
                                dialog.modal('hide');
                                console.log(error);
                                var refresh = bootbox.dialog({
                                    title: "<p class='text-center'>An error has occurred :(</p>",
                                    message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                                    closeButton: false,
                                    buttons: {
                                        refresh: {
                                            label: '<i class="fa fa-refresh"></i> refresh',
                                            callback: function (result) {
                                                location.reload(true);
                                            }
                                        }
                                    }
                                });
                            });
                        }
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}

function removeData(id)
{
    var form = $('#form-delete');
    var url = form.attr('action').replace(':ORDER_ID', id);
    var data = form.serialize();

    var eliminar = bootbox.dialog({
        message: "<p class='text-center'>Sure?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    axios.delete(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'Removed!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refresh = bootbox.dialog({
			                title: "<p class='text-center'>An error has occurred :(</p>",
			                message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
			                closeButton: false,
			                buttons: {
			                    refresh: {
			                        label: '<i class="fa fa-refresh"></i> refresh',
			                        callback: function (result) {
			                            location.reload(true);
			                        }
			                    }
			                }
			            });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}
</script>
@endsection