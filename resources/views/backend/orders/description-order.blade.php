<div class="modal" id="modal-form" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"> &times; </span>
                </button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th>id</th>
                        <th>name</th>
                        <th>cod</th>
                        <th>price</th>
                    </thead>
                    <tbody>
                        <template v-for="(item, i) in product_id[0]">
                            <tr>
                                <th>@{{$data['product_id'][0][i]}}</th>
                                <th>@{{$data['name'][0][i]}}</th>
                                <th>@{{$data['code'][0][i]}}</th>
                                <th>@{{$data['price'][0][i]}}</th>
                            </tr>
                        </template>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Total:</td>
                            <td>@{{$data['total_order']}}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>