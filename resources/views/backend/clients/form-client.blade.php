<div class="modal" id="modal-form" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            {!!Form::open(['route'=>[ 'admin-clients.store'],'method'=>'POST', 'id' => 'form-post', 'class' => 'form-horizontal', 'files'=>true, 'data-vv-scope' => 'form-1'])!!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> &times; </span>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <div class="form-group" :class="{'has-error': errors.has('form-1.fullname') }" id="name">
                        <label for="name" class="col-md-3 control-label">Company name</label>
                        <div class="col-md-6">
                            <input class="form-control" name="name" type="text" v-model="name" v-validate="'required|alpha_spaces|max:255'"/>
                            <span v-show="errors.has('form-1.name')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.name') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('form-1.phone') }" id="phone">
                        <label for="phone" class="col-md-3 control-label">Phone</label>
                        <div class="col-md-6">
                            <input class="form-control" name="phone" type="text" v-model="phone" v-validate="{required: true, max: 255, regex: /\(?([0-9\-\+]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/}"/> 
                            <span v-show="errors.has('form-1.phone')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.phone') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('form-1.email') }" id="email">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-6">
                            <input class="form-control" name="email" type="email" v-model="email" v-validate="'required|email|max:255'"/> 
                            <span v-show="errors.has('form-1.email')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.email') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('form-1.fiscal_address') }" id="fiscal_address">
                        <label for="fiscal_address" class="col-md-3 control-label">Fiscal Address</label>
                        <div class="col-md-6">
                            <input class="form-control" name="fiscal_address" type="text" v-model="fiscal_address" v-validate="'required|alpha_spaces|max:255'"/>
                            <span v-show="errors.has('form-1.fiscal_address')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.fiscal_address') }}
                            </span>
                        </div>
                    </div>
                    
                    <div class="form-group" :class="{'has-error': errors.has('form-1.store_address') }" id="store_address">
                        <label for="store_address" class="col-md-3 control-label">Store Address</label>
                        <div class="col-md-6">
                            <input class="form-control" name="store_address" type="text" v-model="store_address" v-validate="'alpha_spaces|max:255'"/> 
                            <span v-show="errors.has('form-1.store_address')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.store_address') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('form-1.deposit_address') }" id="deposit_address">
                        <label for="deposit_address" class="col-md-3 control-label">Deposit Management</label>
                        <div class="col-md-6">
                            <input class="form-control" name="deposit_address" type="text" v-model="deposit_address" v-validate="'alpha_spaces|max:255'"/> 
                            <span v-show="errors.has('form-1.deposit_address')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.deposit_address') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('form-1.contact_person') }" id="contact_person">
                        <label for="contact_person" class="col-md-3 control-label">Contact person</label>
                        <div class="col-md-6">
                            <input class="form-control" name="contact_person" type="text" v-model="contact_person" v-validate="'required|alpha_spaces|max:255'"/> 
                            <span v-show="errors.has('form-1.contact_person')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.contact_person') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('form-1.contact_phone') }" id="contact_phone">
                        <label for="contact_phone" class="col-md-3 control-label">Telephone contact</label>
                        <div class="col-md-6">
                            <input class="form-control" name="contact_phone" type="text" v-model="contact_phone" v-validate="{required: true, max: 255, regex: /\(?([0-9\-\+]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/}"/> 
                            <span v-show="errors.has('form-1.contact_phone')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.contact_phone') }}
                            </span>
                        </div>
                    </div>

                    <hr id="divider"></hr>

                    <div class="form-group" id="password" :class="{'has-error': errors.has('form-1.password') }">
                        <label for="password" class="col-md-3 control-label">Password</label>
                        <div class="col-md-6">
                            <input class="form-control" name="password" type="password" v-model="password" v-validate="'alpha_num|max:18|min:6'"/> 
                            <span v-show="errors.has('form-1.password')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.password') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" id="confirm_password" :class="{'has-error': errors.has('form-1.confirm_password') }">
                        <label for="confirm_password" class="col-md-3 control-label">Confirm Password</label>
                        <div class="col-md-6">
                            <input class="form-control" name="confirm_password" type="password" v-model="confirm_password" v-validate="'confirmed:password'"/> 
                            <span v-show="errors.has('form-1.confirm_password')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.confirm_password') }}
                            </span>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" @click="modal_form()" class="btn btn-primary btn-save">Send</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            {!!Form::close()!!}
        </div>
    </div>
</div>
