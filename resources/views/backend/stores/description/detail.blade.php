<div class="row">

	<div class="col-md-6">
		<p>Name: <strong>{{ $store->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Rif: <strong>{{ $store->rif }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Address: <strong>{{ $store->address }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Phone: <strong>{{ $store->phone }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Email: <strong>{{ $store->email }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Belonging to: <a href="{{ route('profile.user', $store->user->username)}}"><strong>{{ $store->user->username }}</strong></a></p>
	</div>

	<div class="col-md-6">
		<p>Creation date: {{ $store->created_at }}</p>
	</div>
	
</div>