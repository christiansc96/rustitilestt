<div class="modal" id="modal-form" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            {!!Form::open(['route'=>[ 'admin-stores.store'],'method'=>'POST', 'id' => 'form-post', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'files'=>true])!!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> &times; </span>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">

                    <div class="form-group" :class="{'has-error': errors.has('name') }">
                        <label for="name" class="col-md-3 control-label">Name</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('name')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('name')" data-container="body"></i>
                                <input type="text" id="name" name="name" v-model="name" class="form-control" v-validate="'required|max:191'">
                            </div>
                            <span v-show="errors.has('name')" style="color: red;" class="help is-danger">
                                @{{ errors.first('name') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('states') }">
                        <label for="states" class="col-md-3 control-label">States</label>
                        <div class="col-md-6">
                            <select class="form-control" name="states" v-model="states" v-validate="'required|max:255'">
                                @foreach($states as $state)
                                    <option value="{{$state->id}}">{{$state->name}}</option>
                                @endforeach
                            </select>
                            <span v-show="errors.has('states')" style="color: red;" class="help is-danger">
                                @{{ errors.first('states') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('rif') }">
                        <label for="rif" class="col-md-3 control-label">Rif</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('rif')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('rif')" data-container="body"></i>
                                <input type="text" id="rif" name="rif" v-model="rif" class="form-control" v-validate="'required|max:191'">
                            </div>
                            <span v-show="errors.has('rif')" style="color: red;" class="help is-danger">
                                @{{ errors.first('rif') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('phone') }">
                        <label for="phone" class="col-md-3 control-label">Phone</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('phone')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('phone')" data-container="body"></i>
                                <input type="text" id="phone" name="phone" v-model="phone" class="form-control" v-validate="{required: true, max: 255, regex: /\(?([0-9\-\+]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/}">
                            </div>
                            <span v-show="errors.has('phone')" style="color: red;" class="help is-danger">
                                @{{ errors.first('phone') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('email') }">
                        <label for="email" class="col-md-3 control-label">E-mail</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('email')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('email')" data-container="body"></i>
                                <input type="text" id="email" name="email" v-model="email" class="form-control" v-validate="'required|max:191|email'">
                            </div>
                            <span v-show="errors.has('email')" style="color: red;" class="help is-danger">
                                @{{ errors.first('email') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('address') }">
                        <label for="address" class="col-md-3 control-label">Address</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('address')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('address')" data-container="body"></i>
                                <textarea name="address" class="form-control" style="resize: none;" rows="3" v-model="address" v-validate="'required|max:191'"></textarea>
                            </div>
                            <span v-show="errors.has('address')" style="color: red;" class="help is-danger">
                                @{{ errors.first('address') }}
                            </span>
                        </div>
                    </div>

                    <hr id="separator"></hr>

                    <div class="form-group" :class="{'has-error': errors.has('image') }" id="p-image">
                        <label for="image" class="col-md-3 control-label">Image</label>
                        <div class="col-md-6">
                            <input type="file" id="image" name="image" class="form-control" v-validate="'required'|'mimes:image/jpg,image/jpeg,image/png'">
                            <span v-show="errors.has('image')" style="color: red;" class="help is-danger">
                                @{{ errors.first('image') }}
                            </span>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" @click="modal_form()" class="btn btn-primary btn-save">Send</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            {!!Form::close()!!}
        </div>
    </div>
</div>
