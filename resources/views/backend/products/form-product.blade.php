<div class="modal" id="modal-form" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            {!!Form::open(['route'=>[ 'admin-products.store'],'method'=>'POST', 'id' => 'form-post', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'files'=>true])!!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> &times; </span>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">

                    <div class="form-group" :class="{'has-error': errors.has('name') }">
                        <label for="name" class="col-md-3 control-label">Name</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('name')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('name')" data-container="body"></i>
                                <input type="text" id="name" name="name" v-model="name" class="form-control" v-validate="'required|max:191'">
                            </div>
                            <span v-show="errors.has('name')" style="color: red;" class="help is-danger">
                                @{{ errors.first('name') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('code') }">
                        <label for="code" class="col-md-3 control-label">COD</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('code')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('code')" data-container="body"></i>
                                <input type="text" id="code" name="code" v-model="code" class="form-control" v-validate="'required|max:191'">
                            </div>
                            <span v-show="errors.has('code')" style="color: red;" class="help is-danger">
                                @{{ errors.first('code') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('manufacturer') }">
                        <label for="manufacturer" class="col-md-3 control-label">Made In</label>
                        <div class="col-md-6">
                            <select class="form-control" name="manufacturer" v-model="manufacturer" v-validate="'required|max:255'">
                                @foreach($manufacturers as $manufacturer)
                                    <option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
                                @endforeach
                            </select>
                            <span v-show="errors.has('manufacturer')" style="color: red;" class="help is-danger">
                                @{{ errors.first('manufacturer') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('maintenance') }">
                        <label for="maintenance" class="col-md-3 control-label">Maintenance</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('maintenance')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('maintenance')" data-container="body"></i>
                                <textarea name="maintenance" class="form-control" style="resize: none;" rows="3" v-model="maintenance" v-validate="'required|max:191'"></textarea>
                            </div>
                            <span v-show="errors.has('maintenance')" style="color: red;" class="help is-danger">
                                @{{ errors.first('maintenance') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('description') }">
                        <label for="description" class="col-md-3 control-label">Description</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('description')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('description')" data-container="body"></i>
                                <textarea name="description" class="form-control" style="resize: none;" rows="3" v-model="description" v-validate="'required|max:191'"></textarea>
                            </div>
                            <span v-show="errors.has('description')" style="color: red;" class="help is-danger">
                                @{{ errors.first('description') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('short_description') }">
                        <label for="short_description" class="col-md-3 control-label">Short description</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('short_description')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('short_description')" data-container="body"></i>
                                <textarea name="short_description" class="form-control" style="resize: none;" rows="3" v-model="short_description" v-validate="'required|max:191'"></textarea>
                            </div>
                            <span v-show="errors.has('short_description')" style="color: red;" class="help is-danger">
                                @{{ errors.first('short_description') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('info') }">
                        <label for="info" class="col-md-3 control-label">Info</label>
                        <div class="col-md-6">
                            <div class="input-icon right">
                                <i v-show="errors.has('info')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('info')" data-container="body"></i>
                                <textarea name="info" class="form-control" style="resize: none;" rows="3" v-model="info" v-validate="'required|max:191'"></textarea>
                            </div>
                            <span v-show="errors.has('info')" style="color: red;" class="help is-danger">
                                @{{ errors.first('info') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('price') }">
                        <label for="price" class="col-md-3 control-label">Price</label>
                        <div class="col-md-3">
                            <div class="input-icon right">
                                <i v-show="errors.has('price')" class="fa fa-exclamation tooltips" :data-original-title="errors.first('price')" data-container="body"></i>
                                <input type="text" id="price" name="price" v-model="price" class="form-control" v-validate="'required|max:191|decimal'">
                            </div>
                            <span v-show="errors.has('price')" style="color: red;" class="help is-danger">
                                @{{ errors.first('price') }}
                            </span>
                        </div>
                        <label for="coin" class="col-md-1 control-label">Coin</label>
                        <div class="col-md-2">
                            <select class="form-control" name="coin" v-model="coin" v-validate="'required|max:255'">
                                <option value="BS" selected>BS</option>
                                <option value="USD">USD</option>
                                <option value="EUR">EUR</option>
                            </select>
                            <span v-show="errors.has('coin')" style="color: red;" class="help is-danger">
                                @{{ errors.first('coin') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('colors') }">
                        <label for="colors" class="col-md-3 control-label">Colors</label>
                        <div class="col-md-6">
                            <select class="form-control" name="colors" v-model="colors" v-validate="'required|max:255'" multiple>
                                @foreach($colors as $color)
                                    <option value="{{$color->id}}">{{ucfirst($color->name)}}</option>
                                @endforeach
                            </select>
                            <span v-show="errors.has('colors')" style="color: red;" class="help is-danger">
                                @{{ errors.first('colors') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('sizes') }">
                        <label for="sizes" class="col-md-3 control-label">Sizes</label>
                        <div class="col-md-6">
                            <select class="form-control" name="sizes" v-model="sizes" v-validate="'required|max:255'" multiple>
                                @foreach($sizes as $size)
                                    <option value="{{$size->id}}">{{ucfirst($size->name)}}</option>
                                @endforeach
                            </select>
                            <span v-show="errors.has('sizes')" style="color: red;" class="help is-danger">
                                @{{ errors.first('sizes') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('use') }">
                        <label for="use" class="col-md-3 control-label">Uses</label>
                        <div class="col-md-6">
                            <select class="form-control" name="use" v-model="use" v-validate="'required|max:255'">
                                @foreach($uses as $use)
                                    <option value="{{$use->id}}">{{ucfirst($use->name)}}</option>
                                @endforeach
                            </select>
                            <span v-show="errors.has('use')" style="color: red;" class="help is-danger">
                                @{{ errors.first('use') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('space') }">
                        <label for="space" class="col-md-3 control-label">Spaces</label>
                        <div class="col-md-6">
                            <select class="form-control" name="space" v-model="space" v-validate="'required|max:255'">
                                @foreach($spaces as $space)
                                    <option value="{{$space->id}}">{{ucfirst($space->name)}}</option>
                                @endforeach
                            </select>
                            <span v-show="errors.has('space')" style="color: red;" class="help is-danger">
                                @{{ errors.first('space') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('antislip') }">
                        <label for="antislip" class="col-md-3 control-label">Antislips</label>
                        <div class="col-md-6">
                            <select class="form-control" name="antislip" v-model="antislip" v-validate="'required|max:255'">
                                @foreach($antislips as $antislip)
                                    <option value="{{$antislip->id}}">{{ucfirst($antislip->name)}}</option>
                                @endforeach
                            </select>
                            <span v-show="errors.has('antislip')" style="color: red;" class="help is-danger">
                                @{{ errors.first('antislip') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('brand') }">
                        <label for="brand" class="col-md-3 control-label">Brand</label>
                        <div class="col-md-6">
                            <select class="form-control" name="brand" v-model="brand" v-validate="'required|max:255'">
                                @foreach($brands as $brand)
                                    <option value="{{$brand->id}}">{{ucfirst($brand->name)}}</option>
                                @endforeach
                            </select>
                            <span v-show="errors.has('brand')" style="color: red;" class="help is-danger">
                                @{{ errors.first('brand') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('pasta') }">
                        <label for="pasta" class="col-md-3 control-label">Material</label>
                        <div class="col-md-6">
                            <select class="form-control" name="pasta" v-model="pasta" v-validate="'required|max:255'">
                                @foreach($pastas as $pasta)
                                    <option value="{{$pasta->id}}">{{ucfirst($pasta->name)}}</option>
                                @endforeach
                            </select>
                            <span v-show="errors.has('pasta')" style="color: red;" class="help is-danger">
                                @{{ errors.first('pasta') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('texture') }">
                        <label for="texture" class="col-md-3 control-label">Finish</label>
                        <div class="col-md-6">
                            <select class="form-control" name="texture" v-model="texture" v-validate="'required|max:255'">
                                @foreach($textures as $texture)
                                    <option value="{{$texture->id}}">{{ucfirst($texture->name)}}</option>
                                @endforeach
                            </select>
                            <span v-show="errors.has('texture')" style="color: red;" class="help is-danger">
                                @{{ errors.first('texture') }}
                            </span>
                        </div>
                    </div>

                    <div class="form-group" :class="{'has-error': errors.has('pei') }">
                        <label for="pei" class="col-md-3 control-label">PEI</label>
                        <div class="col-md-6">
                            <select class="form-control" name="pei" v-model="pei" v-validate="'required|max:255'">
                                <option value="I">I</option>
                                <option value="II">II</option>
                                <option value="III">III</option>
                                <option value="IV">IV</option>
                                <option value="V">V</option>
                            </select>
                            <span v-show="errors.has('pei')" style="color: red;" class="help is-danger">
                                @{{ errors.first('pei') }}
                            </span>
                        </div>
                    </div>

                    <hr id="separator"></hr>

                    <div class="form-group" :class="{'has-error': errors.has('images') }" id="p-images">
                        <label for="images" class="col-md-3 control-label">Images</label>
                        <div class="col-md-6">
                            <input type="file" id="images" name="images" class="form-control" v-validate="'required'|'mimes:images/jpg,images/jpeg,images/png'" multiple>
                            <span v-show="errors.has('images')" style="color: red;" class="help is-danger">
                                @{{ errors.first('images') }}
                            </span>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" @click="modal_form()" class="btn btn-primary btn-save">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            {!!Form::close()!!}
        </div>
    </div>
</div>
