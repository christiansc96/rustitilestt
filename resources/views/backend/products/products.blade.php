@extends('layouts.back')

@section('title', 'admin products')

@section('styles')
{{-- dataTables --}}
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css">
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Products
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('be.dashboard') }}">
                            Dashboard
                        </a>
                        <i class="fa fa-circle">
                        </i>
                    </li>
                    <li>
                        <span>
                            Products
                        </span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner" id="products" >
                    <div class="portlet box dark">
				        <div class="portlet-title">
				            <div class="caption">
				                Latest products
				            </div>
				            <a class="btn green-haze btn-outline btn-circle btn-md pull-right" @click="addForm()" style="margin:3px -7px 0 0;">
				                Add product +
				            </a>
				        </div>
				        <div class="portlet-body flip-scroll">
				            <table class="table table-striped table-bordered table-hover table-condensed flip-content" id="product-table">
				                <thead class="flip-content" style="background:#2D3742; color:#FFFFFF;">
				                    <tr>
                                        <th>id</th>
				                        <th>name</th>
				                        <th>price</th>
				                        <th>color</th>
				                        <th>size</th>
				                        <th>use</th>
				                        <th>space</th>
				                        <th>status</th>
				                        <th>options</th>
				                    </tr>
				                </thead>
				            </table>
				        </div>
				    </div>
				    @include('backend.products.form-product')
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
{!!Form::open(['route'=>[ 'superadmin.datatable.status.product', ':PRODUCT_ID'],'method'=>'GET', 'id' => 'form-status'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-products.destroy', ':PRODUCT_ID' ],'method'=>'DELETE', 'id' => 'form-delete'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-products.edit', ':PRODUCT_ID' ],'method'=>'GET', 'id' => 'form-edit'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-products.update', ':PRODUCT_ID' ],'method'=>'PATCH', 'id' => 'form-update'])!!}
{!!Form::close()!!}
<!-- END CONTAINER -->
@endsection

@section('scripts')
<script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script>
var product = new Vue({
    el: '#products',
    data: {
        id: null,
        name: null,
        code: null,
        manufacturer: null,
        description: null,
        short_description: null,
        maintenance: null,
        info: null,
        price: null,
        coin: null,
        use: null,
        space: null,
        colors: null,
        sizes: null,
        texture: null,
        antislip: null,
        pei: null,
        packing: null,
        brand: null,
        pasta: null,
        save_method: null,
        method: null,
    },
    methods: {
        addForm: function(){
            product.save_method = 'add';
            product.method = 'POST';
            $('#modal-form').modal('show');
            $('#separator').show();
            $('#p-images').show();
            product.id = null;
            product.name = null;
            product.code = null;
            product.manufacturer = null;
            product.maintenance = null;
            product.texture = null;
            product.antislip = null;
            product.pei = null;
            product.packing = null;
            product.brand = null;
            product.pasta = null;
            product.description = null;
            product.short_description = null;
            product.info = null;
            product.price = null;
            product.coin = null;
            product.use = null;
            product.space = null;
            product.colors = null;
            product.sizes = null;
            document.getElementById("images").value = "";
            $('.modal-title').text('Add product +');
        },
        modal_form: function(){

        	var $fileUpload = $("input[name='images']");
            if (parseInt($fileUpload.get(0).files.length) > 4){
                bootbox.alert({
                    message: "<p class='text-center'>You can upload a maximum of <strong>4</strong> images</p>",
                    backdrop: true,
                    closeButton: false,
                });
                return false;
            }

            this.$validator.validateAll().then((result) => {
                if(result){

                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });

                    let formData = new FormData();

                    if (product.save_method == 'add'){

                        var form = $('#form-post');

                        var url = form.attr('action');

                        for (var x = 0; x < document.getElementById('images').files.length; x++) {
                            formData.append('images[]', document.getElementById('images').files[x]);
                        }

                    }else{

                        var form = $('#form-update');

                        var url = form.attr('action').replace(':PRODUCT_ID', product.id);

                        formData.append('id', product.id);

                    } 

                    formData.append('_method', product.method);
                    formData.append('name', product.name);
                    formData.append('description', product.description);
                    formData.append('short_description', product.short_description);
                    formData.append('info', product.info);
                    formData.append('price', product.price);
                    formData.append('coin', product.coin);
                    formData.append('use', product.use);
                    formData.append('space', product.space);
                    formData.append('code', product.code);
                    formData.append('maintenance', product.maintenance);
                    formData.append('manufacturer', product.manufacturer);
                    formData.append('texture', product.texture);
                    formData.append('antislip', product.antislip);
                    formData.append('pei', product.pei);
                    formData.append('packing', product.packing);
                    formData.append('brand', product.brand);
                    formData.append('pasta', product.pasta);
                    for (var x = 0; x < product.colors.length; x++)
                    {
                        formData.append('colors[]', product.colors[x]);
                    }
                    for (var x = 0; x < product.sizes.length; x++)
                    {
                        formData.append('sizes[]', product.sizes[x]);
                    }
                    
                    axios.post(url, formData,{
                        headers: {
                            'Content-Type': 'multipart/form-data, application/json;charset=UTF-8'
                        }
                    }).then(function (response) {
                        console.log(response);
                        dialog.modal('hide');
                        table.ajax.reload();
                        swal({
                            title: 'Successfully!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                        $('#modal-form').modal('hide');
                    }).catch(function (error) {
                        console.log(error);
                        dialog.modal('hide');
                        var refresh = bootbox.dialog({
                            title: "<p class='text-center'>An error has occurred :(</p>",
                            message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                            closeButton: false,
                            buttons: {
                                refresh: {
                                    label: '<i class="fa fa-refresh"></i> Refresh',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                    return;
                }
                bootbox.alert({
                    message: "<p class='text-center'>you must correct the errors</p>",
                    backdrop: true,
                    closeButton: false,
                });
            });
        }
    }
});

var table = $('#product-table').DataTable({
				processing: true,
				serverSide: true,
				ajax: "{{ route('superadmin.datatable.products') }}",
				columns: [
                    {data: 'id', name: 'id'},
					{data: 'name', name: 'name'},
					{data: 'price', name: 'price'},
					{data: 'color', name: 'color'},
					{data: 'tamaño', name: 'tamaño'},
					{data: 'uso', name: 'uso'},
					{data: 'espacio', name: 'espacio'},
					{data: 'status', name: 'status'},
					{data: 'opciones', name: 'opciones', orderable: false, searchable: false}
				]
            });

function editForm(id)
{
    var form = $('#form-edit');
    var url = form.attr('action').replace(':PRODUCT_ID', id);
    var data = form.serialize();
    product.save_method = 'edit';
    product.method = 'PATCH';
    axios.get(url, data)
    .then(function (response) {
        $('#modal-form').modal('show');
        $('.modal-title').text('Editar Producto');
        $('#separator').hide();
        $('#p-images').hide();
        product.id = response.data.product.id;
        product.name = response.data.product.name;
        product.description = response.data.product.description;
        product.short_description = response.data.product.short_description;
        product.info = response.data.product.info;
        product.price = response.data.product.price.price;
        product.coin = response.data.product.price.coin;
        product.use = response.data.product.use_id;
        product.space = response.data.product.space_id;
        product.manufacturer = response.data.product.manufacturer_id;
        product.code = response.data.product.code;
        product.packing = response.data.product.packing;
        product.pei = response.data.product.pei;
        product.maintenance = response.data.product.maintenance;
        product.pasta = response.data.pasta.id;
        product.antislip = response.data.antislip.id;
        product.brand = response.data.brand.id;
        product.texture = response.data.texture.id;
        product.colors = [];
        for(var i = 0; i < response.data.colors.length; i++){
            product.colors.push(response.data.colors[i].id);
        }
        product.sizes = [];
        for(var i = 0; i < response.data.sizes.length; i++){
            product.sizes.push(response.data.sizes[i].id);
        }
        document.getElementById("images").value = "";
    }).catch(function (error) {
        console.log(error);
            dialog.modal('hide');
            var refresh = bootbox.dialog({
                title: "<p class='text-center'>An error has occurred :(</p>",
                message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                closeButton: false,
                buttons: {
                    refresh: {
                        label: '<i class="fa fa-refresh"></i> refresh',
                        callback: function (result) {
                            location.reload(true);
                        }
                    }
                }
            });
    });
}

function statusData(id)
{
    var reactive = bootbox.dialog({
        message: "<p class='text-center'>Seguro?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    var form = $('#form-status');
                    var url = form.attr('action').replace(':PRODUCT_ID', id);
                    var data = form.serialize();
                    axios.get(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'status updated!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refresh = bootbox.dialog({
			                title: "<p class='text-center'>An error has occurred :(</p>",
			                message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
			                closeButton: false,
			                buttons: {
			                    refresh: {
			                        label: '<i class="fa fa-refresh"></i> refresh',
			                        callback: function (result) {
			                            location.reload(true);
			                        }
			                    }
			                }
			            });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}

function removeData(id)
{
    var form = $('#form-delete');
    var url = form.attr('action').replace(':PRODUCT_ID', id);
    var data = form.serialize();

    var eliminar = bootbox.dialog({
        message: "<p class='text-center'>are you <strong>sure</strong>?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    axios.delete(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'Deleted!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refresh = bootbox.dialog({
			                title: "<p class='text-center'>An error has occurred :(</p>",
			                message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
			                closeButton: false,
			                buttons: {
			                    refresh: {
			                        label: '<i class="fa fa-refresh"></i> Refresh',
			                        callback: function (result) {
			                            location.reload(true);
			                        }
			                    }
			                }
			            });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}
</script>
@endsection