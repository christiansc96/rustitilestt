<div class="row">

	<div class="col-md-6">
		<p>Name: <strong>{{ $product->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Code: <strong>{{ $product->code }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Brand: ({{ $product->brand->type }})<strong>{{ $product->brand->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Made in: <strong>{{ $product->manufacturer->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Material: <strong>{{ $product->pasta->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>PEI: <strong>{{ $product->pei }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Antislip: <strong>{{ $product->antislip->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Finish: <strong>{{ $product->texture->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Packing: <strong>{{ $product->packing }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Price: <strong>{{ $product->price->price }} {{ $product->price->coin }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Description: {{ $product->description}}</p>
	</div>

	<div class="col-md-6">
		<p>Shor Description: {{ $product->short_description}}</p>
	</div>

	<div class="col-md-6">
		<p>Info: {{ $product->info }}</p>
	</div>

	<div class="col-md-6">
		<p>Colors: |@foreach($product->color as $color)<strong>{{ $color->name }}</strong>|@endforeach</p>
	</div>

	<div class="col-md-6">
		<p>Sizes: |@foreach($product->size as $size)<strong>{{ $size->name }}</strong>|@endforeach</p>
	</div>

	<div class="col-md-6">
		<p>Use: |<strong>{{ $product->uso->name }}</strong>|</p>
	</div>

	<div class="col-md-6">
		<p>Space: |<strong>{{ $product->space->name }}</strong>|</p>
	</div>

	<div class="col-md-6">
		<p>Creation date: {{ $product->created_at }}</p>
	</div>
	
</div>