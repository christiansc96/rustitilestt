<div class="modal" id="modal-form" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            {!!Form::open(['route'=>[ 'admin-galery.store'],'method'=>'POST', 'id' => 'form-post', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'files'=>true])!!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> &times; </span>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">

                    <div class="form-group" :class="{'has-error': errors.has('image') }">
                        <label for="image" class="col-md-3 control-label">Image</label>
                        <div class="col-md-6">
                            <input type="file" id="image" name="image" class="form-control" v-validate="'required'|'mimes:image/jpg,image/jpeg,image/png'">
                            <span v-show="errors.has('image')" style="color: red;" class="help is-danger">
                                @{{ errors.first('image') }}
                            </span>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" @click="modal_form({{ $product->id }})" class="btn btn-primary btn-save">Send</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            {!!Form::close()!!}
        </div>
    </div>
</div>
