<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="alert alert-info text-center" role="alert">
            This gallery should only have a total of 4 photos.
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h4>Galery</h4>
        <div class="portlet box dark">
            <div class="portlet-title">
                <div class="caption">
                    Latest images
                </div>
                <a class="btn green-haze btn-outline btn-circle btn-md pull-right" @click="addForm()" style="margin:3px -7px 0 0;">
                    Add image +
                </a>
            </div>
            <div class="portlet-body flip-scroll">
                <table class="table table-striped table-bordered table-hover table-condensed flip-content" id="galery-table">
                    <thead class="flip-content" style="background:#2D3742; color:#FFFFFF;">
                        <tr>
                            <th>image</th>
                            <th>outstanding</th>
                            <th>actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    @include('backend.products.description.form-galery')
</div>