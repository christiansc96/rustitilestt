@extends('layouts.back')
@section('title')
	{{$user->username}}
@endsection

@section('styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Profile | {{$user->fullname}}
                        <small>settings</small>
                    </h1>
                </div>
            </div>
        </div>
        <!-- BEGIN CONTENT BODY -->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
            				<div class="profile-sidebar">
            					@include('backend.user-profile.profile-sidebar')
            				</div>
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content" id="settings">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light ">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md">
                                                    <i class="icon-globe theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                </div>
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                                    </li>
                                                    {{-- <li>
                                                        <a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
                                                    </li> --}}
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="tab-content">
                                                    <!-- PERSONAL INFO TAB -->
                                                    <div class="tab-pane active" id="tab_1_1">
                                                        @include('backend.user-profile.settings-content.personal-info')
                                                    </div>
                                                    <!-- END PERSONAL INFO TAB -->
                                                    <!-- CHANGE AVATAR TAB -->
                                                    <div class="tab-pane" id="tab_1_2">
                                                    	@include('backend.user-profile.settings-content.change-avatar')
                                                    </div>
                                                    <!-- END CHANGE AVATAR TAB -->
                                                    <!-- CHANGE PASSWORD TAB -->
                                                    <div class="tab-pane" id="tab_1_3">
                                                    	@include('backend.user-profile.settings-content.change-password')
                                                    </div>
                                                    <!-- END CHANGE PASSWORD TAB -->
                                                    <!-- PRIVACY SETTINGS TAB -->
                                                    {{-- <div class="tab-pane" id="tab_1_4">
                                                    	@include('user-profile.settings-content.privacy-settings')
                                                    </div> --}}
                                                    <!-- END PRIVACY SETTINGS TAB -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- END CONTENT BODY -->
@endsection

@section('scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.j')}}s" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    var settings = new Vue({
        el: '#settings',
        data:{
            id: {{$user->id}},
            fullname: '{{$user->fullname}}',
            phone: '{{$user->phone}}',
            username: '{{$user->username}}',
            email: '{{$user->email}}',
            // description: '{{$user->description}}',
            old_password: null,
            password: null,
            confirm_password: null,
            save_method: null,
            method: null,
        },
        methods:{
            updateInfo: function()
            {
                this.$validator.validateAll('form-1').then((result) => {
                    if(result){

                        var dialog = bootbox.dialog({
                            message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                            closeButton: false
                        });

                        let formData = new FormData();

                        var form = $('#form-update-info');

                        var url = form.attr('action');
                        
                        formData.append('_method', 'POST');
                        formData.append('id', settings.id);
                        formData.append('fullname', settings.fullname);
                        formData.append('phone', settings.phone);
                        formData.append('username', settings.username);
                        formData.append('email', settings.email);
                        // formData.append('description', settings.description);

                        axios.post(url, formData,{
                        headers: {
                            'Content-Type': 'multipart/form-data, application/json;charset=UTF-8'
                        }
                        }).then(function (response) {
                            dialog.modal('hide');
                            $(".profile-sidebar").load(" .profile-sidebar");
                            swal({
                                title: 'Successfully!',
                                text: response.data,
                                type: 'success',
                                timer: '1500'
                            });
                        }).catch(function (error) {
                            console.log(error);
                            dialog.modal('hide');
                            var refresh = bootbox.dialog({
                            title: "<p class='text-center'>An error has occurred :(</p>",
                            message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                            closeButton: false,
                            buttons: {
                                refresh: {
                                    label: '<i class="fa fa-refresh"></i> refresh',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                        });

                        return;
                    }
                    bootbox.alert({
                        message: "<p class='text-center'>Must correct errors</p>",
                        backdrop: true,
                        closeButton: false,
                    });
                });
            },
            updatePassword: function()
            {
                this.$validator.validateAll('form-2').then((result) => {
                    if(result){
                        
                        var dialog = bootbox.dialog({
                            message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                            closeButton: false
                        });

                        let formData = new FormData();

                        var form = $('#form-update-password');

                        var url = form.attr('action');
                        
                        formData.append('_method', 'POST');
                        formData.append('id', settings.id);
                        formData.append('old_password', settings.old_password);
                        formData.append('password', settings.password);

                        axios.post(url, formData,{
                        headers: {
                            'Content-Type': 'multipart/form-data, application/json;charset=UTF-8'
                        }
                        }).then(function (response) {
                            dialog.modal('hide');
                            if(response.data == 'password does not match'){

                                swal({
                                    title: 'Error!',
                                    text: response.data,
                                    type: 'error',
                                    timer: '1500'
                                });

                            }else{

                                swal({
                                    title: response.data,
                                    text: 'inicia sesión de nuevo',
                                    type: 'success',
                                    timer: '2500'
                                });

                            }
                        }).catch(function (error) {
                            console.log(error);
                            dialog.modal('hide');
                            var refresh = bootbox.dialog({
                                title: "<p class='text-center'>An error has occurred :(</p>",
                                message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                                closeButton: false,
                                buttons: {
                                    refresh: {
                                        label: '<i class="fa fa-refresh"></i> refresh',
                                        callback: function (result) {
                                            location.reload(true);
                                        }
                                    }
                                }
                            });
                        });

                        return;
                    }
                    bootbox.alert({
                        message: "<p class='text-center'>Must correct errors</p>",
                        backdrop: true,
                        closeButton: false,
                    });
                });
            },
            updateAvatar: function()
            {
                this.$validator.validateAll('form-3').then((result) => {
                    if(result){
                        
                        var dialog = bootbox.dialog({
                            message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                            closeButton: false
                        });

                        let formData = new FormData();

                        var form = $('#form-update-avatar');

                        var url = form.attr('action');
                        
                        formData.append('_method', 'POST');
                        formData.append('id', settings.id);
                        formData.append('avatar', document.getElementById('avatar').files[0]);                        

                        axios.post(url, formData,{
                        headers: {
                            'Content-Type': 'multipart/form-data, application/json;charset=UTF-8'
                        }
                        }).then(function (response) {
                            dialog.modal('hide');
                            if(response.data == "Avatar don't updated"){

                                swal({
                                    title: 'Error!',
                                    text: response.data,
                                    type: 'error',
                                    timer: '1500'
                                });

                            }else{
                                $(".profile-sidebar").load(" .profile-sidebar");
                                swal({
                                    title: 'Successfully!',
                                    text: response.data,
                                    type: 'success',
                                    timer: '2500'
                                });

                            }
                        }).catch(function (error) {
                            console.log(error);
                            dialog.modal('hide');
                            var refresh = bootbox.dialog({
                                title: "<p class='text-center'>An error has occurred :(</p>",
                                message: "<p class='text-center'>There was a problem when sending the data, try to reload the page or if the problem persists contact the admin</p>",
                                closeButton: false,
                                buttons: {
                                    refresh: {
                                        label: '<i class="fa fa-refresh"></i> refresh',
                                        callback: function (result) {
                                            location.reload(true);
                                        }
                                    }
                                }
                            });
                        });

                        return;
                    }
                    bootbox.alert({
                        message: "<p class='text-center'>Must correct errors</p>",
                        backdrop: true,
                        closeButton: false,
                    });
                });
            }
        }
    });
</script>
@endsection