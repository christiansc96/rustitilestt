{{-- <p>
    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                                eiusmod.
</p> --}}
{!!Form::open(['route'=>[ 'profile.user.updateAvatar'],'method'=>'POST', 'id' => 'form-update-avatar', 'enctype' => 'multipart/form-data', 'data-vv-scope' => 'form-3'])!!}

    <div class="form-group" :class="{'has-error': errors.has('form-3.avatar') }">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                <img alt="" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image"/>
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
            </div>
            <div>
                <span class="btn default btn-file">
                    <span class="fileinput-new">
                        Select image
                    </span>
                    <span class="fileinput-exists">
                        Change
                    </span>
                    <input id="avatar" name="avatar" type="file" v-validate="'ext:jpg,png,jpeg'">
                    </input>
                </span>
                <a class="btn default fileinput-exists" data-dismiss="fileinput" href="javascript:;">
                    Remove
                </a>
            </div>
            <span v-show="errors.has('form-3.avatar')" style="color: #e73d4a;" class="help">@{{ errors.first('form-3.avatar') }}</span>
        </div>
        {{-- <div class="clearfix margin-top-10">
            <span class="label label-danger">
                NOTE!
            </span>
            <span>
                Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only
            </span>
        </div> --}}
    </div>
    <div class="margin-top-10">
        <a class="btn green" href="javascript:;" @click="updateAvatar()">
            Submit
        </a>
        <a class="btn default" href="javascript:;">
            Cancel
        </a>
    </div>
{!!Form::close()!!}