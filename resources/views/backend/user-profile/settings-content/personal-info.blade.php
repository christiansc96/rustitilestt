{!!Form::open(['route'=>[ 'profile.user.updateInfo'],'method'=>'POST', 'id' => 'form-update-info', 'data-vv-scope' => 'form-1'])!!}
    <div class="form-group" :class="{'has-error': errors.has('form-1.fullname') }">
        <label class="control-label">
            Full Name
        </label>
        <input class="form-control" name="fullname" type="text" v-model="fullname" v-validate="'required|alpha_spaces|max:255'"/>
        <span class="help is-danger" style="color: #e73d4a;" v-show="errors.has('form-1.fullname')">
            @{{ errors.first('form-1.fullname') }}
        </span>
    </div>
    <div class="form-group" :class="{'has-error': errors.has('form-1.username') }">
        <label class="control-label">
            Username
        </label>
        <input class="form-control" name="username" type="text" v-model="username" v-validate="'required|alpha_num|alpha_dash|max:35'"/>
        <span class="help is-danger" style="color: #e73d4a;" v-show="errors.has('form-1.username')">
            @{{ errors.first('form-1.username') }}
        </span>
    </div>
    <div class="form-group" :class="{'has-error': errors.has('form-1.phone') }">
        <label class="control-label">
            Phone
        </label>
        <input class="form-control" name="phone" type="text" v-model="phone" v-validate="{required: true, max: 255, regex: /\(?([0-9\-\+]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/}"/>
        <span class="help is-danger" style="color: #e73d4a;" v-show="errors.has('form-1.phone')">
            @{{ errors.first('form-1.phone') }}
        </span>
    </div>
    <div class="form-group" :class="{'has-error': errors.has('form-1.email') }">
        <label class="control-label">
            Email
        </label>
        <input class="form-control" name="email" type="email" v-model="email" v-validate="'required|email|max:255'"/>
        <span class="help is-danger" style="color: #e73d4a;" v-show="errors.has('form-1.email')">
            @{{ errors.first('form-1.email') }}
        </span>
    </div>
    {{-- <div class="form-group" :class="{'has-error': errors.has('form-1.description') }">
        <label class="control-label">
            About
        </label>
        <textarea class="form-control" name="description" v-model="description" style="resize: none;" rows="3" v-validate="'max:325'">
        </textarea>
        <span class="help is-danger" style="color: #e73d4a;" v-show="errors.has('form-1.email')">
            @{{ errors.first('form-1.description') }}
        </span>
    </div> --}}
    <div class="margiv-top-10">
        <a class="btn green" href="javascript:;" @click="updateInfo()">
            Save Changes
        </a>
        <a class="btn default" href="javascript:;">
            Cancel
        </a>
    </div>
{!!Form::close()!!}