<!-- PORTLET MAIN -->
<div class="portlet light profile-sidebar-portlet ">
    <!-- SIDEBAR USERPIC -->
    <div class="profile-userpic">
        <img alt="{{$user->image->name}}" title="{{$user->image->name}}" class="img-responsive" src="
                                @if($user->image->id == 1)
                                    {{asset($user->image->name)}}
                                @else
                                    {{asset('storage/images/users/'.$user->id.'/'.$user->image->name)}}
                                @endif
                            ">
    </div>
    <!-- END SIDEBAR USERPIC -->
    <!-- SIDEBAR USER TITLE -->
    <div class="profile-usertitle">
        <div class="profile-usertitle-name">
            {{$user->fullname}}
        </div>
        <div class="profile-usertitle-job">
            {{$user->roles[0]->name}}
        </div>
    </div>
    <!-- END SIDEBAR USER TITLE -->
    <!-- SIDEBAR BUTTONS -->
    <div class="profile-userbuttons">
        <button class="btn btn-circle green btn-sm" type="button">
            Follow
        </button>
        <button class="btn btn-circle red btn-sm" type="button">
            Message
        </button>
    </div>
    <!-- END SIDEBAR BUTTONS -->
    <!-- SIDEBAR MENU -->
    <div class="profile-usermenu">
        <ul class="nav">
            <li @if(Request::segment(3) != 'ajustes') class="active" @endif>
                <a href="{{ route('profile.user', $user->username) }}">
                    <i class="icon-home">
                    </i>
                    Preview
                </a>
            </li>
            <li @if(Request::segment(3) == 'ajustes') class="active" @endif>
                <a href="{{ route('settings.user', $user->username) }}">
                    <i class="icon-settings">
                    </i>
                    Setting
                </a>
            </li>
            {{--
            <li>
                <a href="page_user_profile_1_help.html">
                    <i class="icon-info">
                    </i>
                    Help
                </a>
            </li>
            --}}
        </ul>
    </div>
    <!-- END MENU -->
</div>
<!-- END PORTLET MAIN -->
<!-- PORTLET MAIN -->
<div class="portlet light ">
    <!-- STAT -->
    <div class="row list-separated profile-stat">
        <div class="col-md-4 col-sm-4 col-xs-6">
            <div class="uppercase profile-stat-title">
                37
            </div>
            <div class="uppercase profile-stat-text">
                Projects
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6">
            <div class="uppercase profile-stat-title">
                51
            </div>
            <div class="uppercase profile-stat-text">
                Tasks
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6">
            <div class="uppercase profile-stat-title">
                61
            </div>
            <div class="uppercase profile-stat-text">
                Uploads
            </div>
        </div>
    </div>
    <!-- END STAT -->
    <div>
        <h4 class="profile-desc-title">
            About {{$user->fullname}}
        </h4>
        <span class="profile-desc-text">
            {{-- {{$user->description}} --}}
        </span>
        {{-- <div class="margin-top-20 profile-desc-link">
            <i class="fa fa-globe">
            </i>
            <a href="http://www.keenthemes.com">
                www.keenthemes.com
            </a>
        </div> --}}
        {{--
        <div class="margin-top-20 profile-desc-link">
            <i class="fa fa-twitter">
            </i>
            <a href="http://www.twitter.com/keenthemes/">
                @keenthemes
            </a>
        </div>
        <div class="margin-top-20 profile-desc-link">
            <i class="fa fa-facebook">
            </i>
            <a href="http://www.facebook.com/keenthemes/">
                keenthemes
            </a>
        </div>
        --}}
    </div>
</div>
<!-- END PORTLET MAIN -->
