@extends('layouts.back')
@section('title')
	{{$user->username}}
@endsection

@section('styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
 <!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Profile | {{$user->fullname}}
                        <small>Preview</small>
                    </h1>
                </div>
            </div>
        </div>
        <!-- BEGIN CONTENT BODY -->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                {{-- <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('be.dashboard') }}">Dashboard</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Usuarios</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Profile</span>
                    </li>
                </ul> --}}
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="profile-sidebar">
                                @include('backend.user-profile.profile-sidebar')
                            </div>
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                @if($user->roles[0]->id == 1)
                                    @include('backend.user-profile.profile-content.superadmin-content')
                                @elseif($user->roles[0]->id == 2)
                                    @include('backend.user-profile.profile-content.admin-content')
                                @elseif($user->roles[0]->id == 3)
                                    @include('backend.user-profile.profile-content.instructor-content')
                                @endif
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
@endsection

@section('scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/gmaps/gmaps.min.js')}}" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('assets/pages/scripts/profile.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/pages/scripts/timeline.min.js')}}" type="text/javascript"></script>
@endsection