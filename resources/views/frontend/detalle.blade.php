@extends('layouts.front')

@section('title')
    {{ $rproduct->name }}
@endsection

@section('styles')
<!-- GALERIA -->
<link rel='stylesheet'  href="{{asset('css/style1.css')}}" type='text/css' media='all' />
<link rel='stylesheet'  href="{{asset('css/style2.css')}}" type='text/css' media='all' />
<link rel='stylesheet'  href="{{asset('css/style3.css')}}" type='text/css' media='all' />


<script type='text/javascript' src='http://demo.wponlinesupport.com/prodemo/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://demo.wponlinesupport.com/prodemo/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
@endsection

@section('content')

        <!-- Header End -->

        <!-- Page Header Begins -->

        <div id="page-header">
            <div class="header-bg-parallax" style="margin-top: -90px;">
                <div class="overlay">
                    <div class="container text-center">
                        <div class="header-description">
                            <h1>Detail product</h1>
                            <div class="breadcrumbs">
                                <ul>
                                    <li><a href="{{ route('fe.home') }}">Home</a>
                                    </li>
                                    <li><a href="{{ route('fe.products') }}">Product</a>
                                    </li>
                                    <li><a href="#" class="active">Detail product</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /header-small-nav -->
                        </div>
                        <!-- /header-description -->
                    </div>
                    <!-- /container -->
                </div>
                <!-- /overlay -->
            </div>
            <!-- /product-filter -->
        </div>

        <!-- Page Header End -->

        <!-- Product Detail Begin -->

        <div class="p-details">
            <div class="container padding-top-100 padding-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <div class="product">
                            <div class="col-md-6">
                                <article id="post-7882" class="post-7882 page type-page status-publish has-post-thumbnail hentry">  
                                    <div class="entry-content">
                                        <div class="msacwl-slider-wrap  msacwl-row-clearfix">
                                            <div id="msacwl-slider-1" class="msacwl-slider msacwl-design-1 msacwl-slider-popup" data-slider-nav-for='msacwl-slider-nav-1'>
                                                <div class="msacwl-gallery-slider msacwl-common-slider">
                                                    @foreach ($rproduct->image as $image)
                                                        <div class="msacwl-slide" data-item-index="{{$count_image++}}">
                                                            <div class="msacwl-img-wrap" style='height:500px;'>
                                                                <a class="msacwl-img-link" href="javascript:void(0);" data-mfp-src="../storage/images/products/{{$rproduct->id}}/{{$image->name}}"></a>
                                                                <img class="msacwl-img" src="../storage/images/products/{{$rproduct->id}}/{{$image->name}}" data-title="{{$rproduct->name}}" alt="{{$rproduct->name}}" />
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <div class="msacwl-slider-conf" data-conf="{&quot;autoplay&quot;:&quot;true&quot;,&quot;autoplay_speed&quot;:3000,&quot;speed&quot;:300,&quot;arrows&quot;:&quot;true&quot;,&quot;dots&quot;:&quot;false&quot;,&quot;loop&quot;:&quot;true&quot;,&quot;nav_slide_column&quot;:&quot;3&quot;,&quot;nav_center_mode&quot;:true}"></div>
                                            </div>
                                        </div>
                                        <div class="msacwl-slider-nav-1 msacwl-slider-nav design-1"></div>
                                    </div>
                                </article>
                                <!-- /product-image -->
                            </div>
                            <!-- /column-->

                            <div class="col-md-4">
                                <div class="product-details">
                                    <div class="product-title">
                                        <p><a href="#">{{ $rproduct->name }}</a>
                                        </p>
                                    </div>
                                    @if($rproduct->brand)
                                        <p style="padding-top: 10px;"><strong>Brand:</strong> {{ $rproduct->brand->name }}.</p>
                                    @endif
                                    @if($rproduct->manufacturer)
                                        <p style="padding-top: 10px;"><strong>Made in:</strong> {{ $rproduct->manufacturer->name }}.</p>
                                        <!-- /product-title -->
                                    @endif
                                    @if($rproduct->uso)
                                        <p style="padding-top: 10px;"><strong>Use:</strong> {{ $rproduct->uso->name }}.</p>
                                        <!-- /product-title -->
                                    @endif
                                    @if($rproduct->pei)
                                        <p style="padding-top: 10px;"><strong>PEI:</strong> {{ $rproduct->pei }}.</p>
                                        <!-- /product-title -->
                                    @endif
                                    @if($rproduct->pasta)
                                        <p style="padding-top: 10px;"><strong>Material:</strong> {{ $rproduct->pasta->name }}.</p>
                                        <!-- /product-title -->
                                    @endif
                                    @if($rproduct->texture)
                                        <p style="padding-top: 10px;"><strong>Finish:</strong> {{ $rproduct->texture->name }}.</p>
                                        <!-- /product-title -->
                                    @endif
                                    @if($rproduct->antislip)
                                        <p style="padding-top: 10px;"><strong>Antislip:</strong> {{ $rproduct->antislip->name }}.</p>
                                        <!-- /product-title -->
                                    @endif
                                    <div class="product-small-detail padding-vertical-20">
                                        <p>{{ $rproduct->short_description }} </p>
                                    </div>
                                    <!-- /product-small-detail-->

                                    @if (!Auth::guest() || Auth::guard('clients')->check())
                                        <div class="product-price padding-bottom-20">
                                            <p>{{ $rproduct->price->price }} {{ $rproduct->price->coin }}</p>
                                            {{-- <del>Bs 210.000,00</del> --}}
                                        </div>
                                        <!-- /product-price -->
                                    @endif

                                    <div class="product-list-actions padding-top-20 padding-bottom-20">
                                        <form class="padding-bottom-25">
                                            <div>
                                                <div class="col-md-6" style="padding: 0;">
                                                    <div class="form-group">
                                                        <label for="p_size">Size</label>
                                                        <select name="p_size" id="p_size" class="form-control">
                                                            @foreach($rproduct->size as $size)
                                                                <option value="{{ $size->id }}">{{ $size->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <!-- /form-control -->
                                                    </div>
                                                </div>
                                                <!-- /form-group -->

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="p_color">Color</label>
                                                        <select name="p_color" id="p_color" class="form-control">
                                                            @foreach($rproduct->color as $color)
                                                                <option value="{{ $color->id }}">{{ $color->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <!-- /form-control -->
                                                    </div>
                                                </div>
                                            </div>
                                                <!-- /form-group -->
                                        </form>
                                        <!-- /form -->
                                        @if (!Auth::guest() || Auth::guard('clients')->check())
                                            <div class="product-quantity padding-top-30">
                                                <div class="quantity">
                                                    <input type="button" value="-" class="minus" id="rest">
                                                    <input type="text" value="1" id="qty" name="qty" class="quantity-number">
                                                    <input type="button" value="+" class="plus" id="sum">
                                                </div>
                                                <!-- /quantity -->
                                            </div>
                                            <!-- /product-quantity -->
                                            <div class="product-cart margin-left-30 margin-top-30 padding-bottom-10">
                                                <a href="javascript:;" onclick="addToCart({{$rproduct->id}})">
                                                    <p>+ Add to Cart</p>
                                                </a>
                                            </div>
                                        <!-- /product-cart -->
                                        @endif

                                        <div class="social-share padding-vertical-30">
                                            <p>Share:</p>
                                            <div class="share">
                                                <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=http://rustitilestt.com/ceramic-product/{{Request::segment(2)}}','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');"><i class="fa fa-facebook"></i>
                                               </a>
                                                <a href="javascript: void(0);" onclick="window.open('http://twitter.com/home?status=http://rustitilestt.com/ceramic-product/{{Request::segment(2)}}','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');"><i class="fa fa-twitter"></i>
                                               </a>
                                            </div>
                                            <!-- /share -->
                                        </div>
                                        <!-- /social-share -->

                                        </div>
                                        <!-- /product-category-tag -->
                                    </div>
                                    <!-- /product-list-actions -->
                                </div>
                                <!-- /product-details -->
                            </div>
                            <!-- /column -->
                        </div>
                        <!-- /product -->
                    </div>
                    <!-- /column -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>

        <!-- Product Detail End -->

        <!-- Tabs Begin -->

        <div class="container padding-bottom-100">
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs">
                        <ul class="tab-links text-center">
                            <li class="active">
                                <a href="#tab1">
                                    <span>Technical Sheet</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="#tab2">
                                    <span>Packing</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="#tab3">
                                    <span>Catalogue</span>
                                </a>
                            </li>
                        </ul>
                        <!-- /tab-links -->

                        <div class="tab-content">
                            <div id="tab1" class="tab active">
                                <div class="tab-description">
                                    <p class="padding-vertical-40">{{ $rproduct->description }}</p>

                                </div>
                                <!-- /tab-description -->
                            </div>
                            <!-- /tab -->

                            <div id="tab2" class="tab">
                                <div class="tab-description">
                                    <p class="padding-vertical-40">{{ $rproduct->info }}</p>

                                </div>
                                <!-- /tab-description -->
                            </div>
                            <!-- /tab -->

                            <div id="tab3" class="tab">
                                <div class="tab-description">
                                    <p class="padding-vertical-40">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when anunknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>

                                </div>
                                <!-- /tab-description -->
                            </div>
                            <!-- /tab -->
                        </div>
                        <!-- /tab-content -->
                    </div>
                    <!-- /tabs -->
                </div>
                <!-- /column -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->

        <!-- Tabs End -->

        <!-- Related Products Begin -->

        <div class="related-products">
            <div class="container text-center padding-top-20 padding-bottom-100">
                <h2 class="double-line"><span>Related products</span></h2>
                <p class="sub-tittle"></p>
                <div class="row padding-top-60">
                    @if($results)
                        @foreach($results as $result) 
                            <div class="col-md-3 col-sm-6">
                                <div class="product-item padding-bottom-60">
                                    <div class="product-image">
                                        <img src="{{ asset('storage/images/products/'.$result['id'].'/'.$result['image'][0]['name']) }}" alt="{{ $result['image'][0]['name'] }}" title="{{ $result['image'][0]['name'] }}">
                                            <div class="product_overlay">
                                                <div class="product-cart" style="padding-bottom: 14px;">
                                                    <a href="#">
                                                        <p>+ Add to Cart</p>
                                                    </a>
                                                </div>
                                                <!-- /product-cart -->
                                            </div>
                                            <!-- /product_overlay -->
                                        </div>
                                        <!-- /product-image -->
                                        <div class="product-short-detail padding-top-20">
                                            <div class="product-title">
                                                <p><a href="{{ route('fe.show.products', $result['slug']) }}">{{ $result['name'] }}</a>
                                                </p>
                                            </div>
                                            <!-- /product-title -->
                                            @if (!Auth::guest() || Auth::guard('clients')->check())
                                                <div class="product-price">
                                                    <p>{{ $result['price']['price'] }} {{$result['price']['coin']}}</p>
                                                </div>
                                            @endif
                                            <!-- /product-price -->
                                        </div>
                                        <!-- /product-short-detail -->
                                </div>
                                <!-- /product-item -->
                            </div>
                        @endforeach
                    @endif
                    <!-- /column -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>

        <!-- Related Products End -->
{!!Form::open(['route'=>['rustitiles-cart.store'],'method'=>'POST', 'id' => 'form-post-cart'])!!}
{!!Form::close()!!}
@endsection
@section('scripts')
<script type='text/javascript' src='http://demo.wponlinesupport.com/prodemo/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>
<script type='text/javascript' src='http://demo.wponlinesupport.com/prodemo/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.4.5'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var Mpwc_Pro_Popup = {"enable":"1","cookie_prefix":"_hellpo_"};
/* ]]> */
</script>
<script type='text/javascript' src='http://demo.wponlinesupport.com/prodemo/wp-content/plugins/album-and-image-gallery-plus-lightbox-pro/assets/js/jquery.magnific-popup.min.js?ver=1.2.7'></script>
<script type='text/javascript' src='http://demo.wponlinesupport.com/prodemo/wp-content/plugins/album-and-image-gallery-plus-lightbox-pro/assets/js/slick.min.js?ver=1.2.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var WpIsgp = {"is_mobile":"0","is_rtl":"0"};
/* ]]> */
</script>
<script type='text/javascript' src='http://demo.wponlinesupport.com/prodemo/wp-content/plugins/meta-slider-and-carousel-with-lightbox-pro/assets/js/wp-igsp-pro-public.js?ver=1.2.4'></script>
<script>
$( "#rest" ).click(function() {
    var qty = $("#qty").val();

    if(qty > 1){
        qty--;
    }

    $("#qty").val(qty);

});
$( "#sum" ).click(function() {
  var qty = $("#qty").val();

    if(qty < 10){
        qty++;
    }

    $("#qty").val(qty);
});

var addToCart = function(id, form = $('#form-post-cart'))
{          
    var qty = $("#qty").val();

    var url = form.attr('action');

    var data = form.serialize()+'&id='+id+'&qty='+qty;

    $.post(url, data, function(response){
        console.log(response);
        $("#count").load(" #count");
        $(".cart-product-list").load(" .cart-product-list");
        $("#total_price").load(" #total_price");
        swal({
            title: 'Aprobado!',
            text: response,
            type: 'success',
            timer: '2500'
        })
    });

}
</script>
@endsection