@extends('layouts.front')

@section('title', 'Home')

@section('styles')

@endsection

@section('content')
<!-- Main-slider begin -->
<div id="hero">
    <div class="flexslider">
        <ul class="slides">
            @foreach ($showcases as $showcase)
                <li class="slide" data-background="storage/images/show-cases/{{$showcase->id}}/{{$showcase->image->name}}" data-thumbnail="storage/images/show-cases/{{$showcase->id}}/{{$showcase->image->name}}">
                    <div class="slider-caption fx-caption-2 text-center" style="margin-top: -40px;">
                        <h1 class="sl-big-heading-3">{{$showcase->heading}}</h1>
                        <p>{{$showcase->description}}</p>
                    </div>
                    <!-- /slider-caption -->
                </li>
            @endforeach        
            <!-- /slide -->
        </ul>
        <!-- /slides -->
    </div>
    <!-- /flexslider -->
</div>

<!-- End Main-slider -->

<!-- Banner-offers Begin -->

<div class="banner-offer">
    <div class="container-fluid text-center" style="padding:5px; margin-top: 0px;">
        <div class="col-md-3 p-image" style="padding:5px;">
        	<a href="{{ route('fe.products.filter', 'habitación') }}">
                <img src="img/img1.jpg" alt="" />
                <div class="hover_overlay">
                    <div class="banner-content">
                        <div class="content">
                            <h1 style="font-size: 54px !important; font-weight: bold;">LIVING ROOM</h1>
                        </div>
                    </div>
                    <!-- /banner-content -->
                </div>
            </a>
                <!-- /hover-overlay -->
        </div>
        <!-- /column -->

        <div class="col-md-3 p-image" style="padding:5px;">
        	<a href="{{ route('fe.products.filter', 'baños') }}">
                <img src="img/img4.jpg" alt="" />
                <div class="hover_overlay">
                    <div class="banner-content">
                        <div class="content">
                            <h1 style="font-size: 54px !important; font-weight: bold;">BATHROOM</h1>
                        </div>
                    </div>
                    <!-- /banner-content -->
                </div>
            </a>
                <!-- /hover-overlay -->
        </div>
        <!-- /column -->

        <div class="col-md-3 p-image" style="padding:5px;">
            <a href="{{ route('fe.products.filter', 'cocina') }}">
                <img src="img/img5.jpg" alt="" />
                <div class="hover_overlay">
                    <div class="banner-content">
                        <div class="content">
                            <h1 style="font-size: 54px !important; font-weight: bold;">KITCHEN</h1>
                        </div>
                    </div>
                    <!-- /banner-content -->
                </div>
            </a>
                <!-- /hover-overlay -->
        </div>
        <!-- /column -->

        <div class="col-md-3 p-image" style="padding:5px;">
            <a href="{{ route('fe.products.filter', 'exteriores') }}">
                <img src="img/img3.jpg" alt="" />
                <div class="hover_overlay">
                    <div class="banner-content">
                        <div class="content">
                            <h1 style="font-size: 54px !important; font-weight: bold; text-transform: uppercase;">OUTDOOR</h1>
                        </div>
                    </div>
                    <!-- /banner-content -->
                </div>
            </a>
                <!-- /hover-overlay -->
        </div>
                <!-- /column -->
    </div>
    <!-- /container-fluid -->
</div>

<!-- Banner Offers End -->

<!-- Best Seller Begin -->

<div class="best-seller padding-vertical-100">
    <div class="container text-center">
        {{-- <h2 class="double-line"><span>Products by use</span></h2> --}}
        {{-- <p class="sub-tittle">Here we will place the uses of the products offered by the brand</p> --}}
        <div class="row padding-top-60">
            <div class="col-md-12">
                <div class="col-md-6">
                    {{-- @if($products_paredes != null)
                        <div class="best-product padding-right-25">
                            <div class="product-image">
                                @foreach($products_paredes[0]->image as $image)
                                    @if($image->outstanding == 1)
                                        <a href="{{ route('fe.show.products', $products_paredes[0]->slug) }}">
                                            <img alt="" src="../storage/images/products/{{ $products_paredes[0]->id }}/{{ $image->name }}" style="max-width: 255px;">
                                        </a>
                                    @endif
                                @endforeach
                                @if (!Auth::guest() || Auth::guard('clients')->check())
                                    <div class="product_overlay">
                                        <div class="product-cart" style="padding-bottom: 14px;">
                                            <a href="javascript:;" onclick="addToCart({{$products_paredes[0]->id}})">
                                                <p>
                                                    + Agregar al carrito
                                                </p>
                                            </a>
                                        </div>
                                        <!-- /product-cart -->
                                    </div>
                                @endif
                                <!-- /product_overlay -->
                            </div>
                            <!-- /product-image -->
                            <div class="product-short-detail padding-top-20">
                                <div class="product-title">
                                    <p><a href="{{ route('fe.show.products', $products_paredes[0]->slug) }}">{{ $products_paredes[0]->name }}</a>
                                    </p>
                                </div>
                                <!-- /product-title -->
                                @if (!Auth::guest() || Auth::guard('clients')->check())
                                    <div class="product-price">
                                        <p>{{ $products_paredes[0]->price->price }} {{ $products_paredes[0]->price->coin }}</p>
                                    </div>
                                @endif
                                <!-- /product-price -->
                            </div>
                            <!-- /product-short-detail -->
                        </div>
                        /best-product

                        <div class="best-product">
                            <div class="product-image">
                                @foreach($products_paredes[1]->image as $image)
                                    @if($image->outstanding == 1)
                                        <a href="{{ route('fe.show.products', $products_paredes[1]->slug) }}">
                                            <img alt="" src="../storage/images/products/{{ $products_paredes[1]->id }}/{{ $image->name }}" style="max-width: 255px;">
                                        </a>
                                    @endif
                                @endforeach
                                @if (!Auth::guest() || Auth::guard('clients')->check())
                                    <div class="product_overlay">
                                        <div class="product-cart" style="padding-bottom: 14px;">
                                            <a href="javascript:;" onclick="addToCart({{$products_paredes[1]->id}})">
                                                <p>
                                                    + Agregar al carrito
                                                </p>
                                            </a>
                                        </div>
                                        <!-- /product-cart -->
                                    </div>
                                @endif
                                <!-- /product_overlay -->
                            </div>
                            <!-- /product-image -->
                            <div class="product-short-detail padding-top-20">
                                <div class="product-title">
                                    <p><a href="{{ route('fe.show.products', $products_paredes[1]->slug) }}">{{ $products_paredes[1]->name }}</a>
                                    </p>
                                </div>
                                <!-- /product-title -->
                                @if (!Auth::guest() || Auth::guard('clients')->check())
                                    <div class="product-price">
                                        <p>{{ $products_paredes[1]->price->price }} {{ $products_paredes[1]->price->coin }}</p>
                                    </div>
                                @endif
                                <!-- /product-price -->
                            </div>
                            <!-- /product-short-detail -->
                        </div>
                            <!-- /best-product -->
                    @endif --}}
                        <!-- /best-product -->
                </div>
                <!-- /column -->

                <div class="col-md-6 col-sm-6 p-image">
                	<a href="{{ route('fe.products.filter', 'paredes') }}">
	                    <img src="img/productos/pared.jpg" alt="" />
	                    <div class="hover_overlay">
	                        <div class="banner-content">
	                            <div class="content">
	                                <h1 style="font-size: 54px !important; font-weight: bold;">WALL TILES</h1>
	                            </div>
	                        </div>
	                        <!-- /banner-content -->
	                    </div>
	                </a>
	                    <!-- /hover-overlay -->
                </div>
                <!-- /column -->
                <!-- /poster -->

                <div class="col-md-6 col-sm-6 p-image">
                	<a href="{{ route('fe.products.filter', 'pisos') }}">
	                    <img src="img/productos/pisos.jpg" alt="" />
	                    <div class="hover_overlay">
	                        <div class="banner-content">
	                            <div class="content">
	                                <h1 style="font-size: 54px !important; font-weight: bold;">FLOOR TILES</h1>
	                            </div>
	                        </div>
	                        <!-- /banner-content -->
	                    </div>
	                </a>
	                    <!-- /hover-overlay -->
                </div>
                <!-- /column -->
                <div class="col-md-6 padding-top-25">
                   {{-- @if($products_pisos != null)
                        <div class="best-product padding-right-25">
                            <div class="product-image">
                                @foreach($products_pisos[0]->image as $image)
                                    @if($image->outstanding == 1)
                                        <a href="{{ route('fe.show.products', $products_pisos[0]->slug) }}">
                                            <img alt="" src="../storage/images/products/{{ $products_pisos[0]->id }}/{{ $image->name }}" style="max-width: 255px;">
                                        </a>
                                    @endif
                                @endforeach
                                <img src="../storage/images/products/{{ $products_pisos[0]->id }}/{{ $products_pisos[0]->image[0]->name }}" style="max-width: 255px;" alt="">
                                @if (!Auth::guest())
                                    <div class="product_overlay">
                                        <div class="product-cart" style="padding-bottom: 14px;">
                                            <a href="javascript:;" onclick="addToCart({{$products_pisos[0]->id}})">
                                                <p>
                                                    + Agregar al carrito
                                                </p>
                                            </a>
                                        </div>
                                        <!-- /product-cart -->
                                    </div>
                                @endif
                                <!-- /product_overlay -->
                            </div>
                            <!-- /product-image -->
                            <div class="product-short-detail padding-top-20">
                                <div class="product-title">
                                    <p><a href="{{ route('fe.show.products', $products_pisos[0]->slug) }}">{{ $products_pisos[0]->name }}</a>
                                    </p>
                                </div>
                                <!-- /product-title -->
                                <div class="product-price">
                                    <p>{{ $products_pisos[0]->price->price }} {{ $products_pisos[0]->price->coin }}</p>
                                </div>
                                <!-- /product-price -->
                            </div>
                            /product-short-detail
                        </div>
                        <!-- /best-product -->

                        <div class="best-product">
                            <div class="product-image">
                                @foreach($products_pisos[1]->image as $image)
                                    @if($image->outstanding == 1)
                                        <a href="{{ route('fe.show.products', $products_pisos[1]->slug) }}">
                                            <img alt="" src="../storage/images/products/{{ $products_pisos[1]->id }}/{{ $image->name }}" style="max-width: 255px;">
                                        </a>
                                    @endif
                                @endforeach
                                @if (!Auth::guest())
                                    <div class="product_overlay">
                                        <div class="product-cart" style="padding-bottom: 14px;">
                                            <a href="javascript:;" onclick="addToCart({{$products_pisos[1]->id}})">
                                                <p>
                                                    + Agregar al carrito
                                                </p>
                                            </a>
                                        </div>
                                        <!-- /product-cart -->
                                    </div>
                                @endif
                                <!-- /product_overlay -->
                            </div>
                            <!-- /product-image -->
                            <div class="product-short-detail padding-top-20">
                                <div class="product-title">
                                    <p><a href="{{ route('fe.show.products', $products_pisos[1]->slug) }}">{{ $products_pisos[1]->name }}</a>
                                    </p>
                                </div>
                                <!-- /product-title -->
                                <div class="product-price">
                                    <p>{{ $products_pisos[1]->price->price }} {{ $products_pisos[1]->price->coin }}</p>
                                </div>
                                <!-- /product-price -->
                            </div>
                            <!-- /product-short-detail -->
                        </div>
                        <!-- /best-product -->
                    @endif --}}
                    <!-- /best-product -->
                </div>
                <!-- /column -->
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>

<!-- Best Seller End -->

<!-- Video LightBox Begin -->

<div class="vl-box">
    <div class="white-overlay"></div>
    <div class="container text-center">
        <div class="video-box">
            <a href="https://www.youtube.com/embed/pYAXsbsFBC8" class="play-button"><img src="img/video-play-icon.png" alt="" /></a>
            <h1>Promotional video</h1>
            <p>Here we could place some video maybe of some process or what we want</p>
        </div>
        <!-- /video box -->
    </div>
    <!-- /container -->
</div>

<!-- Video LightBox End -->

<!-- Blog Begin -->

<div class="blog-latest margin-top-100">
    <div class="container text-center">
        <h2 class="double-line"><span>Blog</span></h2>
        <p class="sub-tittle">Here we put tips and recommendations for decoration and interior design</p>
        <div class="row padding-bottom-60">
            <div class="col-md-12">
                <div class="blog-carousel pr-carousel">
                    <div class="item">
                        <div class="product">
                            <img src="img/blog/blog1.jpg" class="img-responsive" alt="" />
                            <div class="hover_overlay">
                                <div class="hover-search">
                                    <a href="#"><i class="icon_link_alt"></i></a>
                                </div>
                            </div>
                            <!-- /hover_overlay -->
                        </div>
                        <div class="blog-short-detail">
                            <div class="post-title padding-vertical-10">
                                <p><a href="#">Title of the article published on the blog</a>
                                </p>
                            </div>
                        </div>
                        <!-- /blog-short-detail -->
                    </div>
                    <!-- /item -->

                    <div class="item">
                        <div class="product">
                            <img src="img/blog/blog4.jpg" class="img-responsive" alt="" />
                            <div class="hover_overlay">
                                <div class="hover-search">
                                    <a href="#"><i class="icon_link_alt"></i></a>
                                </div>
                            </div>
                            <!-- /hover_overlay -->
                        </div>
                        <div class="blog-short-detail">
                            <div class="post-title padding-vertical-10">
                                <p><a href="#">Title of the article published on the blog</a>
                                </p>
                            </div>
                        </div>
                        <!-- /blog-short-detail -->
                    </div>
                    <!-- /item -->

                    <div class="item">
                        <div class="product">
                            <img src="img/blog/blog2.jpg" class="img-responsive" alt="" />
                            <div class="hover_overlay">
                                <div class="hover-search">
                                    <a href="#"><i class="icon_link_alt"></i></a>
                                </div>
                            </div>
                            <!-- /hover_overlay -->
                        </div>
                        <div class="blog-short-detail">
                            <div class="post-title padding-vertical-10">
                                <p><a href="#">Title of the article published on the blog</a>
                                </p>
                            </div>
                        </div>
                        <!-- /blog-short-detail -->
                    </div>
                    <!-- /item -->

                    <div class="item">
                        <div class="product">
                            <img src="img/blog/blog3.jpg" class="img-responsive" alt="" />
                            <div class="hover_overlay">
                                <div class="hover-search">
                                    <a href="#"><i class="icon_link_alt"></i></a>
                                </div>
                            </div>
                            <!-- /hover_overlay -->
                        </div>
                        <div class="blog-short-detail">
                            <div class="post-title padding-vertical-10">
                                <p><a href="#">Title of the article published on the blog</a>
                                </p>
                            </div>
                        </div>
                        <!-- /blog-short-detail -->
                    </div>
                    <!-- /item -->
                </div>
                <!-- /blog-carousel -->
            </div>
            <!-- /column -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>

<!-- Blog End -->
{!!Form::open(['route'=>['rustitiles-cart.store'],'method'=>'POST', 'id' => 'form-post-cart'])!!}
{!!Form::close()!!}
@endsection

@section('scripts')
<script>

var addToCart = function(id, form = $('#form-post-cart'))
{          

  var url = form.attr('action');

  var data = form.serialize()+'&id='+id+'&qty=1';

  $.post(url, data, function(response){
    console.log(response);
    $("#count").load(" #count");
    $(".cart-product-list").load(" .cart-product-list");
    $("#total_price").load(" #total_price");
    swal({
        title: 'Aprobado!',
        text: response,
        type: 'success',
        timer: '2500'
    })

  });

}
</script>
@endsection
        