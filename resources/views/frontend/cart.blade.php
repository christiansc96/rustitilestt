<div class="side-cart">
    <div class="close-btn text-center padding-vertical-30">
        <a class="close-cart" href="#">
            Close
        </a>
    </div>
    <!-- /close-btn -->
    <div class="shoping-cart-box">
        <div class="cart-product-list padding-bottom-20">
            @foreach(Cart::instance('tuceramica')->content() as $cartItem)
                <div class="cart-product">
                    <a href="#">
                        <img alt="{{$cartItem->options->image}}" src="../storage/images/products/{{$cartItem->options->product_id}}/{{$cartItem->options->image}}">
                        </img>
                    </a>
                    <div class="cart-product-info">
                        <div class="product-name">
                            <a href="#">
                                {{$cartItem->name}}
                            </a>
                        </div>
                        <!-- /product-name -->
                        <div class="product-atributes padding-vertical-15">
                            <p class="quantity">
                                Qty: {{$cartItem->qty}}
                            </p>
                        </div>
                        <!-- /product-atributes -->
                        <div class="product-price">
                            <p>
                                {{$cartItem->price * $cartItem->qty}} {{$cartItem->options->coin}}
                            </p>
                        </div>
                        <!-- /product-price -->
                        <a class="remove-btn" href="#" onclick="deleteData('{{$cartItem->rowId}}')">
                            <i class="fa fa-close">
                            </i>
                        </a>
                    </div>
                    <!-- /cart-product-info -->
                </div>
            @endforeach
            <!-- /cart-product -->
        </div>
        <!-- /cart-product-list -->
        <div class="cart-prices padding-top-30">
            {{-- <div class="shipping">
                <span class="shipping-text">
                    Envio :
                </span>
                <span class="product-price pull-right">
                    50.000.000,00 Bs
                </span>
            </div> --}}
            <!-- /shipping -->
            <div class="total-price padding-top-15" id="total_price">
                <span class="price-text">
                    Total :
                </span>
                <span class="product-price pull-right">
                    {{Cart::subtotal()}}
                </span>
            </div>
            <!-- /total-price -->
        </div>
        <!-- /cart-prices -->
        <div class="cart-buttons margin-top-30">
            <a class="view-cart-btn" href="{{ route('rustitiles-cart.index') }}">
                View cart
            </a>
            <a class="checkout-btn margin-top-25" href="javascript:;" onclick="addOrder1()">
                send order
            </a>
            <a class="checkout-btn margin-top-25" href="{{ route('rustitiles-my-orders.index') }}">
                My orders
            </a>
        </div>
        <!-- /cart-buttons -->
    </div>
    <!-- /shoping-cart-box -->
</div>
<!-- /side-cart -->
{!!Form::open(['route'=>[ 'rustitiles-cart.destroy', ':CART_ID' ],'method'=>'DELETE', 'id' => 'form-delete'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'rustitiles-my-orders.store'],'method'=>'POST', 'id' => 'form-post'])!!}
{!!Form::close()!!}
<script>
var addOrder1 = function(form = $('#form-post')){          

    var url = form.attr('action');

    var data = form.serialize();

    $.post(url, data, function(response){
        $("#count").load(" #count");
        $(".cart-product-list").load(" .cart-product-list");
        $("#total_price").load(" #total_price");
        $("#cart-table").load(" #cart-table");
        $("#totally").load(" #totally");
        swal({
            title: 'successfully!',
            text: response,
            type: 'success',
            timer: '2500'
        })

    });

}

var deleteData = function(id){

    var form = $('#form-delete');

    var url = form.attr('action').replace(':CART_ID', id);

    var data = form.serialize();

    swal({
        title: 'Sure?',
        text: "You will not be able to reverse this!",
        type: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
    }).then(function () {
        $.post(url, data, function(response){
          $("#count").load(" #count");
          $(".cart-product-list").load(" .cart-product-list");
          $("#total_price").load(" #total_price");
          swal({
              title: 'successfully!',
              text: response,
              type: 'success',
              timer: '1500'
          })
        });
    });

}
</script>