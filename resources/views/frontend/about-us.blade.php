@extends('layouts.front')
@section('title', 'About Us')
@section('styles')
@endsection
@section('content')

<!-- Page Header Begins -->
<div id="page-header">
    <div class="header-bg-parallax" style="margin-top: -90px;">
        <div class="overlay">
            <div class="container text-center">
                <div class="header-description">
                    <h1>About Us</h1>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('fe.home') }}">Home</a>
                            </li>
                            <li><a href="{{ route('fe.about_us') }}" class="active">About Us</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /header-small-nav -->
                </div>
                <!-- /header-description -->
            </div>
            <!-- /container -->
        </div>
        <!-- /overlay -->
    </div>
    <!-- /product-filter -->
</div>

<!-- Page Header End -->

<!-- Product Item List Begin -->

<div class="contact">
    <div class="container text-center padding-top-100 padding-bottom-75">
        <h2>RUSTITILES T&amp;T</h2>
        <div class="row">
            <div class="col-md-7">
                <p class="contact-p margin-top-50" style="font-size: 1rem; color: #000; font-weight: bold; text-align: right;">Are you tired of over-priced tiles? Are you tired of uncustomized service? Finally is the word that better describes our company: Finally you will get the best
quality/price relation. Finally you will have the best customer attention. Come visit us at our showrooms in Chaguanas!</p>

                <p class="contact-p margin-top-20" style="text-align: right;">A new modern world of quality tiles based on excellence and care with details, where you can find the most interesting European brands of the Ceramic World. We work with skilled professionals, that can provide you with unique and trendy solutions to your home or business.</p>

                <p class="contact-p margin-top-20" style="text-align: right;">We supply different types of construction materials: Ceramic Tiles, Porcelain Tiles, Terracotta Tiles, Concrete Tiles, Natural Stones as well as Thin Set, Bathroom Sets &Fittings.</p>
            </div>

            <div class="col-md-5">
                <img src="img/about1.jpg" alt="" style="margin-top: 24px;" />
            </div>

        </div>

    </div>
    <!-- /container -->
</div>
<!-- Contact Form End -->
@endsection
@section('scripts')
@endsection