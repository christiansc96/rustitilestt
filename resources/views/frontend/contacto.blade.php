@extends('layouts.front')
@section('title', 'Contact')
@section('styles')
@endsection
@section('content')

<!-- Header End -->

<!-- Page Header Begins -->

<div id="page-header">
    <div class="header-bg-parallax" style="margin-top: -90px;">
        <div class="overlay">
            <div class="container text-center">
                <div class="header-description">
                    <h1>Contact</h1>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('fe.home') }}">Home</a>
                            </li>
                            <li><a href="{{ route('fe.contact') }}" class="active">Contact</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /header-small-nav -->
                </div>
                <!-- /header-description -->
            </div>
            <!-- /container -->
        </div>
        <!-- /overlay -->
    </div>
    <!-- /product-filter -->
</div>

<!-- Page Header End -->

<!-- Product Item List Begin -->

<div class="contact">
    <div class="container text-center padding-top-100 padding-bottom-75">
        <h2>Contact Us</h2>
        <p class="contact-p margin-top-50">Do you want to renovate the floor of your loved home with high-end quality products and the best price?<br> Are you improvising your business and want an unique touch? Are you a contractor that wants the best for your clients?<br>Contact us to have customized solutions with the best

guaranteed price!</p>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <form action="contact.php" method="POST" id="contact-form" class="contact-form padding-top-50 margin-bottom-15">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group text-left">
                                <label for="contact-name">Name</label>
                                <input type="text" name="name" id="contact-name" class="form-control" placeholder="Mark Stevens" required>
                            </div>
                            <!-- /form-group -->
                        </div>
                        <!-- /column -->

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group text-left">
                                <label for="contact-email">Email</label>
                                <input type="email" name="email" id="contact-email" class="form-control" placeholder="MarkStevens@gmail.com" required>
                            </div>
                            <!-- /form-group -->
                        </div>
                        <!-- /column -->

                        <div class="col-md-12">
                            <div class="form-group text-left">
                                <label for="contact-message">Message</label>
                                <textarea name="message" id="contact-message" class="form-control" placeholder="Write your message here..." required></textarea>
                            </div>
                            <!-- /form-group -->
                            <button type="submit" class="btn-form margin-top-25">Send your message</button>
                            <!-- /button -->
                            <div id="ajax-message" class="margin-top-30"></div>
                            <!-- /ajax-message -->
                        </div>
                        <!-- /column -->
                    </div>
                    <!-- /row -->
                </form>
                <!-- /form -->
            </div>
            <!-- /column -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>

<!-- Contact Form End -->

<!-- Contact Info Begin -->

<div class="contact">
    <div class="text-center padding-bottom-40">
        <h2>Contact Info.</h2>
        <p class="contact-p margin-top-50">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feu-
            <br>giat nulla facilisis at vero eros et accumsan.</p>
        <!-- /contact-map -->
        <div class="container contact-info">
            <div class="row padding-vertical-30">
                <div class="col-md-4 col-sm-4">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3922.1656122895834!2d-61.4076200847004!3d10.566210292464417!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c35feba58220d4f%3A0x21d1656c2a3d5970!2s121+Munroe+Road%2C+Trinidad+y+Tobago!5e0!3m2!1ses!2sve!4v1543334504847" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                    <h4>Cunupia, Trinidad</h4>
                    <p class="margin-top-10">Rustitiles – L.P. No. 121 Lot C, Munroe Road</p>
                </div>
                <!-- /column -->

                <div class="col-md-4 col-sm-4">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3922.425685234232!2d-61.415436085201236!3d10.545823592478255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c35f902d0b7477f%3A0x19eff1a7ec1d2b06!2sRustitiles+T%26T+LTD!5e0!3m2!1ses!2sve!4v1543334351396" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                    <h4>Chaguanas, Trinidad</h4>
                    <p class="margin-top-10">John Peter Road / Uriah Butler Highway</p>
                </div>
                <!-- /column -->

                <div class="col-md-4 col-sm-4">
                    <img src="img/icon-check.png" alt="" />
                    <h4 class="margin-top-20">email Address</h4>
                    <p class="margin-top-10">Email: info@rustitilestt.com</p>
                    <p class="margin-top-10" style="font-weight: bold;">Telf+1 (868) 220-5681</p>
                </div>
                <!-- /column -->
            </div>
            <!-- /row -->
            <div class="shadow">
                <img src="img/shadow.png" />
            </div>
            <!-- /shadow -->
        </div>
        <!-- /container -->
    </div>
    <!-- /text-center -->
</div>

<!-- Contact info End -->
@endsection
@section('scripts')
@endsection