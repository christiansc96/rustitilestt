@extends('layouts.front')

@section('title', 'Products')

@section('styles')
@endsection

@section('content')
<!-- Header End -->
<!-- Page Header Begins -->
<div id="page-header">
    <div class="header-bg-parallax" style="margin-top: -90px;">
        <div class="overlay">
            <div class="container text-center">
                <div class="header-description">
                    <h1>
                        Our products
                    </h1>
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="{{ route('fe.home') }}">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a class="active" href="#">
                                    Products
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /header-small-nav -->
                </div>
                <!-- /header-description -->
            </div>
            <!-- /container -->
        </div>
        <!-- /overlay -->
    </div>
    <!-- /product-filter -->
</div>
<!-- Page Header End -->
<!-- Product Item List Begin -->
<div class="product-item-list">
    <div class="container-fluid padding-vertical-60">
        <div class="row" style="padding:0 25px">
            <!-- Siderbar -->
            <div class="col-md-2 col-sm-4 sidebar">
                <div class="sidebar-search padding-bottom-10">
                    <h4>
                        Searcher
                    </h4>
                    <form class="product-search padding-top-40 padding-bottom-5">
                        <button class="btn-search" title="Buscar" type="submit">
                            <i class="fa fa-search">
                            </i>
                        </button>
                        <!-- /button -->
                        <input class="form-control" placeholder="Buscar producto..." id="buscar_producto_2" type="text">
                        </input>
                        <input type="hidden" name="producto_idproducto_2" id="producto_idproducto_2">
                        <div id="autocomplete_producto_container_2"></div>
                    </form>
                    <!-- /product-search -->
                </div>
                <!-- /sidebar-search -->
                <div class="product-categories padding-vertical-5">
                    <h4>
                        Spaces
                    </h4>
                    <div class="product-categories-list">
                        <ul class="padding-top-20">
                            @foreach ($spaces as $space)
                                @if($space->product->count() != 0)
                                    <a href="{{ route('fe.products.filter', $space->name) }}" style="color: black !important;">
                                        <li>
                                            
                                            {{ucfirst($space->name)}}
                                        
                                            <span>
                                                {{ $space->product->count() }}
                                            </span>
                                        </li>
                                    </a>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <!-- /product-categories-list-->
                </div>
                <!-- /product-categories-->
                <div class="product-categories padding-vertical-5">
                    <h4>
                        Uses
                    </h4>
                    <div class="product-categories-list">
                        <ul class="padding-top-20">
                            @foreach ($uses as $use)
                                @if($use->product->count() != 0)
                                    <a href="{{ route('fe.products.filter', $use->name) }}" style="color: black !important;">
                                        <li>
                                            {{ucfirst($use->name)}}
                                        
                                            <span>
                                                {{ $use->product->count() }}
                                            </span>
                                        </li>
                                    </a>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <!-- /product-categories-list-->
                </div>
                <div class="product-categories padding-vertical-5">
                    <h4>
                        Brands
                    </h4>
                    <div class="product-categories-list">
                        <ul class="padding-top-20">
                            @foreach ($brands as $brand)
                                @if($brand->product->count() != 0)
                                    <a href="{{ route('fe.products.filter', $brand->name) }}" style="color: black !important;">
                                        <li>
                                            
                                            {{ucfirst($brand->name)}}
                                        
                                            <span>
                                                {{ $brand->product->count() }}
                                            </span>
                                        </li>
                                    </a>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <!-- /product-categories-list-->
                </div>
                <div class="product-categories padding-vertical-5">
                    <h4>
                        Origin
                    </h4>
                    <div class="product-categories-list">
                        <ul class="padding-top-20">
                            @foreach ($manufacturers as $manufacturer)
                                @if($manufacturer->product->count() != 0)
                                    <a href="{{ route('fe.products.filter', $manufacturer->name) }}" style="color: black !important;">
                                        <li>
                                            
                                            {{ucfirst($manufacturer->name)}}
                                        
                                            <span>
                                                {{ $manufacturer->product->count() }}
                                            </span>
                                        </li>
                                    </a>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <!-- /product-categories-list-->
                </div>
                <!-- /product-categories-->
                {{-- <div class="filter-price padding-vertical-5">
                    <h4>
                        Precio
                    </h4>
                    <div class="slider-range padding-top-50 padding-bottom-20">
                        <fieldset class="price-range" data-max="50000" data-min="0" data-step="25">
                            <div class="price-slider">
                            </div>
                            <p>
                                <input type="text" value="0"/>
                                <input type="text" value="50000"/>
                            </p>
                            <div class="filter-button">
                                <a href="#">
                                    Filtrar
                                </a>
                            </div>
                            <!-- /filter-button-->
                        </fieldset>
                        <!-- /product-categories-->
                    </div>
                    <!-- /slider-range -->
                </div> --}}
                <!-- /filter-price -->
                <div class="color-options padding-vertical-5">
                    <h4>
                        Colors
                    </h4>
                    <div class="color-list">
                        <ul class="padding-top-20">
                            @foreach($colors as $color)
                                @if($color->product->count() != 0)
                                    <a href="{{ route('fe.products.filter', $color->name) }}" style="color: black !important;">
                                        <li>
                                            {{ ucfirst($color->name) }}
                                            <span>
                                                {{ $color->product->count() }}
                                            </span>
                                        </li>
                                    </a>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <!-- /color-list-->
                </div>
                <!-- /color-options-->
                <div class="color-options padding-vertical-5">
                    <h4>
                        Sizes
                    </h4>
                    <div class="color-list">
                        <ul class="padding-top-20">
                            @foreach($sizes as $size)
                                @if($size->product->count() != 0)
                                    <a href="{{ route('fe.products.filter', $size->name) }}" style="color: black !important;">
                                        <li>
                                            
                                            {{ $size->name }}
                                            
                                            <span>
                                               {{ $size->product->count() }}
                                            </span>
                                        </li>
                                    </a>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <!-- /color-list-->
                </div>
                <!-- /color-options-->
            </div>
            <!-- /sidebar -->
            <!-- Product Item List Begin -->
            @if($products)
                <div class="product-item-list col-md-10 col-sm-8 padding-top-20 text-center">
                    @foreach($products->chunk(25) as $product_min)
                        @foreach($product_min as $product)
                            <div class="col-md-3 col-sm-6">
                                <div class="product-item padding-bottom-60">
                                    <div class="product-image">
                                        @foreach($product->image as $image)
                                            @if($image->outstanding == 1)
                                                <a href="{{ route('fe.show.products', $product->slug) }}">
                                                    <img alt="" src="../storage/images/products/{{ $product->id }}/{{ $image->name }}">
                                                </a>
                                            @endif
                                        @endforeach
                                        @if (!Auth::guest() || Auth::guard('clients')->check())
                                            <div class="product_overlay">
                                                <div class="product-cart" style="padding-bottom: 14px;">
                                                    <a href="javascript:;" onclick="addToCart({{$product->id}})">
                                                        <p>
                                                            + Agregar al carrito
                                                        </p>
                                                    </a>
                                                </div>
                                                <!-- /product-cart -->
                                            </div>
                                        @endif
                                            <!-- /product_overlay -->
                                    </div>
                                    <!-- /product-image -->
                                    <div class="product-short-detail padding-top-20">
                                        <div class="product-title">
                                            <p>
                                                <a href="{{ route('fe.show.products', $product->slug) }}">
                                                   {{ $product->name }}
                                                </a>
                                            </p>
                                        </div>
                                        @if (!Auth::guest() || Auth::guard('clients')->check())
                                            <div class="product-price">
                                                <p>{{ $product->price->price }} {{$product->price->coin}}</p>
                                            </div>
                                        @endif
                                        <!-- /product-title -->
                                    </div>
                                    <!-- /product-short-detail -->
                                </div>
                                <!-- /product-item -->
                            </div>
                        @endforeach
                    @endforeach
                    {{-- <div class="pagination padding-top-50"> --}}
                        {!! $products->render() !!}
                                                {{-- <ul><li class="active"></li></ul> --}}
                        {{-- <ul>
                            <li class="active"><a href="productos.html">01</a>
                            </li>
                            <li><a href="productos2.html">02</a>
                            </li>
                            <li><a href="productos2.html"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul> --}}
                    {{-- </div> --}}
                    <!-- /pagination -->
                </div>
            @else
                <div class="product-item-list col-md-10 col-sm-8 padding-top-20 text-center">
                    <div class="col-md-3 col-sm-6">
                        There are no products available
                    </div>
                </div>
            @endif
            <!-- /product-item-list -->
        </div>
    </div>
    <!-- /container -->
</div>
<!-- Product Item List End -->
{!!Form::open(['route'=>['rustitiles-cart.store'],'method'=>'POST', 'id' => 'form-post-cart'])!!}
{!!Form::close()!!}
@endsection

@section('scripts')
<script>
$( "#find_product" ).keyup(function(event) {
    console.log(event.key);   
});
var addToCart = function(id, form = $('#form-post-cart'))
{          

  var url = form.attr('action');

  var data = form.serialize()+'&id='+id+'&qty=1';

  $.post(url, data, function(response){
    console.log(response);
    $("#count").load(" #count");
    $(".cart-product-list").load(" .cart-product-list");
    $("#total_price").load(" #total_price");
    swal({
        title: 'Aprobado!',
        text: response,
        type: 'success',
        timer: '2500'
    })

  });

}

$(document).ready(function(){
    $("#buscar_producto_2").keyup(function(){
      $.ajax({
          url: '/Autocomplete',
          type:'GET',
          dataType:'json',
          data:{info:$('#buscar_producto_2').val()}
      }).done(function(data){
        console.log(data);
        if( data != 0){
          $("#producto_idproducto_2").val(data.id);
          $("#autocomplete_producto_container_2").fadeIn();
          $("#autocomplete_producto_container_2").html(data.output);
          $(document).on('click','#opcion_productos', function(){
            // $("#buscar").prop("href", "http://peltca.pro/Productos/"+data.id);
            $("#buscar_producto_2").val($(this).text());
            $("#autocomplete_producto_container_2").fadeOut();
          });
        }else{
          $("#buscar").prop("href", "#");
          $("#autocomplete_producto_container_2").fadeOut();
        }
      });
    });
});
</script>
@endsection