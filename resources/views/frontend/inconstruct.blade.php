<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="ThemeStarz">

    <link href="{{ asset('inconstruct/assets/fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('inconstruct/assets/fonts/elegant-fonts.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('inconstruct/assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('inconstruct/assets/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('inconstruct/assets/css/magnific-popup.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('inconstruct/assets/css/trackpad-scroll-emulator.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('inconstruct/assets/css/style.css') }}" type="text/css">

    <title>Tu Cerámica</title>

</head>

<body class=" frame nav-btn-only">

<div id="outer-wrapper" class="animate translate-z-in">
    <div id="inner-wrapper">
        <div id="table-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <header><a href="#" class="animate animate fade-in animation-time-3s"><img src="{{ asset('inconstruct/assets/img/logo.png') }}" alt="" style="margin-left: 5%;"></a></header>
                </div>

            </div>
            <div class="container">
                <!--end row-header-->
                <div id="row-content">
                    <div id="navigation-wrapper" class="animate">
                        <a href="#contact" class="open-side-panel" style="position: absolute; right: 50px;"><span>Contact</span></a>
                    </div>
                    <!--end navigation-wrapper-->
                    <div id="content-wrapper">
                        <div id="content" class="animate translate-z-in animation-time-2s delay-05s" style="margin-top: -100px;">
                            <h2 class="opacity-70" style="text-align: center;">We are working to offer you the best service</h2>
                            <h1 class="opacity-70" style="text-align: center; margin-top: -30px;">Is very close</h1>
                            <div class="center count-down animate" data-countdown-year="2018" data-countdown-month="7" data-countdown-day="2"></div>
                        </div>
                        <!--end content-->
                    </div>
                    <!--end content-wrapper-->
                </div>
                <!--end row-content-->
                <div id="row-footer">
                    <footer>
                        <div class="social-icons" align="center">
                            <a href="https://twitter.com/tuceramica" class="animate fade-in animation-time-1s delay-1s" target="blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.facebook.com/pages/tuceramicacom/329210243846626" class="animate fade-in animation-time-1s delay-08s" target="blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.youtube.com/channel/UCA-jGA9SFCo37mIlFmQpvYg" class="animate fade-in animation-time-1s delay-06s" target="blank"><i class="fa fa-youtube"></i></a>
                            <a href="https://www.instagram.com/tuceramicaweb" class="animate fade-in animation-time-1s delay-06s" target="blank"><i class="fa fa-instagram"></i></a>
                        </div>
                    </footer>
                </div>
                <!--end row-footer-->
            </div>
            <!--end container-->
        </div>
        <!--end table-wrapper-->
        <div class="background-wrapper has-vignette zoom-animation">
            <div><img src="{{ asset('inconstruct/assets/img/background-14.jpg') }}" alt=""></div>
        </div>
        <!--end background-wrapper-->
    </div>
    <!--end inner-wrapper-->
</div>
<!--end outer-wrapper-->



<div class="side-panel" id="contact">
    <div class="close-panel"><i class="fa fa-chevron-right"></i></div>
    <div class="wrapper">
        <div class="tse-scrollable">
            <div class="tse-content">
                <div class="wrapper">
                    <div class="container">
                        <h2>Contact</h2>
                        <section>
                            <p>
                                Tuceramica.com focuses its activity on everything related to the world of coating, covering products such as: ceramics, porcelain, leveling and paste systems, together with a universe of stores. It includes the brands MAPISA, PERONDA, RECER, CERAMIC and KERATEC, CERAMIFÁCIL and has commercial delegations both in Venezuela and abroad.
                            </p>
                            <p>
                                Tuceramica.com is currently characterized by offering products adapted to the different styles currently demanded by professionals in interior design and decoration.
                            </p>
                        </section>
                        <section>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <h3>Contact us</h3>
                                    <address>
                                        Caracas, Venezuela
                                        <br>
                                        0212.741.36.87
                                        <br>
                                        <a href="#">tuceramicaweb@gmail.com</a>
                                    </address>
                                </div>
                                <!--end col-sm-6-->
                                <div class="col-md-6 col-sm-6">
                                    <h3>Social</h3>
                                    <figure><a href="https://www.facebook.com/pages/tuceramicacom/329210243846626" class="icon" target="blank"><i class="fa fa-facebook"></i>Facebook</a></figure>
                                    <figure><a href="https://twitter.com/tuceramica" class="icon" target="blank"><i class="fa fa-twitter"></i>Twitter</a></figure>
                                    <figure><a href="https://www.instagram.com/tuceramicaweb" class="icon" target="blank"><i class="fa fa-instagram"></i>Instagram</a></figure>
                                    <figure><a href="https://www.youtube.com/channel/UCA-jGA9SFCo37mIlFmQpvYg" class="icon" target="blank"><i class="fa fa-youtube"></i>Youtube</a></figure>
                                </div>
                                <!--end col-sm-6-->
                            </div>
                        </section>
                        <section>
                            <h3>Write U.S</h3>
                            <form id="form-contact" action="#" method="post" class="form clearfix inputs-underline">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="form-contact-name" name="name" placeholder="Nombre" required>
                                        </div>
                                        <!--end form-group -->
                                    </div>
                                    <!--end col-md-6 col-sm-6 -->
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="form-contact-email" name="email" placeholder="Email" required>
                                        </div>
                                        <!--end form-group -->
                                    </div>
                                    <!--end col-md-6 col-sm-6 -->
                                </div>
                                <!--end row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control" id="form-contact-message" rows="8" name="message" placeholder="Mensaje" required></textarea>
                                        </div>
                                        <!--end form-group -->
                                    </div>
                                    <!--end col-md-12 -->
                                </div>
                                <!--end row -->
                                <div class="form-group clearfix">
                                    <button type="submit" class="btn pull-right btn-default btn-framed btn-rounded" id="form-contact-submit">Enviar Mensaje</button>
                                </div>
                                <!--end form-group -->
                                <div class="form-contact-status"></div>
                            </form>
                            <!--end form-contact -->
                        </section>
                    </div>
                    <!--end container-->
                </div>
                <!--end wrapper-->
            </div>
            <!--end tse-content-->
        </div>
        <!--end tse-scrollable-->
    </div>
    <!--end wrapper-->
</div>
<!--end contact-->

<div class="backdrop"></div>

<script type="text/javascript" src="{{ asset('inconstruct/assets/js/jquery-2.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('inconstruct/assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('inconstruct/assets/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('inconstruct/assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('inconstruct/assets/js/jquery.magnific-popup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('inconstruct/assets/js/jquery.trackpad-scroll-emulator.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('inconstruct/assets/js/jquery.plugin.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('inconstruct/assets/js/jquery.countdown.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('inconstruct/assets/js/custom.js') }}"></script>



</body>
