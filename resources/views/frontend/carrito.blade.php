@extends('layouts.front')
@section('title', 'Cart')
@section('styles')
@endsection
@section('content')

<!-- Header End -->

<!-- Page Header Begins -->

<div id="page-header">
    <div class="header-bg-parallax" style="margin-top: -90px;">
        <div class="overlay">
            <div class="container text-center">
                <div class="header-description">
                    <h1>Cart</h1>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('fe.home') }}">Home</a>
                            </li>
                            <li><a href="{{ route('fe.products') }}">Products</a>
                            </li>
                            <li><a href="{{ route('rustitiles-cart.index') }}" class="active">Cart</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /header-small-nav -->
                </div>
                <!-- /header-description -->
            </div>
            <!-- /container -->
        </div>
        <!-- /overlay -->
    </div>
    <!-- /product-filter -->
</div>

<!-- Page Header End -->

<!-- Cart Table Begin -->

<div class="cart-table">
    <div class="container padding-vertical-100">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table" id="cart-table">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                        </thead>
                        <!-- /thead -->

                        <tbody>
                          @foreach(Cart::instance('tuceramica')->content() as $cartItem)
                            {{-- expr --}}
                            <tr>
                                <td>
                                    <img class="shopping-product" src="storage/images/products/{{$cartItem->options->product_id}}/{{$cartItem->options->image}}" style="max-width: 100px;" alt="">
                                </td>
                                <td class="product-name">
                                    <p>{{$cartItem->name}}</p>
                                </td>
                                <!-- /product-name -->
                                <td class="product-price">
                                    <h5>{{$cartItem->price}} {{$cartItem->options->coin}}</h5>
                                </td>
                                <!-- /product-price -->
                                <td>
                                  <form method="POST" action="{{route('rustitiles-cart.update', ':CART_ID')}}" id="form-update-{{$cartItem->rowId}}">
                                    {{csrf_field()}}
                                    {{method_field('PATCH')}}
                                    <div class="input-group quantity-number">
                                        <input type="text" name="qty" id="qty" class="form-control" value="{{$cartItem->qty}}">
                                        <input type="hidden" name="id" value="'{{$cartItem->rowId}}'">
                                        <input type="hidden" name="up_down" id="up_down" value="0">
                                        <div class="input-group-btn-vertical">
                                            <div class="btn" onclick="upQty('{{$cartItem->rowId}}')"><i class="fa fa-angle-up"></i>
                                            </div>
                                            <!-- /btn -->
                                            <div class="btn" onclick="downQty('{{$cartItem->rowId}}')"><i class="fa fa-angle-down"></i>
                                            </div>
                                            <!-- /btn -->
                                        </div>
                                        <!-- /input-group-btn-vertical -->
                                    </div>
                                    <!-- /quantity-number -->
                                  </form>
                                </td>
                                <td class="product-price">
                                    <h5 id="product-price">{{$cartItem->price * $cartItem->qty}} {{$cartItem->options->coin}}</h5>
                                </td>
                                <!-- /product-price -->
                                <td class="text-center"><a class="btn-close" href="#" onclick="deleteData('{{$cartItem->rowId}}')"><i class="fa fa-close"></i></a>
                                </td>
                                <!-- /btn-close -->
                            </tr>
                          @endforeach
                        </tbody>
                        <!-- /tbody -->
                    </table>
                    <!-- /table -->
                </div>
                <!-- /table-responsive -->

            </div>
            <!-- /column -->
        </div>
        <!-- /row -->

        <div class="row">
            <div class="col-md-4 pull-right">
                <div class="shop_measures" >
                    <h4>Total</h4>
                    <ul class="cart-total padding-top-50 margin-bottom-80">
                        {{-- <li>Subtotal:<span class="product-price">Bs 640.000</span>
                        </li>
                        <li>IVA:<span class="product-price">Bs 10.000</span>
                        </li>
                        <li>Envio:<span class="product-price">Bs 10.000</span>
                        </li> --}}
                        <li>Total:<span class="product-price total" id="totally">Bs {{Cart::subtotal()}}</span>
                        </li>
                    </ul>
                    <!-- /cart-total -->
                    <a href="javascript:;" class="btn-form pull-right" onclick="addOrder2()">Send purchase request</a>
                </div>
                <!-- /shop_measures -->
            </div>
            <!-- /column -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
{!!Form::open(['route'=>[ 'carrito.destroy', ':CART_ID' ],'method'=>'DELETE', 'id' => 'form-delete'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'mis-pedidos.store'], 'method'=>'POST', 'id' => 'form-post-carrito'])!!}
{!!Form::close()!!}
@endsection
@section('scripts')
<script>

var addOrder2 = function(form = $('#form-post-carrito')){          

    var url = form.attr('action');

    var data = form.serialize();

    $.post(url, data, function(response){

        $("#count").load(" #count");
        $(".cart-product-list").load(" .cart-product-list");
        $("#total_price").load(" #total_price");
        $("#cart-table").load(" #cart-table");
        $("#totally").load(" #totally");
        swal({
            title: 'successfully!',
            text: response,
            type: 'success',
            timer: '2500'
        })

    });

}
  
var upQty = function(id){

  var form = $('#form-update-'+id);
  
  $('#form-update-'+id+' #up_down').val(1)

  var url = form.attr('action').replace(':CART_ID', id);

  var data = form.serialize();

  $.post(url, data, function(response){
    $("#count").load(" #count");
    $(".cart-product-list").load(" .cart-product-list");
    $("#total_price").load(" #total_price");
    $("#cart-table").load(" #cart-table");
    $("#totally").load(" #totally");
    
    // swal({
    //     title: 'successfully y enviado!',
    //     text: response,
    //     type: 'success',
    //     timer: '1500'
    // })
  });

}

var downQty = function(id){

  var form = $('#form-update-'+id);
  
  $("#up_down").attr('value', 0);
  
  var url = form.attr('action').replace(':CART_ID', id);

  var data = form.serialize();

  $.post(url, data, function(response){
    $("#count").load(" #count");
    $(".cart-product-list").load(" .cart-product-list");
    $("#total_price").load(" #total_price");
    $("#cart-table").load(" #cart-table");
    $("#totally").load(" #totally");
    // swal({
    //     title: 'successfully y enviado!',
    //     text: response,
    //     type: 'success',
    //     timer: '1500'
    // })
  });

}

var deleteData = function(id){

  var form = $('#form-delete');

  var url = form.attr('action').replace(':CART_ID', id);

  var data = form.serialize();

  swal({
    title: 'Sure?',
    text: "You will not be able to reverse this!",
    type: 'warning',
    showCancelButton: true,
    cancelButtonColor: '#d33',
    confirmButtonColor: '#3085d6',
    confirmButtonText: 'Si',
    cancelButtonText: 'No',
  }).then(function () {
    $.post(url, data, function(response){
      $("#cart-table").load(" #cart-table");
      $("#count").load(" #count");
      $(".cart-product-list").load(" .cart-product-list");
      $("#total_price").load(" #total_price");
      $("#totally").load(" #totally");
      table.ajax.reload();
      swal({
          title: 'successfully!',
          text: response,
          type: 'success',
          timer: '1500'
      })
    });
  });

}
</script>
@endsection