@extends('layouts.front')

@section('title', 'Directory')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
@endsection

@section('content')
<!-- Page Header Begins -->
<iframe src="https://www.google.com/maps/d/u/0/embed?mid=19ed_E-6M8qIkAXg5miGSdoP0UhE9RV5I" style="border-top: 120px solid #000000;" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
<!-- Page Header End -->
<!-- Product Item List Begin -->
<div class="product-item-list">
    <div class="container-fluid padding-vertical-60">
        <div class="row" style="padding:0 25px">
            <!-- Siderbar -->
            <div class="col-md-2 col-sm-4 sidebar">
                <div class="product-categories padding-vertical-5">
                    <h4>
                        States
                    </h4>
                    <div class="product-categories-list">
                        <ul class="padding-top-20">
                            @foreach ($states as $state)
                                <li>
                                    <a href="{{ route('fe.show.directory', $state->name) }}">
                                        {{$state->name}}
                                    </a>
                                    <span>
                                        {{$state->store->count()}}
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /product-categories-list-->
                </div>
                <!-- /product-categories-->
            </div>
            <!-- /sidebar -->
            <!-- Product Item List Begin -->
            <div class="product-item-list col-md-10 col-sm-8 padding-top-20 text-center">
                @foreach ($stores as $store)
                    <div class="col-md-6 col-sm-6">
                        <div style="width: 100%; border: 1px solid #eaeaea; height: auto; padding: 15px; margin-bottom: 25px;">
                            <div class="row">
                                <div class="col-md-3" style="margin: 0;">
                                    <img src="{{ asset('storage/images/stores/'.$store->id.'/'.$store->image->name) }}" alt="{{$store->image->name}}">
                                </div>
                                <div class="col-md-9" style="margin: 0;">
                                    <h1 style="text-align: left; margin: 5px 0 15px 0">
                                        {{$store->name}}
                                    </h1>
                                    
                                    <p style="text-align: left; margin: 10px 0; color: #868686;">
                                        <strong style="color: #000;">
                                            Phone:
                                        </strong>
                                        {{$store->phone}}
                                    </p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <!-- /column -->
                {{-- <div class="pagination padding-top-50">
                    <ul>
                        <li class="active">
                            <a href="productos.html">
                                01
                            </a>
                        </li>
                        <li>
                            <a href="productos2.html">
                                02
                            </a>
                        </li>
                        <li>
                            <a href="productos2.html">
                                <i class="fa fa-angle-double-right">
                                </i>
                            </a>
                        </li>
                    </ul>
                </div> --}}
                <!-- /pagination -->
            </div>
            <!-- /product-item-list -->
        </div>
    </div>
    <!-- /container -->
</div>
<!-- Product Item List End -->
@endsection

@section('scripts')

@endsection
