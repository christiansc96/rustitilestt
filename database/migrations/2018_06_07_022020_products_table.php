<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('code')->nullable();
            $table->string('pei')->nullable();
            $table->string('packing')->nullable();
            $table->text('description')->nullable();
            $table->text('maintenance')->nullable();
            $table->text('short_description')->nullable();
            $table->text('info')->nullable();
            $table->string('slug')->unique();
            $table->tinyInteger('active')->nullable();

            $table->unsignedInteger('use_id');
            $table->foreign('use_id')->references('id')->on('uses')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('manufacturer_id');
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('brand_id');
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('pasta_id');
            $table->foreign('pasta_id')->references('id')->on('pastas')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('antislip_id');
            $table->foreign('antislip_id')->references('id')->on('anti-slips')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('texture_id');
            $table->foreign('texture_id')->references('id')->on('textures')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('space_id');
            $table->foreign('space_id')->references('id')->on('spaces')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('price_id');
            $table->foreign('price_id')->references('id')->on('prices')->onDelete('cascade')->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
