<?php

use Illuminate\Database\Seeder;
use App\State;

class StateSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $state = new State();
        $state->name = 'Amazonas';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Anzoátegui';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Apure';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Aragua';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Barinas';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Bolívar';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Carabobo';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Cojedes';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Delta Amacuro';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Distrito Capital';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Falcón';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Guárico';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Lara';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Mérida';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Miranda';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Monagas';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Nueva Esparta';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Portuguesa';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Sucre';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Táchira';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Trujillo';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Vargas';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Yaracuy';
        $state->active = 1;
        $state->save();

        $state = new State();
        $state->name = 'Zulia';
        $state->active = 1;
        $state->save();
    }
}
