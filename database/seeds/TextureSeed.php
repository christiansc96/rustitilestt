<?php

use Illuminate\Database\Seeder;
use App\Texture;

class TextureSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $texture = new Texture();
        $texture->name = 'mate';
        $texture->active = 1;
        $texture->save();

        $texture = new Texture();
        $texture->name = 'satinado';
        $texture->active = 1;
        $texture->save();

        $texture = new Texture();
        $texture->name = 'brillante';
        $texture->active = 1;
        $texture->save();

        $texture = new Texture();
        $texture->name = 'pulido';
        $texture->active = 1;
        $texture->save();
    }
}
